DROP DATABASE IF EXISTS eperizinan;
CREATE DATABASE eperizinan;
USE eperizinan;

CREATE TABLE IF NOT EXISTS JenisRole (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama TEXT,
	dapatTambahPemohon BOOLEAN DEFAULT false,
	dapatEditPemohon BOOLEAN DEFAULT false,
	dapatHapusPemohon BOOLEAN DEFAULT false,
	dapatValidasiPermohonan BOOLEAN DEFAULT false,
	dapatTambahPermohonan BOOLEAN DEFAULT false,
	dapatEditPermohonan BOOLEAN DEFAULT false,
	dapatHapusPermohonan BOOLEAN DEFAULT false,
	dapatEditMonitoring BOOLEAN DEFAULT false,
	dapatHapusMonitoring BOOLEAN DEFAULT false,
	dapatTambahBerkas BOOLEAN DEFAULT false,
	dapatEditBerkas BOOLEAN DEFAULT false,
	dapatHapusBerkas BOOLEAN DEFAULT false,
	dapatTambahPenerimaKuasa BOOLEAN DEFAULT false,
	dapatEditPenerimaKuasa BOOLEAN DEFAULT false,
	dapatHapusPenerimaKuasa BOOLEAN DEFAULT false,
	dapatEditPersyaratan BOOLEAN DEFAULT false,
	dapatKonfigurasi BOOLEAN DEFAULT false
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Role (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	username TEXT,
	password VARCHAR(300) DEFAULT '$2y$10$WpOufCbskJ47nqxtqkpKUepq.cRUPSwCesKRisotw9jtc9vpfE0OG',
	nomorJenisRole BIGINT,
	FOREIGN KEY (nomorJenisRole)
	REFERENCES JenisRole (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Log (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	waktu VARCHAR(100),
	kegiatan VARCHAR(100),
	data VARCHAR(100),
	nomorRole BIGINT,
	FOREIGN KEY (nomorRole)
	REFERENCES Role (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS JenisBangunan (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama VARCHAR(100)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Kecamatan (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama VARCHAR(100)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Kelurahan (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama VARCHAR(100),
	nomorKecamatan BIGINT,
	FOREIGN KEY (nomorKecamatan)
	REFERENCES Kecamatan (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;

INSERT INTO
	JenisRole (nama, dapatTambahPemohon, dapatEditPemohon,
		    dapatHapusPemohon,dapatValidasiPermohonan, dapatTambahPermohonan,
		    dapatEditPermohonan, dapatHapusPermohonan, dapatEditMonitoring,
		    dapatHapusMonitoring, dapatTambahBerkas, dapatEditBerkas,
		    dapatHapusBerkas, dapatTambahPenerimaKuasa, dapatEditPenerimaKuasa,
		    dapatHapusPenerimaKuasa, dapatEditPersyaratan, dapatKonfigurasi)
VALUES
	('admin', true, true, true, true, true, true,
	 true, true, true, true, true, true, true,
	 true, true, true, true);


INSERT INTO
	JenisRole (nama, dapatTambahPemohon, dapatEditPemohon,
		    dapatHapusPemohon,dapatValidasiPermohonan, dapatTambahPermohonan,
		    dapatEditPermohonan, dapatHapusPermohonan, dapatEditMonitoring,
		    dapatHapusMonitoring, dapatTambahBerkas, dapatEditBerkas,
		    dapatHapusBerkas, dapatTambahPenerimaKuasa, dapatEditPenerimaKuasa,
		    dapatHapusPenerimaKuasa, dapatEditPersyaratan, dapatKonfigurasi)
VALUES
	('operator', true, false,
	false, true, true,
	false, false, false,
	false, true, false,
	false, true, false,
	false, false, false);


INSERT INTO JenisRole(nama) VALUES ('viewer');

INSERT INTO Role (username, password, nomorJenisRole)
VALUES ('indonesia', '$2y$10$WpOufCbskJ47nqxtqkpKUepq.cRUPSwCesKRisotw9jtc9vpfE0OG', 1);

INSERT INTO Kecamatan (nama) VALUES ('Cimahi Utara');
INSERT INTO Kecamatan (nama) VALUES ('Cimahi Tengah');
INSERT INTO Kecamatan (nama) VALUES ('Cimahi Selatan');

INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Cibabat', 1);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Cipageran', 1);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Citereup', 1);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Pasirkaliki', 1);

INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Baros', 2);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Cigugur Tengah', 2);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Cimahi', 2);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Karang Mekar', 2);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Padasuka', 2);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Setia Manah', 2);

INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Cibeber', 3);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Cibereum', 3);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Leuwigajah', 3);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Melong', 3);
INSERT INTO Kelurahan (nama, nomorKecamatan) VALUES ('Utama', 3);


CREATE TABLE IF NOT EXISTS Pemohon (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama TEXT,
	alamat TEXT,
	jenisBadanUsaha VARCHAR(100) DEFAULT 'perseorangan',
	noAktePerusahaan TEXT,
	namaBadanUsaha TEXT,
	jabatan TEXT,
	alamatBadanUsaha TEXT,
	npwp TEXT,
	luasBangunan DOUBLE,
	luasTanah DOUBLE,
	nomorJenisBangunan BIGINT,
	alamatLokasi VARCHAR(100),
	rt TINYINT,
	rw TINYINT,
	nomorKelurahan BIGINT,
	kota VARCHAR(10) DEFAULT 'Cimahi',
	nomorSuratPermohonan VARCHAR(100),
	nomorKtp VARCHAR(100),
	nomorSuratKuasa VARCHAR(100),
	nomorAktePendirian VARCHAR(100),
	nomorPbb VARCHAR(100),
	nomorProposalProyek VARCHAR(100),
	nomorRincianLahan VARCHAR(100),
	nomorSuratPernyataan VARCHAR(100),
	nomorSuratRencanaPembangunan VARCHAR(100),
	nomorSuratDomisiliPerusahaan VARCHAR(100),
	nomorSuratPemberitahuanTetangga VARCHAR(100),
	nomorSertifikatTanah VARCHAR(100),
	tahunSertifikatTanah VARCHAR(100),
	luasTanahSertifikatTanah DOUBLE,
	atasNamaSertifikatTanah VARCHAR(100),
	jenisKepemilikanTanahSertifikatTanah VARCHAR(100),
	nomorDisposisiSekda VARCHAR(100),
	nomorDisposisiKaBappeda VARCHAR(100),
	nomorDisposisiKabid VARCHAR(100),
	FOREIGN KEY (nomorKelurahan)
	REFERENCES Kelurahan (nomor)
	ON DELETE CASCADE,
	FOREIGN KEY (nomorJenisBangunan)
	REFERENCES JenisBangunan (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS Berkas (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama TEXT,
	ext VARCHAR(10),
	nomorPemohon BIGINT,
	FOREIGN KEY (nomorPemohon)
	REFERENCES Pemohon (nomor)
)ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS PenerimaKuasa (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nama TEXT,
	alamat TEXT,
	noHp TEXT,
	nomorPemohon BIGINT,
	FOREIGN KEY (nomorPemohon)
	REFERENCES Pemohon (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Permohonan (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	tglPermohonan VARCHAR(100),
	tglBerkasMasuk VARCHAR(100),
	pembahasanRapatBKPRD TEXT,
	tglPembahasan VARCHAR(100),
	tglTinjauanLapangan VARCHAR(100),
	validasi1 BOOLEAN,
	validasi2 BOOLEAN,
	validasi3 BOOLEAN,
	izinPrinsip VARCHAR(100),
	nomorNotaDinasPenolakan VARCHAR(100),
	latitude DOUBLE,
	longitude DOUBLE,
	nomorPemohon BIGINT,
	namaPemohon VARCHAR(100),
	beritaAcaraLapangan VARCHAR(100),
	jenisKegiatan TEXT,
	nomorIzinPrinsip VARCHAR(100),
	tglIzinPrinsip VARCHAR(100),
	penerimaPerizinan VARCHAR(100),
	tglPenerimaPerizinan VARCHAR(100),
	tglIzinLokasi VARCHAR(100),
	nomorIzinLokasi VARCHAR(100),
	FOREIGN KEY (nomorPemohon)
	REFERENCES Pemohon (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Monitoring (
	nomor BIGINT PRIMARY KEY AUTO_INCREMENT,
	nomorPermohonan BIGINT,
	nomorIppt VARCHAR(100),
	tglIppt VARCHAR(100),
	dalamProsesIppt VARCHAR(100),
	nomorDokumenLingkungan VARCHAR(100),
	tglDokumenLingkungan VARCHAR(100),
	dalamProsesDokumenLingkungan VARCHAR(100),
	nomorAmdalLalin VARCHAR(100),
	tglAmdalLalin VARCHAR(100),
	dalamProsesAmdalLalin VARCHAR(100),
	nomorKkop VARCHAR(100),
	tglKkop VARCHAR(100),
	dalamProsesKkop VARCHAR(100),
	nomorImb VARCHAR(100),
	tglImb VARCHAR(100),
	dalamProsesImb VARCHAR(100),
	FOREIGN KEY (nomorPermohonan)
	REFERENCES Permohonan (nomor)
	ON DELETE CASCADE
)ENGINE=InnoDB;
