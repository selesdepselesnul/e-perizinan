-- 2016-03-05T11:21:21+07:00 - mysql:dbname=eperizinan;host=localhost
SET FOREIGN_KEY_CHECKS = 0;

-- Table structure for table `berkas`

DROP TABLE IF EXISTS `berkas`;
CREATE TABLE `berkas` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `ext` varchar(10) DEFAULT NULL,
  `nomorPemohon` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorPemohon` (`nomorPemohon`),
  CONSTRAINT `berkas_ibfk_1` FOREIGN KEY (`nomorPemohon`) REFERENCES `pemohon` (`nomor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table `berkas`

LOCK TABLES `berkas` WRITE;
UNLOCK TABLES;

-- Table structure for table `jenisbangunan`

DROP TABLE IF EXISTS `jenisbangunan`;
CREATE TABLE `jenisbangunan` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table `jenisbangunan`

LOCK TABLES `jenisbangunan` WRITE;
INSERT INTO `jenisbangunan` VALUES (1,'Gudang');
UNLOCK TABLES;

-- Table structure for table `jenisrole`

DROP TABLE IF EXISTS `jenisrole`;
CREATE TABLE `jenisrole` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `dapatTambahPemohon` tinyint(1) DEFAULT '0',
  `dapatEditPemohon` tinyint(1) DEFAULT '0',
  `dapatHapusPemohon` tinyint(1) DEFAULT '0',
  `dapatValidasiPermohonan` tinyint(1) DEFAULT '0',
  `dapatTambahPermohonan` tinyint(1) DEFAULT '0',
  `dapatEditPermohonan` tinyint(1) DEFAULT '0',
  `dapatHapusPermohonan` tinyint(1) DEFAULT '0',
  `dapatEditMonitoring` tinyint(1) DEFAULT '0',
  `dapatHapusMonitoring` tinyint(1) DEFAULT '0',
  `dapatTambahBerkas` tinyint(1) DEFAULT '0',
  `dapatEditBerkas` tinyint(1) DEFAULT '0',
  `dapatHapusBerkas` tinyint(1) DEFAULT '0',
  `dapatTambahPenerimaKuasa` tinyint(1) DEFAULT '0',
  `dapatEditPenerimaKuasa` tinyint(1) DEFAULT '0',
  `dapatHapusPenerimaKuasa` tinyint(1) DEFAULT '0',
  `dapatEditPersyaratan` tinyint(1) DEFAULT '0',
  `dapatKonfigurasi` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table `jenisrole`

LOCK TABLES `jenisrole` WRITE;
INSERT INTO `jenisrole` VALUES (1,'admin',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
INSERT INTO `jenisrole` VALUES (2,'operator',1,0,0,1,1,0,0,0,0,1,0,0,1,0,0,0,0);
INSERT INTO `jenisrole` VALUES (3,'viewer',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
INSERT INTO `jenisrole` VALUES (4,'custom',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
UNLOCK TABLES;

-- Table structure for table `kecamatan`

DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table `kecamatan`

LOCK TABLES `kecamatan` WRITE;
INSERT INTO `kecamatan` VALUES (1,'Cimahi Utara');
INSERT INTO `kecamatan` VALUES (2,'Cimahi Tengah');
INSERT INTO `kecamatan` VALUES (3,'Cimahi Selatan');
UNLOCK TABLES;

-- Table structure for table `kelurahan`

DROP TABLE IF EXISTS `kelurahan`;
CREATE TABLE `kelurahan` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `nomorKecamatan` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorKecamatan` (`nomorKecamatan`),
  CONSTRAINT `kelurahan_ibfk_1` FOREIGN KEY (`nomorKecamatan`) REFERENCES `kecamatan` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Dumping data for table `kelurahan`

LOCK TABLES `kelurahan` WRITE;
INSERT INTO `kelurahan` VALUES (1,'Cibabat',1);
INSERT INTO `kelurahan` VALUES (2,'Cipageran',1);
INSERT INTO `kelurahan` VALUES (3,'Citereup',1);
INSERT INTO `kelurahan` VALUES (4,'Pasirkaliki',1);
INSERT INTO `kelurahan` VALUES (5,'Baros',2);
INSERT INTO `kelurahan` VALUES (6,'Cigugur Tengah',2);
INSERT INTO `kelurahan` VALUES (7,'Cimahi',2);
INSERT INTO `kelurahan` VALUES (8,'Karang Mekar',2);
INSERT INTO `kelurahan` VALUES (9,'Padasuka',2);
INSERT INTO `kelurahan` VALUES (10,'Setia Manah',2);
INSERT INTO `kelurahan` VALUES (11,'Cibeber',3);
INSERT INTO `kelurahan` VALUES (12,'Cibereum',3);
INSERT INTO `kelurahan` VALUES (13,'Leuwigajah',3);
INSERT INTO `kelurahan` VALUES (14,'Melong',3);
INSERT INTO `kelurahan` VALUES (15,'Utama',3);
UNLOCK TABLES;

-- Table structure for table `log`

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `waktu` varchar(100) DEFAULT NULL,
  `kegiatan` varchar(100) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  `nomorRole` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorRole` (`nomorRole`),
  CONSTRAINT `log_ibfk_1` FOREIGN KEY (`nomorRole`) REFERENCES `role` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- Dumping data for table `log`

LOCK TABLES `log` WRITE;
INSERT INTO `log` VALUES (1,'Saturday, 05-Mar-2016 09:20:25 WIB','login',NULL,1);
INSERT INTO `log` VALUES (2,'Saturday, 05-Mar-2016 09:46:35 WIB','logout',NULL,1);
INSERT INTO `log` VALUES (3,'Saturday, 05-Mar-2016 09:46:44 WIB','login',NULL,2);
INSERT INTO `log` VALUES (4,'Saturday, 05-Mar-2016 09:47:03 WIB','logout',NULL,2);
INSERT INTO `log` VALUES (5,'Saturday, 05-Mar-2016 09:47:10 WIB','login',NULL,1);
INSERT INTO `log` VALUES (6,'Saturday, 05-Mar-2016 09:47:22 WIB','logout',NULL,1);
INSERT INTO `log` VALUES (7,'Saturday, 05-Mar-2016 09:47:30 WIB','login',NULL,2);
INSERT INTO `log` VALUES (8,'Saturday, 05-Mar-2016 09:47:50 WIB','menambahkan','pemohon dengan nomor : 1',2);
INSERT INTO `log` VALUES (9,'Saturday, 05-Mar-2016 09:50:09 WIB','login',NULL,1);
INSERT INTO `log` VALUES (10,'Saturday, 05-Mar-2016 10:07:36 WIB','logout',NULL,2);
INSERT INTO `log` VALUES (11,'Saturday, 05-Mar-2016 10:07:43 WIB','login',NULL,1);
INSERT INTO `log` VALUES (12,'Saturday, 05-Mar-2016 10:07:52 WIB','menambahkan','proses dengan nomor : 1 ;dari pemohon dengan nomor : 1',1);
INSERT INTO `log` VALUES (13,'Saturday, 05-Mar-2016 10:39:14 WIB','menambahkan','pemohon dengan nomor : 2',1);
INSERT INTO `log` VALUES (14,'Saturday, 05-Mar-2016 10:39:21 WIB','menambahkan','pemohon dengan nomor : 3',1);
INSERT INTO `log` VALUES (15,'Saturday, 05-Mar-2016 10:40:14 WIB','menambahkan','proses dengan nomor : 2 ;dari pemohon dengan nomor : 2',1);
INSERT INTO `log` VALUES (16,'Saturday, 05-Mar-2016 10:40:22 WIB','menambahkan','proses dengan nomor : 3 ;dari pemohon dengan nomor : 3',1);
INSERT INTO `log` VALUES (17,'Saturday, 05-Mar-2016 10:40:30 WIB','memvalidasi','proses dengan nomor : 2 ;dari pemohon dengan nomor : 2',1);
INSERT INTO `log` VALUES (18,'Saturday, 05-Mar-2016 10:41:05 WIB','memvalidasi','proses dengan nomor : 3 ;dari pemohon dengan nomor : 3',1);
INSERT INTO `log` VALUES (19,'Saturday, 05-Mar-2016 10:44:42 WIB','memvalidasi','proses dengan nomor : 3 ;dari pemohon dengan nomor : 3',1);
INSERT INTO `log` VALUES (20,'Saturday, 05-Mar-2016 10:45:22 WIB','memvalidasi','proses dengan nomor : 2 ;dari pemohon dengan nomor : 2',1);
INSERT INTO `log` VALUES (21,'Saturday, 05-Mar-2016 10:57:46 WIB','logout',NULL,1);
INSERT INTO `log` VALUES (22,'Saturday, 05-Mar-2016 10:57:54 WIB','login',NULL,1);
INSERT INTO `log` VALUES (23,'Saturday, 05-Mar-2016 10:58:41 WIB','logout',NULL,1);
INSERT INTO `log` VALUES (24,'Saturday, 05-Mar-2016 10:58:52 WIB','login',NULL,2);
INSERT INTO `log` VALUES (25,'Saturday, 05-Mar-2016 11:07:43 WIB','logout',NULL,2);
INSERT INTO `log` VALUES (26,'Saturday, 05-Mar-2016 11:07:49 WIB','login',NULL,1);
INSERT INTO `log` VALUES (27,'Saturday, 05-Mar-2016 11:08:04 WIB','logout',NULL,1);
INSERT INTO `log` VALUES (28,'Saturday, 05-Mar-2016 11:08:11 WIB','login',NULL,2);
INSERT INTO `log` VALUES (29,'Saturday, 05-Mar-2016 11:08:27 WIB','logout',NULL,2);
INSERT INTO `log` VALUES (30,'Saturday, 05-Mar-2016 11:08:59 WIB','login',NULL,2);
INSERT INTO `log` VALUES (31,'Saturday, 05-Mar-2016 11:09:03 WIB','logout',NULL,2);
INSERT INTO `log` VALUES (32,'Saturday, 05-Mar-2016 11:09:09 WIB','login',NULL,1);
INSERT INTO `log` VALUES (33,'Saturday, 05-Mar-2016 11:09:27 WIB','logout',NULL,1);
INSERT INTO `log` VALUES (34,'Saturday, 05-Mar-2016 11:09:33 WIB','login',NULL,2);
INSERT INTO `log` VALUES (35,'Saturday, 05-Mar-2016 11:20:54 WIB','logout',NULL,2);
INSERT INTO `log` VALUES (36,'Saturday, 05-Mar-2016 11:21:07 WIB','login',NULL,1);
UNLOCK TABLES;

-- Table structure for table `monitoring`

DROP TABLE IF EXISTS `monitoring`;
CREATE TABLE `monitoring` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomorPermohonan` bigint(20) DEFAULT NULL,
  `nomorIppt` varchar(100) DEFAULT NULL,
  `tglIppt` varchar(100) DEFAULT NULL,
  `dalamProsesIppt` varchar(100) DEFAULT NULL,
  `nomorDokumenLingkungan` varchar(100) DEFAULT NULL,
  `tglDokumenLingkungan` varchar(100) DEFAULT NULL,
  `dalamProsesDokumenLingkungan` varchar(100) DEFAULT NULL,
  `nomorAmdalLalin` varchar(100) DEFAULT NULL,
  `tglAmdalLalin` varchar(100) DEFAULT NULL,
  `dalamProsesAmdalLalin` varchar(100) DEFAULT NULL,
  `nomorKkop` varchar(100) DEFAULT NULL,
  `tglKkop` varchar(100) DEFAULT NULL,
  `dalamProsesKkop` varchar(100) DEFAULT NULL,
  `nomorImb` varchar(100) DEFAULT NULL,
  `tglImb` varchar(100) DEFAULT NULL,
  `dalamProsesImb` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorPermohonan` (`nomorPermohonan`),
  CONSTRAINT `monitoring_ibfk_1` FOREIGN KEY (`nomorPermohonan`) REFERENCES `permohonan` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table `monitoring`

LOCK TABLES `monitoring` WRITE;
INSERT INTO `monitoring` VALUES (1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `monitoring` VALUES (2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `monitoring` VALUES (3,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
UNLOCK TABLES;

-- Table structure for table `pemohon`

DROP TABLE IF EXISTS `pemohon`;
CREATE TABLE `pemohon` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `alamat` text,
  `jenisBadanUsaha` varchar(100) DEFAULT 'perseorangan',
  `noAktePerusahaan` text,
  `namaBadanUsaha` text,
  `jabatan` text,
  `alamatBadanUsaha` text,
  `npwp` text,
  `luasBangunan` double DEFAULT NULL,
  `luasTanah` double DEFAULT NULL,
  `nomorJenisBangunan` bigint(20) DEFAULT NULL,
  `alamatLokasi` varchar(100) DEFAULT NULL,
  `rt` tinyint(4) DEFAULT NULL,
  `rw` tinyint(4) DEFAULT NULL,
  `nomorKelurahan` bigint(20) DEFAULT NULL,
  `kota` varchar(10) DEFAULT 'Cimahi',
  `nomorSuratPermohonan` varchar(100) DEFAULT NULL,
  `nomorKtp` varchar(100) DEFAULT NULL,
  `nomorSuratKuasa` varchar(100) DEFAULT NULL,
  `nomorAktePendirian` varchar(100) DEFAULT NULL,
  `nomorPbb` varchar(100) DEFAULT NULL,
  `nomorProposalProyek` varchar(100) DEFAULT NULL,
  `nomorRincianLahan` varchar(100) DEFAULT NULL,
  `nomorSuratPernyataan` varchar(100) DEFAULT NULL,
  `nomorSuratRencanaPembangunan` varchar(100) DEFAULT NULL,
  `nomorSuratDomisiliPerusahaan` varchar(100) DEFAULT NULL,
  `nomorSuratPemberitahuanTetangga` varchar(100) DEFAULT NULL,
  `nomorSertifikatTanah` varchar(100) DEFAULT NULL,
  `tahunSertifikatTanah` varchar(100) DEFAULT NULL,
  `luasTanahSertifikatTanah` double DEFAULT NULL,
  `atasNamaSertifikatTanah` varchar(100) DEFAULT NULL,
  `jenisKepemilikanTanahSertifikatTanah` varchar(100) DEFAULT NULL,
  `nomorDisposisiSekda` varchar(100) DEFAULT NULL,
  `nomorDisposisiKaBappeda` varchar(100) DEFAULT NULL,
  `nomorDisposisiKabid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorKelurahan` (`nomorKelurahan`),
  KEY `nomorJenisBangunan` (`nomorJenisBangunan`),
  CONSTRAINT `pemohon_ibfk_1` FOREIGN KEY (`nomorKelurahan`) REFERENCES `kelurahan` (`nomor`) ON DELETE CASCADE,
  CONSTRAINT `pemohon_ibfk_2` FOREIGN KEY (`nomorJenisBangunan`) REFERENCES `jenisbangunan` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table `pemohon`

LOCK TABLES `pemohon` WRITE;
INSERT INTO `pemohon` VALUES (1,'Joni Iskandar','','perseorangan','','','','','',0,0,1,'',0,0,1,'Cimahi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','');
INSERT INTO `pemohon` VALUES (2,'Bambang','','perseorangan','','','','','',0,0,1,'',0,0,1,'Cimahi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','');
INSERT INTO `pemohon` VALUES (3,'Ismail','','perseorangan','','','','','',0,0,1,'',0,0,1,'Cimahi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','');
UNLOCK TABLES;

-- Table structure for table `penerimakuasa`

DROP TABLE IF EXISTS `penerimakuasa`;
CREATE TABLE `penerimakuasa` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `alamat` text,
  `noHp` text,
  `nomorPemohon` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorPemohon` (`nomorPemohon`),
  CONSTRAINT `penerimakuasa_ibfk_1` FOREIGN KEY (`nomorPemohon`) REFERENCES `pemohon` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table `penerimakuasa`

LOCK TABLES `penerimakuasa` WRITE;
UNLOCK TABLES;

-- Table structure for table `permohonan`

DROP TABLE IF EXISTS `permohonan`;
CREATE TABLE `permohonan` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `tglPermohonan` varchar(100) DEFAULT NULL,
  `tglBerkasMasuk` varchar(100) DEFAULT NULL,
  `pembahasanRapatBKPRD` text,
  `tglPembahasan` varchar(100) DEFAULT NULL,
  `tglTinjauanLapangan` varchar(100) DEFAULT NULL,
  `validasi1` tinyint(1) DEFAULT NULL,
  `validasi2` tinyint(1) DEFAULT NULL,
  `validasi3` tinyint(1) DEFAULT NULL,
  `izinPrinsip` varchar(100) DEFAULT NULL,
  `nomorNotaDinasPenolakan` varchar(100) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `nomorPemohon` bigint(20) DEFAULT NULL,
  `namaPemohon` varchar(100) DEFAULT NULL,
  `beritaAcaraLapangan` varchar(100) DEFAULT NULL,
  `jenisKegiatan` text,
  `nomorIzinPrinsip` varchar(100) DEFAULT NULL,
  `tglIzinPrinsip` varchar(100) DEFAULT NULL,
  `penerimaPerizinan` varchar(100) DEFAULT NULL,
  `tglPenerimaPerizinan` varchar(100) DEFAULT NULL,
  `tglIzinLokasi` varchar(100) DEFAULT NULL,
  `nomorIzinLokasi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorPemohon` (`nomorPemohon`),
  CONSTRAINT `permohonan_ibfk_1` FOREIGN KEY (`nomorPemohon`) REFERENCES `pemohon` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table `permohonan`

LOCK TABLES `permohonan` WRITE;
INSERT INTO `permohonan` VALUES (1,'05/03/2016','','','','',NULL,NULL,NULL,'dalam proses',NULL,0,0,1,'Joni Iskandar','Ada','','','','','','','');
INSERT INTO `permohonan` VALUES (2,'05/03/2016','','','','',NULL,0,NULL,'dalam proses','fsafas312fsaf',0,0,2,'Bambang','Ada','','','','','','','');
INSERT INTO `permohonan` VALUES (3,'05/03/2016','','','','',1,1,1,'dalam proses','',0,0,3,'Ismail','Ada','','','','','','','');
UNLOCK TABLES;

-- Table structure for table `role`

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `nomor` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` text,
  `password` varchar(300) DEFAULT '$2y$10$WpOufCbskJ47nqxtqkpKUepq.cRUPSwCesKRisotw9jtc9vpfE0OG',
  `nomorJenisRole` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`nomor`),
  KEY `nomorJenisRole` (`nomorJenisRole`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`nomorJenisRole`) REFERENCES `jenisrole` (`nomor`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table `role`

LOCK TABLES `role` WRITE;
INSERT INTO `role` VALUES (1,'indonesia','$2y$10$WpOufCbskJ47nqxtqkpKUepq.cRUPSwCesKRisotw9jtc9vpfE0OG',1);
INSERT INTO `role` VALUES (2,'sicustom','$2y$10$WpOufCbskJ47nqxtqkpKUepq.cRUPSwCesKRisotw9jtc9vpfE0OG',4);
UNLOCK TABLES;
SET FOREIGN_KEY_CHECKS = 1;

-- Completed on: 2016-03-05T11:21:21+07:00
