<?php
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class JenisRoleModel extends EupbModel {

	public $nomor;
	public $nama;
	public $dapatTambahPemohon;
	public $dapatEditPemohon;
	public $dapatHapusPemohon;
	public $dapatValidasiPermohonan;
	public $dapatTambahPermohonan;
	public $dapatEditPermohonan;
	public $dapatHapusPermohonan;
	public $dapatEditMonitoring;
	public $dapatHapusMonitoring;
	public $dapatTambahBerkas;
	public $dapatEditBerkas;
	public $dapatHapusBerkas;
	public $dapatTambahPenerimaKuasa;
	public $dapatEditPenerimaKuasa;
	public $dapatHapusPenerimaKuasa;
	public $dapatEditPersyaratan;
	public $dapatKonfigurasi;

	public function __construct() {
		parent::__construct('JenisRole');
	}

}