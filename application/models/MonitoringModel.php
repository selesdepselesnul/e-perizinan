<?php
require_once 'EupbModel.php'; 
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class MonitoringModel extends EupbModel {
	
	public $nomor;
	public $nomorPermohonan;
	public $nomorIppt;
	public $tglIppt;
	public $dalamProsesIppt;
	public $nomorDokumenLingkungan;
	public $tglDokumenLingkungan;
	public $dalamProsesDokumenLingkungan;
	public $nomorAmdalLalin;
	public $tglAmdalLalin;
	public $dalamProsesAmdalLalin;
	public $nomorKkop;
	public $tglKkop;
	public $dalamProsesKkop;
	public $nomorImb;
	public $tglImb;
	public $dalamProsesImb;

	public function __construct() {
		parent::__construct('Monitoring');
	}
}