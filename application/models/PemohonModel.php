	<?php
	require_once 'EupbModel.php'; 
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class PemohonModel extends EupbModel {

	public $nomor;  
	public $nama;  
	public $alamat;    
	public $namaBadanUsaha; 
	public $jabatan; 
	public $alamatBadanUsaha;  
	public $npwp;
	public $jenisBadanUsaha;
	public $noAktePerusahaan;
	public $luasBangunan;
	public $luasTanah;
	public $alamatLokasi;
	public $nomorJenisBangunan;
	public $rt;
	public $rw;
	public $nomorKelurahan;
	public $kota;
	public $nomorSuratPermohonan;
	public $nomorKtp;
	public $nomorSuratKuasa;
	public $nomorAktePendirian;
	public $nomorPbb;
	public $nomorProposalProyek;
	public $nomorRincianLahan;
	public $nomorSuratPernyataan;
	public $nomorSuratRencanaPembangunan;
	public $nomorSuratDomisiliPerusahaan;
	public $nomorSuratPemberitahuanTetangga;
	public $nomorSertifikatTanah;
	public $tahunSertifikatTanah; 
	public $luasTanahSertifikatTanah;
	public $atasNamaSertifikatTanah; 
	public $jenisKepemilikanTanahSertifikatTanah; 
	public $nomorDisposisiSekda;
	public $nomorDisposisiKaBappeda;
	public $nomorDisposisiKabid;

	public function __construct() {
		parent::__construct('Pemohon');
	}

	public function whereNameLike($pattern_name) {
		return $this->db
		->like('nama', $pattern_name, 'after')
		->get('Pemohon')
		->result();  
	}

	public function findPersyaratanById($nomor) {
		$pemohon = $this->findById($nomor);
		return (object) [
		'nomorPemohon' => $pemohon->nomor, 
		'namaPemohon' => $pemohon->nama,
		'nomorSuratPermohonan' => $pemohon->nomorSuratPermohonan,
		'nomorKtp' => $pemohon->nomorKtp,
		'nomorSuratKuasa' => $pemohon->nomorSuratKuasa,
		'nomorAktePendirian' => $pemohon->nomorAktePendirian,
		'nomorPbb' => $pemohon->nomorPbb,
		'nomorProposalProyek' => $pemohon->nomorProposalProyek,
		'nomorRincianLahan' => $pemohon->nomorRincianLahan,
		'nomorSuratPernyataan' => $pemohon->nomorSuratPernyataan,
		'nomorSuratRencanaPembangunan' => $pemohon->nomorSuratRencanaPembangunan,
		'nomorSuratDomisiliPerusahaan' => $pemohon->nomorSuratDomisiliPerusahaan,
		'nomorSuratPemberitahuanTetangga' => $pemohon->nomorSuratPemberitahuanTetangga,
		'nomorSertifikatTanah' => $pemohon->nomorSertifikatTanah,
		'tahunSertifikatTanah' => $pemohon->tahunSertifikatTanah,
		'luasTanahSertifikatTanah' => $pemohon->luasTanahSertifikatTanah,
		'atasNamaSertifikatTanah' => $pemohon->atasNamaSertifikatTanah,
		'jenisKepemilikanTanahSertifikatTanah' => $pemohon->jenisKepemilikanTanahSertifikatTanah
		];
	}


}