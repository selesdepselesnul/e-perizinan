<?php
require_once 'EupbModel.php'; 
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class PersyaratanModel extends EupbModel {  
	
    public $nomor; 
    public $nomorSuratPermohonan;
    public $nomorKtp;
    public $nomorSuratKuasa;
    public $nomorAktePendirian;
    public $nomorSertifikatTanah;
    public $nomorPbb;
    public $nomorProposalProyek;
    public $nomorRincianLahan;
    public $nomorSuratPernyataan;
    public $nomorSuratRencanaPembangunan;
    public $nomorSuratDomisiliPerusahaan;
    public $nomorSuratPemberitahuanTetangga;
    public $nomorPemohon;

    public function __construct() {
        parent::__construct('Persyaratan');
    }

    public function getAll() {
        return $this->db->get('Persyaratan')->result();
    }

    public function wherePemohonID($nomorPemohon) {
        return $this->db
        ->get_where(
            'Persyaratan', 
            ['nomorPemohon' => $nomorPemohon])
        ->result();
    }
}