<?php
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class RoleModel extends EupbModel {

	public $nomor;
	public $username;
	public $nomorJenisRole;

	public function __construct() {
		parent::__construct('Role');
	}

	public function getRole($username, $password) {
		$roleModel =  $this->db
		->get_where(
			'Role',
			['username' => $username])
		->custom_row_object(0, 'RoleModel');
		if(is_null($roleModel))
			return NULL;
		else
			return password_verify($password, $roleModel->password) ? $roleModel : NULL;
	}
}
