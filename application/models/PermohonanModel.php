<?php
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class PermohonanModel extends EupbModel {

	public $nomor;
	public $nomorPemohon;
	public $namaPemohon;
	public $latitude;
	public $longitude;
	public $validasi1;
	public $validasi2;
	public $validasi3;
	public $beritaAcaraLapangan;
	public $jenisKegiatan;
	public $tglIzinPrinsip;
	public $nomorIzinPrinsip;
	public $tglPermohonan;
	public $izinPrinsip;
	public $tglBerkasMasuk;
	public $pembahasanRapatBKPRD;
	public $tglPembahasan;
	public $tglTinjauanLapangan;
	public $penerimaPerizinan;
	public $tglPenerimaPerizinan;
	public $nomorNotaDinasPenolakan;
	public $tglIzinLokasi;
	public $nomorIzinLokasi;


	public function __construct() {
		parent::__construct('Permohonan');
	}

	private function whereValidation($value) {
		return count($this->db
			->get_where(
				'Permohonan',
				[
				'validasi1'=> $value,
				'validasi2' => $value,
				'validasi3' => $value
				])
			->result());
	}

	public function countNotValidate($numberOfValidation) {
		return $this->whereValidation($numberOfValidation, NULL);
	}

	public function countDenyByValidation($numberOfValidation) {
		return $this->whereValidation($numberOfValidation, FALSE);
	}

	public function getValidationStatus($nomor) {
		$permohonan = $this->findById($nomor);
		if ((is_null($permohonan->validasi1) && is_null($permohonan->validasi2) && is_null($permohonan->validasi3))
            || ($permohonan->validasi1 == 1 && is_null($permohonan->validasi2) && is_null($permohonan->validasi3))
			|| (is_null($permohonan->validasi1) && $permohonan->validasi2 == 1 && is_null($permohonan->validasi1))
            || (is_null($permohonan->validasi1) && is_null($permohonan->validasi2) && $permohonan->validasi3 == 1))
			return 'dalam proses';
		else if($permohonan->validasi1 == 0 || $permohonan->validasi2 == 0 || $permohonan->validasi3 == 0)
			return 'ditolak';
		else
			return 'diterima';
	}

	public function countValidation() {
		return [
		'completed' => $this->whereValidation(true),
		'inProcessing' => $this->db
			->where(
			 '(validasi1 IS NULL AND validasi2 IS NULL AND validasi3 IS NULL)'.
			 ' OR (validasi1 = 1 AND validasi2 IS NULL AND validasi3 IS NULL)'.
			 ' OR (validasi1 IS NULL AND validasi2 = 1 AND validasi3 IS NULL)'.
			 ' OR (validasi1 IS NULL AND validasi2 IS NULL AND validasi3 = 1)')
			->count_all_results('Permohonan'),
		'cancel' => $this->db
			->where('validasi1 =', FALSE)
			->or_where('validasi2 =', FALSE)
			->or_where('validasi3 =', FALSE)
			->count_all_results('Permohonan'),
		'all' => $this->permohonanModel->count()
		];
	}

	public function countAcceptByValidation($numberOfValidation) {
		return $this->whereValidation($numberOfValidation, TRUE);
	}

	public function getAll() {
		return $this->db->get('Permohonan')->result();
	}

	private function getFirstOfYear($order) {
		$obj =
		$this->db
		->select('tglPermohonan')
		->order_by('nomor', $order)
		->limit(1)
		->get('Permohonan')
		->row(1);
		if($obj)
			return explode('/', $obj->tglPermohonan)[2];
		else
			return '';
	}

	public function getFirstYear() {
		return $this->getFirstOfYear('ASC');
	}

	public function onYear($year) {
		return $this->db
		->like('tglPermohonan', $year, 'before')
		->get('Permohonan')->result();
	}

	public function onMonthYear($month, $year) {
		return $this->db
		->like('tglPermohonan', $month.'/'.$year, 'before')
		->get('Permohonan')->result();
	}

	public function getLastYear() {
		return $this->getFirstOfYear('DESC');
	}

}
