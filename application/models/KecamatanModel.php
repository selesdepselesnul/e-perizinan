<?php
require_once 'EupbModel.php';  
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class KecamatanModel extends EupbModel {
	public $nomor;
	public $nama;
	
	public function __construct() {
		parent::__construct('Kecamatan');
	}
}