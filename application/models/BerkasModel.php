<?php
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class BerkasModel extends EupbModel {

	public $nomor;
	public $nama;
	public $nomorPemohon;
	public $ext;

	public function __construct() {
		parent::__construct('Berkas');
	}

	public function wherePemohonId($nomorPemohon) {
		return $this->db
		->get_where(
			'Berkas',
			['nomorPemohon' => $nomorPemohon])
		->result();
	}

}
