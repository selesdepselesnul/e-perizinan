<?php  
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PenerimaKuasaModel extends EupbModel {
	
	public $nomor; 
	public $nama;
	public $alamat; 
	public $noHp; 
	public $nomorPemohon;   

	public function __construct() {
		parent::__construct('PenerimaKuasa');
	}

	public function wherePemohonID($nomorPemohon) {
		return $this->db
					->get_where(
						'PenerimaKuasa', 
						['nomorPemohon' => $nomorPemohon])
					->result();
	}

}