<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
abstract class EupbModel extends CI_Model {
	private $table_name;

	public function __construct($table_name) {
		parent::__construct();
		$this->table_name = $table_name;
	}

	public function save() {
		$this->db->insert(
			$this->table_name,
			$this);
	}

	public function getAll() {
		return $this->db
					->get($this->table_name)
					->result();
	}

	public function update() {
		$this->db->update(
			$this->table_name,
			$this,
			"nomor = ".$this->nomor);
	}

	private function getAllIdModel() {
		return $this->db
		->select('nomor')
		->get($this->table_name)
		->result();
	}

	public function getAllId() {
		return array_map(
			function ($x) {
				return $x->nomor;
			}, $this->getAllIdModel());
	}

	public function findById($nomor) {
		return $this->db
		->get_where(
			$this->table_name,
			['nomor' => $nomor])
		->custom_row_object(0, $this->table_name.'Model');
	}

	public function isExists($field, $data) {
		return !is_null($this->db
		->get_where(
			$this->table_name,
			[$field => $data])
		->custom_row_object(0, $this->table_name.'Model'));
	}

	public function getLimit($limit, $pivot) {
		if($pivot == 0)
			return
		$this->db
		->limit($limit)
		->get($this->table_name)
		->result();

		return
		$this->db
		->limit($limit, $pivot)
		->get($this->table_name)
		->result();
	}






	public function count() {
		return $this->db->count_all($this->table_name);
	}

	public function delete($nomor) {
		$this->db->delete(
			$this->table_name,
			['nomor' => $nomor]);
	}

	public function getLastNomor() {
		return $this->db->insert_id();
	}
}
