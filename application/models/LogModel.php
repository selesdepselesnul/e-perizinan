<?php
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class LogModel extends EupbModel {

	public $nomor; 
	public $waktu; 
	public $kegiatan; 
	public $data; 
	public $nomorRole; 

	public function __construct() {
		parent::__construct('Log');
	}

	public function whereRoleID($nomorRole) {
		return $this->db
		->get_where(
			'Log', 
			['nomorRole' => $nomorRole])
		->result();
	}

}