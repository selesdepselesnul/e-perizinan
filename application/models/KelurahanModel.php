<?php
require_once 'EupbModel.php';
/**
 * @author : Moch Deden (https://giuthub.com/selesdepselesnul)
 */
class KelurahanModel extends EupbModel {

	public $nomor;
	public $nama;

	public function __construct() {
		parent::__construct('Kelurahan');
	}

	public function whereKecamatanId($nomorKecamatan) {
		return $this->db
		->get_where(
			'Kelurahan', 
			['nomorKecamatan' => $nomorKecamatan])
		->result();
	}

}