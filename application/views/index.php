<h2>
<span class="glyphicon glyphicon-stats">
<?php if(isset($header))
	echo " $header"; ?>
</span>
</h2>
<hr/>
<h2>
	<kbd>
		Jumlah Proses : <span id="allProsesCount"><span>
	</kbd>
</h2>
<div id="prosesStatisticDraggable" class="ui-widget-content">
	<div id="prosesStatisticTabs">
		<ul>
			<li><a href="#permohonanChartTab-1">#</a></li>
			<li><a href="#permohonanChartTab-2">%</a></li>
		</ul>
		<div id="permohonanChartTab-1">
				<div class="row">
					<canvas id="permohonanBarChart" width="600" height="600" class="col-md-offset-3"></canvas>
				</div>
		</div>

		<div id="permohonanChartTab-2">
			<div class="row">
				<div class="col-md-offset-2">

					<canvas id="permohonanPieChart" width="800" height="600"></canvas>
					<div id="permohonanPieChartLegend" class="chart-legend"></div>

				</div>
			</div>

		</div>
	</div>
</div>

</div>
<script>
	$("#prosesStatisticTabs").tabs()
	$("#prosesStatisticDraggable").draggable()

	$(document).ready(function() {
		$.getJSON(BASE_URL+'json/permohonan/countvalidation')
		.done(function(xs) {
			$('#allProsesCount').text(xs.all)
			const ctxBar = $("#permohonanBarChart").get(0).getContext("2d")
			var barChartData = {
				labels: ["Selesai", "Ditolak", "Dalam Proses"],
				datasets: [{
						label: "Jumlah",
						fillColor: "rgba(151,187,205,0.5)",
						strokeColor: "rgba(151,187,205,0.8)",
						highlightFill: "rgba(151,187,205,0.75)",
						highlightStroke: "rgba(151,187,205,1)",
						data: [xs.completed, xs.cancel, xs.inProcessing]
				}]
			}
			const prosesNumBarChart = new Chart(ctxBar).Bar(barChartData, {

				scaleBeginAtZero : true,


				scaleShowGridLines : true,


				scaleGridLineColor : "rgba(0,0,0,.05)",


				scaleGridLineWidth : 1,


				scaleShowHorizontalLines: true,

				scaleShowVerticalLines: true,


				barShowStroke : true,


				barStrokeWidth : 2,


				barValueSpacing : 5,

				barDatasetSpacing : 1,


				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>"
			})

			const toPercentValidation = (val) => parseFloat(((val / xs.all) * 100).toFixed(2))

			// For a pie chart
			var pieChartData = [
			    {
			        value: toPercentValidation(xs.cancel),
			        color:"#F7464A",
			        highlight: "#FF5A5E",
			        label: "Ditolak"
			    },
			    {
			        value: toPercentValidation(xs.completed),
			        color: "#46BFBD",
			        highlight: "#5AD3D1",
			        label: "Diterima"
			    },
			    {
			        value: toPercentValidation(xs.inProcessing),
			        color: "#FDB45C",
			        highlight: "#FFC870",
			        label: "Dalam Proses"
			    }
			]
			const ctxPieChart = $("#permohonanPieChart").get(0).getContext("2d")
			var permohonanPieChart = new Chart(ctxPieChart).Pie(pieChartData,{
			    //Boolean - Whether we should show a stroke on each segment
			    segmentShowStroke : true,

			    //String - The colour of each segment stroke
			    segmentStrokeColor : "#fff",

			    //Number - The width of each segment stroke
			    segmentStrokeWidth : 2,

			    //Number - The percentage of the chart that we cut out of the middle
			    percentageInnerCutout : 0, // This is 0 for Pie charts

			    //Number - Amount of animation steps
			    animationSteps : 100,

			    //String - Animation easing effect
			    animationEasing : "easeOutBounce",

			    //Boolean - Whether we animate the rotation of the Doughnut
			    animateRotate : true,

			    //Boolean - Whether we animate scaling the Doughnut from the centre
			    animateScale : false,

				tooltipTemplate: "<%= value %>%",

			    //String - A legend template
			    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

			})

			$('#permohonanPieChartLegend').html(permohonanPieChart.generateLegend());
		})
	})
</script>
