<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="<?php echo site_url('assets/js/jquery-2.1.4.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/main.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/jquery-ui.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/underscore-min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/dataTables.buttons.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/jszip.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/pdfmake.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/vfs_fonts.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/buttons.html5.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/buttons.print.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/buttons.colVis.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/Chart.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/datepicker-indonesia.js'); ?>"></script>
  <script>
    const DEFAULT_DIALOG_OPTION = {
      autoOpen: false,
      modal: true,
      dragable: true,
      height: 280,
      width: 1200,
      show: {
        effect: 'blind',
        duration: 1000
      },
      hide: {
        effect: 'explode',
        duration: 1000
      }
    };
    const BASE_URL = "<?php echo site_url(); ?>";
    const role = <?php echo json_encode($this->roleModel->findById($this->session->role['nomor'])); ?>;
    function default_lang(data_name) {
      return {
        buttons: {
          pageLength: {
            _: "Pilih baris",
            '-1': "Semua baris"
          }
        },
        lengthMenu: "Menampilkan _MENU_" + data_name + " per halaman",
        zeroRecords: "Tidak ada " + data_name + " yang ditemukan",
        info: "Halaman _PAGE_ dari _PAGES_",
        infoEmpty: "Data " + data_name + " Kosong",
        infoFiltered: "(Diseleksi dari _MAX_ total data" + data_name +")",
        searchPlaceholder: "Masukan data " + data_name,
        search: "Cari " + data_name + " :",
        paginate: {
          previous: "Sebelumnya",
          next : "Berikutnya"
        }
      }
    }

    $(document).ready(() => {
        $('#listData').click(x => {
            $('[class="active"]').attr('class', '')
            if($('#sidebar').attr('class') === 'container-fluid collapse') {
                $('#listData').attr('class', 'active')
                $('#mainContent').attr('class','container col-sm-offset-2 col-md-offset-2')

                if (/.*\/pemohon.*/.test(window.location.pathname))
                    $('#pemohon').attr('class', 'active')
                else if (/.*permohonan.*/.test(window.location.pathname))
                    $('#permohonan').attr('class', 'active')
                else if (/.*rekapdatapemohon.*/.test(window.location.pathname))
                    $('#rekapdatapemohon').attr('class', 'active')
                else if (/.*monitoring.*/.test(window.location.pathname))
                    $('#monitoring').attr('class', 'active')

            } else {
                $('#listData').attr('class', '')
                $('#mainContent').attr('class','container-fluid')
            }

            // console.log('click data')
        })
        $(document).tooltip()
    })
  </script>

  <title><?php echo $title; ?></title>
  <!-- Bootstrap -->
  <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/dashboard.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jquery-ui.theme.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jquery.dataTables.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/buttons.dataTables.min.css'); ?>" rel="stylesheet">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>
     <?php

     $laporan_activated = "";
     $penerimakuasa_activated = "";
     $pemohon_activated = "";
     $persyaratan_activated = "";
     $permohonan_activated = "";
     $latlong_activated = "";
     $datamaster_activated = "";
     $profile_activated = "";
     $monitoring_activated = "";


    if ($activating=="laporan")
      $laporan_activated = 'class="active"';
    else if($activating =="penerimakuasa")
      $penerimakuasa_activated = 'class="active"';
    else if ($activating == "pemohon")
      $pemohon_activated = 'class="active"';
    else if ($activating == "persyaratan")
      $persyaratan_activated = 'class="active"';
    else if ($activating == "latlong")
      $latlong_activated = 'class="active"';
    else if ($activating == "permohonan")
      $permohonan_activated = 'class="active"';
    else if ($activating == "datamaster")
      $datamaster_activated = 'class="active"';
    else if ($activating == "profile")
      $profile_activated = 'class="active"';
    else if ($activating == "monitoring")
      $monitoring_activated = 'class="active"';


    ?>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
       <div class="navbar-header">
        <a href="<?php echo site_url();?>">
          <img src="<?php echo site_url('assets/pic/lambangcimahismall.png')?>">
        </a>
        <kbd class="well" style="background-color: #f0ad4e; color: #000; border-color: transparent;">
          <b>Badan Perencanaan Pembangunan Daerah -Kota Cimahi-</b></kbd>
        </div>

        <div id="navbar" class="navbar-collapse collapse">

          <ul class="nav navbar-nav navbar-right">
              <li id="listData">
               <a title="aktivasi sidebar" data-toggle="collapse" data-target="#sidebar">
                <span class="glyphicon glyphicon-th-list"></span>
              </a>
            </li>
           <li <?php echo $latlong_activated;?>>
            <a title="map" href="<?php echo site_url('latlong');?>">
             <span class="glyphicon glyphicon-map-marker"></span>
           </a>
         </li>

   <li class="dropdown <?php echo (empty($profile_activated) && empty($datamaster_activated)) ? '' : ' active' ; ?>">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-user"></i>
                        <?php echo $this->session->role['username'];?> <b class="caret">

                        </b></a>
                       <ul class="dropdown-menu">
                           <li <?php echo $profile_activated;?>>
                               <a href="<?php echo site_url('profile');?>"><span class=" glyphicon glyphicon-user"></span> Profile</a>
                           </li>

                           <?php
                           $role = $this->roleModel->findById($this->session->role['nomor']);
                           if ($this->jenisRoleModel->findById($role->nomorJenisRole)->dapatKonfigurasi == 1)
                            echo '<li '.$datamaster_activated.'><a href="'.site_url('datamaster').'">'
                          .'<span class="glyphicon glyphicon-cog"></span> Data Master'.
                          '</a></li>';
                          ?>
                           <li class="divider"></li>
                           <li>
                               <a href="<?php echo site_url('logout');?>"><span class="glyphicon glyphicon-off"></span> Log Out</a>
                           </li>
                       </ul>
                   </li>
 </ul>
</div>
</div>
</nav>

<div class="container-fluid collapse" id="sidebar">
  <div class="row">
   <div class="col-sm-2 col-md-2 sidebar">
    <ul class="nav nav-sidebar">


    <li id="pemohon">
      <a href="<?php echo site_url('pemohon');?>">
       Pemohon
     </a>
   </li>
   <li id="permohonan">
     <a href="<?php echo site_url('permohonan');?>">Proses</a></li>
     <li id="rekapdatapemohon">
       <a href="<?php echo site_url('rekapdatapemohon');?>">Laporan</a></li>
       <li id="monitoring">
         <a href="<?php echo site_url('monitoring');?>">Monitoring</a></li>
       </ul>

       <div class="well" style="background-color: #f0ad4e">
        <a href="https://github.com/selesdepselesnul" style="color: #000">
          <h7 style="color: #000;margin-bottom: 0px;">e-Perizinan v1.0</a>
            <span class="glyphicon glyphicon-copyright-mark"></span>
            <?php  echo '2016'; ?></h7>
          </div>
        </div>
      </div>
    </div>
    <div class="main">
     <div id="mainContent" class="container-fluid">
      <?php
      $this->load->view($page);
      ?>
     </div>
  </div>
</div>
</div>
</div>
</div>
</body>
</html>
