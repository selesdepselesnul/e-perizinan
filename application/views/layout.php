<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="<?php echo site_url('assets/js/jquery-2.1.4.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo site_url('assets/js/main.js'); ?>"></script>
  <script>
    const BASE_URL = "<?php echo site_url(); ?>";
  </script>
  <title><?php echo $title; ?></title>
  <!-- Bootstrap -->
  <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <div class="container">
      <body>
        <?php
        if(isset($header))
          echo "<h2>$header</h2><hr/>";
        
        $this->load->view($page); 
        ?>
      </body>
    </div>
    </html>