<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>
	<?php

	echo validation_errors();
	if(is_null($berkas)) {
		$is_hidden = "";
		$nama = '';
		$ext = '';
		$url = site_url('berkas/'.$nomor_pemohon);
	} else {
		$is_hidden = "hidden";
		$nama = $berkas->nama;
		$ext = $berkas->ext;
		$url = site_url('berkas/'.$nomor_pemohon.'/'.$berkas->nomor);
	}

	if(!is_null($error))
		array_walk($error, function ($item, $key){
			echo $key.' : '.$item.'<br/>';
		});
	echo form_open_multipart($url, ['class' => 'form-horizontal']);
	?>
	<fieldset class="scheduler-border">

		<legend class="scheduler-border">Dari Pemohon</legend>

		<div class="form-group">
			<label for="nomor_pemohon" class="control-label col-sm-2">Nomor</label>
			<div class="col-sm-4">
				<input name="nomor_pemohon" class="form-control" type="number"
				value="<?php echo $nomor_pemohon; ?>" disabled />
			</div>
		</div>

		<div class="form-group">
			<label for="nama_pemohon" class="control-label col-sm-2">Nama</label>
			<div class="col-sm-4">
				<input name="nama_pemohon" class="form-control" type="text"
				value="<?php echo $nama_pemohon; ?>" disabled />
			</div>
		</div>

	</fieldset>

	<div class="form-group">
		<label for="nama" class="control-label col-sm-2">Nama Berkas</label>
		<div class="col-sm-4">
			<input name="nama" class="form-control" type="text"
			value="<?php echo $nama; ?>" />
		</div>

		<div class="col-md-2 col-sm-4">
				<select name="ext" class="form-control">
					<?php
					$this->eupbElement->generateOptionNonObj(
						['.jpg', '.pdf', '.png'],
						$ext);
						?>
					</select>
		</div>
	</div>

	<div class="form-group" <?php echo $is_hidden; ?>>
		<label for="berkas_file" class="control-label col-sm-2">Berkas</label>
		<div class="col-sm-4">
			<input name="berkas_file" type="file"/>
			<p class="help-block">Pilih berkas yang hendak disimpan untuk pemohon
				<?php echo $nama_pemohon; ?>
			</p>
		</div>

	</div>


	<button type="submit" class="btn btn-primary col-sm-2"/>
	<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</form>
</div>
