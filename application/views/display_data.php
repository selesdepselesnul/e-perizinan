<select name="data_kind" id="dataKind" class="form-control">
	<option id="placeHolder">Pilih jenis data...</option>
	<option value="pemohon">Pemohon</option>
	<option value="penerimakuasa">Penerima kuasa</option>
	<option value="suratkuasapemberi">Surat kuasa pemberi</option>
	<option value="syarat">Syarat</option>
	<option value="permohonan">Permohonan</option>
</select>
<input type="text" id="dataId" class="form-control" disabled />
<div class="table-responsive">
<table id="dataTable" class="table">
</table>
</div>
<script>
	const BASE_URL = "<?php echo site_url(); ?>";
</script>
<script src="<?php echo site_url('assets/js/display-data.js'); ?>"></script>
