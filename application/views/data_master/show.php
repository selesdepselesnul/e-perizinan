<div id="dataMasterDraggable" class="ui-widget-content">
	<div id="dataMasterTabs">
		<ul>
			<li><a href="#tabs-1">Role</a></li>
			<li><a href="#tabs-2">Jenis Role</a></li>
			<li><a href="#tabs-3">Kecamatan</a></li>
			<li><a href="#tabs-4">Jenis Bangunan</a></li>
			<li><a href="#tabs-5">Database</a></li>
		</ul>
		<div id="tabs-1">
			<div class="container">
				<table id="roleTable" class="display">
						<thead>
							<tr>
								<th>Nomor</th>
								<th>Username</th>
								<th>Jenis Role</th>
								<th>Log</th>
								<th>Edit</th>
								<th>Hapus</th>
							</tr>
						</thead>
				</table>
			</div>
			<br/>
			<div class="form-group">
				<div class="col-sm-3 col-sm-offset-2">
					<input id="usernameRoleInput" type="text" class="form-control"
					placeholder="username" name="usernameRole"/>
				</div>
				<div class="col-sm-3">
					<select id="jenisRoleInput" class="form-control"
					name="jenisRole">
				</select>
			</div>
			<div class="col-sm-1">
				<button id="roleButton" class="form-control">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</div>
		</div>
		<br/>
	</div>

	<div id="tabs-2">
		<table id="jenisRoleTable" class="display">
			<thead>
				<tr>
					<th>Jenis Role</th>
					<th>Kewenangan</th>
					<th>Hapus</th>
				</tr>
			</thead>
		</table>
		<br/>
		<div class="form-group">
			<div class="col-sm-3 col-sm-offset-4">
				<input id="namaJenisRoleInput" type="text" class="form-control"
				placeholder="jenis role"/>
			</div>

			<div class="col-sm-1">
				<button id="jenisroleButton" class="form-control">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</div>
		</div>
		<br/>
	</div>



	<div id="tabs-3">
		<table id="kecamatanTable" class="display">
			<thead>
				<tr>
					<th>Kecamatan</th>
					<th>Kelurahan</th>
					<th>Edit</th>
					<th>Hapus</th>
				</tr>
			</thead>
		</table>
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<input id="kecamatanInput" type="text" class="form-control" placeholder="nama kecamatan"/>
			</div>
			<div class="col-sm-1">
				<button id="kecamatanButton" class="form-control"><span class="glyphicon glyphicon-plus"></span></button>
			</div>
		</div>
		<br/>
	</div>

	<div id="tabs-4">
		<table id="jenisBangunanTable" class="display">
			<thead>
				<tr>
					<th>Jenis</th>
					<th>Edit</th>
					<th>Hapus</th>
				</tr>
			</thead>
		</table>
		<br/>
		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-4">
				<input type="text" id="jenisbangunanInput" class="form-control"
				placeholder="jenis bangunan"/>
			</div>
			<div class="col-sm-1">
				<button class="form-control adding-button" id="jenisbangunanButton">
					<span class="glyphicon glyphicon-plus"></span>
				</button>
			</div>
		</div>
		<br/>
	</div>

	<div id="tabs-5">
		<br/>
		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Export</legend>
			<div class="form-group">
				<div class="col-sm-4 col-md-4">
					<input type="text" id="exportFile" class="form-control"
					placeholder="nama file"/>
				</div>
				<div class="col-sm-1 col-md-1">
					<button class="form-control adding-button"
						id="databaseExportButton">
						<span class="glyphicon glyphicon-save-file"></span>
					</button>
				</div>
			</div>
		</fieldset>
		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Import</legend>
			<div class="form-group">
				<div class="col-sm-4 col-md-4">
					<input type="text" id="importFile" class="form-control"
					placeholder="nama file"/>
				</div>
				<div class="col-sm-1 col-md-1">
					<button class="form-control adding-button"
						id="databaseImportButton">
						<span class="glyphicon glyphicon-open-file"></span>
					</button>
				</div>
			</div>
		</fieldset>
		<br/>
		<br/>
		<br/>
	</div>

</div>
</div>

<div id="kelurahanDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="kelurahanTable" class="display">
				<thead>
					<tr>
						<th>Kelurahan</th>
						<th>Edit</th>
						<th>Hapus</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<div id="logDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="logTable" class="display">
				<thead>
					<tr>
						<th>Waktu</th>
						<th>Kegiatan</th>
						<th>Data</th>
						<th>Hapus</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<script>
	$(document).ready(() => {

		const writeJenisRole = () => {
			$.when($.ajax(BASE_URL+'json/jenisrole')).done((x) => {
				const xs = $.parseJSON(x)
				const allJenisRole = xs.data
				console.log(allJenisRole)
				$('.jenis-role').remove()
				_.each(allJenisRole, (x) => {
					console.log(x)
					$('#jenisRoleInput').append('<option class="jenis-role" value="'
						+ x.nomor +'">'
						+ x.nama +'</option>')
				})
			})
		}
		writeJenisRole()

		$('#databaseExportButton').click(() => {
			$('.database-dumping').remove()
			$.post(BASE_URL + 'json/database/export',{
				'file_name' : $('#exportFile').val()
			}).done((x) => {
				$('#tabs-5').append(
					'<h2 class="alert alert-success database-dumping" '+
					'role="alert">Database berhasil di dumping dengan nama : '+
					$.parseJSON(x)+'.sql</h2>')
				$('#exportFile').val('')
			})
		})

		$('#databaseImportButton').click(() => {
			$('.database-dumping').remove()
			$.post(BASE_URL + 'json/database/import',{
				'file_name' : $('#importFile').val()
			}).done((x) => {
				$('#tabs-5').append(
					'<h2 class="alert alert-success database-dumping" '+
					'role="alert">Database pada file '+
					$.parseJSON(x)+'.sql berhasil di import</h2>')
				$('#importFile').val('')
			})
		})

		const newDataHandler = (suplier, dataName, buttonName, initializer, finalizer, nullField) => {
			$('#'+buttonName).click(() => {
				if(nullField())
					console.log('field tidak boleh kosong')
				else
					$.post(
						BASE_URL + 'json/' + dataName,
						suplier())
					.done((x) => {
						console.log(x)
						if(x !== '-1') {
							initializer()
							finalizer()
						}
					})
			})
		}

		const initJenisBangunan = () => {
			$('#jenisBangunanTable').DataTable().destroy()
			$('#jenisBangunanTable').DataTable({
				language: default_lang('jenis bangunan'),
				ajax: BASE_URL + '/json/jenisbangunan/',
				processing: true,
				columns: [
				{ data : "nama" },
				{ data : null,
					orderable: false,
					render : function(x) {
						return '<button title="perbaharui data jenis bangunan '
						+ x.nama + '" id="'+x.nama+'" class="btn btn-warning"'
						+ 'onclick="onEditing(\'jenisbangunan\','+x.nomor+')">'
						+'<span class="glyphicon'
						+' glyphicon-pencil"></span>'
						+'</button>'
					}
			},
			{ data : null,
				orderable: false,
				render : function(x) {
					return '<button title="hapus data jenis bangunan ' + x.nama
					+'" id="removingButton_'+ x.nomor
					+'" class="removing-button '
					+'btn btn-danger" onclick="onRemovingWithFinalizer(\'jenisbangunan\', '
					+ x.nomor + ', redirectToDiv(\'datamaster\', \'tabs-4\'))">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'
				}
			}],
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
			dom: 'Bfrtip',
			buttons: [
			'pageLength',
			{
				extend: 'colvis',
				text: 'Pilih Kolom'

			},
			{
				extend: 'excelHtml5',
				text: 'Excel',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			},
			{
				extend: 'print',
				text: 'Print',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			}]
})
}

onListKelurahan = (nomor, nama) => {
	DEFAULT_DIALOG_OPTION.title = 'Daftar Kelurahan di Kecamatan : ' + nama
	$('#kelurahanDialog').dialog(DEFAULT_DIALOG_OPTION)
	$("title").text('Daftar Kelurahan di Kecamatan ' + nama)
	$('#kelurahanDialog').dialog('open')
	const initKelurahan = () => {
		$('#kelurahanTable').DataTable().destroy()
		$('#kelurahanTable').DataTable({
			language: default_lang('kelurahan'),
			ajax: BASE_URL + 'json/kelurahan/wherekecamatanid/'+nomor,
			processing: true,
			columns: [
			{ data : "nama" },
			{ data : null,
				orderable: false,
				render : function(x) {
					return '<button title="perbaharui data kelurahan ' + x.nama
					+ '" class="editing-button btn btn-warning" '
					+ 'onclick="onEditing(\'kelurahan/' + x.nomorKecamatan
					+ '\', ' +x.nomor+')">'
					+ '<span class="glyphicon'
					+ ' glyphicon-pencil"></span>'
					+ '</button>'
				}
			},
			{ data : null,
				orderable: false,
				render : function(x) {
					return '<button title="hapus data kelurahan ' + x.nama
					+'" id="removingButton_'+ x.nomor
					+'" class="removing-button '
					+'btn btn-danger" onclick="onRemovingWithFinalizer(\'kelurahan\', '+ x.nomor
					+', redirectToDiv(\'datamaster\', \'tabs-3\'))">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'
				}
			}],
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
			dom: 'Bfrtip',
			buttons: [
			{
				text: 'Tambah',
				action: function () {
					onAdding('kelurahan/'+nomor)
				}
			},
			'pageLength',

			{
				extend: 'colvis',
				text: 'Pilih Kolom'

			},
			{
				extend: 'excelHtml5',
				text: 'Excel',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			},
			{
				extend: 'print',
				text: 'Print',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			}]
})

}
initKelurahan()
newDataHandler(
	() => ({ nama : $('#kelurahanInput').val(),
		nomorKecamatan: nomor }),
	'kelurahan',
	'kelurahanButton',
	() => {
		$('#kelurahanInput').val('')
	},
	() => {
		window.open(BASE_URL+'/datamaster#tabs-3', '_self')
	})

}


onListLog = (nomor, nama) => {
	DEFAULT_DIALOG_OPTION.title = 'Daftar Log : ' + nama
	$('#logDialog').dialog(DEFAULT_DIALOG_OPTION)
	$("title").text('Daftar Log : ' + nama)
	$('#logDialog').dialog('open')

	const initLog = () => {
		$('#logTable').DataTable().destroy()
		$('#logTable').DataTable({
			language: default_lang('log'),
			ajax: BASE_URL + 'json/log/whereroleid/'+nomor,
			processing: true,
			columns: [
			{ data : "waktu" },
			{ data : "kegiatan"},
			{ data : "data"},
			{ data : null,
			  orderable: false,
			  render : function(x) {
					return '<button title="hapus log ' + x.waktu
					+'" id="removingButton_'+ x.nomor
					+'" class="removing-button '
					+'btn btn-danger" onclick="onRemovingWithFinalizer(\'log\', '+ x.nomor
					+', redirectToDiv(\'datamaster\', \'tabs-1\'))">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'
			}
			}],
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
			dom: 'Bfrtip',
			buttons: [
			'pageLength',
			{
				extend: 'colvis',
				text: 'Pilih Kolom'

			},
			{
				extend: 'excelHtml5',
				text: 'Excel',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			},
			{
				extend: 'print',
				text: 'Print',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			}]
		})

	}

	initLog()
}

const initKecamatan = () => {
	$('#kecamatanTable').DataTable().destroy()
	$('#kecamatanTable').DataTable({
		language: default_lang('kecamatan'),
		ajax: BASE_URL + 'json/kecamatan/',
		processing: true,
		columns: [
		{ data : "nama" },
		{ data : null,
			orderable: false,
			render : function(x) {
				$('#kelurahanDialog').dialog({
					autoOpen: false,
					modal: true,
					dragable: true,
					height: 280,
					width: 1200,
					show: {
						effect: 'blind',
						duration: 1000
					},
					hide: {
						effect: 'explode',
						duration: 1000
					}
				})
				return '<button class="btn btn-default" title="daftar kelurahan di kecamatan '
				 + x.nama + ' "onclick="onListKelurahan('+x.nomor+', \''+x.nama
				 +'\')">kelurahan</button>'
			}
		},
		{ data : null,
		  orderable: false,
		  render : function(x) {
				return '<button title="perbaharui data kecamatan ' + x.nama
				+ '" id="editingKecamatan_'
				+ x.nomor +'" class="editing-button '
				+ 'btn btn-warning" onclick="onEditing(\'kecamatan\','+x.nomor+')">'
				+'<span class="glyphicon'
				+' glyphicon-pencil"></span>'
				+'</button>'
			}
		},
		{ data : null,
		  orderable: false,
		  render : function(x) {
				return '<button title="hapus data kecamatan ' + x.nama
				+'" id="removingButton_'+ x.nomor
				+'" class="removing-button '
				+'btn btn-danger" onclick="onRemovingWithFinalizer(\'kecamatan\', '+ x.nomor
				+', redirectToDiv(\'datamaster\', \'tabs-3\'))">'
		+'<span class="glyphicon'
		+' glyphicon-trash">'
		+'</span></button>'
		}
		}],
		lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
		dom: 'Bfrtip',
		buttons: [
		'pageLength',
		{
			extend: 'colvis',
			text: 'Pilih Kolom'

		},
		{
			extend: 'excelHtml5',
			text: 'Excel',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		},
		{
			extend: 'print',
			text: 'Print',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		}]
	})
}

const initRole = () => {


	$('#roleTable').DataTable().destroy()
	$('#roleTable').DataTable({
		language: default_lang('role'),
		ajax: BASE_URL + 'json/role/',
		processing: true,
		columns: [
		{ data : "nomor"},
		{ data : "username" },
		{ data : "jenis" },
		{ data : null,
			orderable: false,
			render : function(x) {
				return '<button title="' + "log data " + x.username
				+'" class="btn btn-default" onclick="onListLog('+x.nomor+', \''+x.username+'\')">'
				+'<span class="glyphicon glyphicon-book">'
				+'</span></button>'
			}
		},
		{ data : null,
			orderable: false,
			render : function(x) {
				return '<button title="' + 'perbaharui data ' + x.username
				+ '" id="editingKecamatan_'
				+ x.nomor +'" class="editing-button '
				+ 'btn btn-warning" onclick="onEditing(\'role\','+x.nomor+')">'
				+'<span class="glyphicon'
				+' glyphicon-pencil"></span>'
				+'</button>'
			}
		},
		{ data : null,
			orderable: false,
			render : function(x) {
				return '<button title="hapus data ' + x.username
				+'" id="removingButton_'+ x.nomor
				+'" class="removing-button '
				+'btn btn-danger" onclick="onRemoving(\'role\', '+ x.nomor+', \'datamaster\')">'
				+'<span class="glyphicon'
				+' glyphicon-trash">'
				+'</span></button>'
			}
		}],
		lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
		dom: 'Bfrtip',
		buttons: [
		'pageLength',
		{
			extend: 'colvis',
			text: 'Pilih Kolom'

		},
		{
			extend: 'excelHtml5',
			text: 'Excel',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		},
		{
			extend: 'print',
			text: 'Print',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		}]
	})
}

const initJenisRole = () => {


	$('#jenisRoleTable').DataTable().destroy()
	$('#jenisRoleTable').DataTable({
		language: default_lang('jenis role'),
		ajax: BASE_URL + 'json/jenisrole/',
		processing: true,
		columns: [
		{ data: 'nama' },
		{ data : null,
			orderable: false,
			render : (x) => '<button title="perbaharui kewenangan ' + x.nama
			+'" id="editingButton_'
			+x.nomor+'" class="editing-button '
			+'btn btn-warning" onclick="onEditing(\'jenisrole\', '+ x.nomor+', \'datamaster\')">'
			+'<span class="glyphicon glyphicon-flag"></span>'
			+'</button>'

		},
		{ data : null,
			orderable: false,
			render : function(x) {
				return '<button title="hapus data ' + x.nama
				+'" id="removingButton_'+ x.nomor
				+'" class="removing-button '
				+'btn btn-danger" onclick="onRemovingWithFinalizer(\'jenisrole\', '+ x.nomor
				+', redirectToDiv(\'datamaster\', \'tabs-2\'))">'
				+'<span class="glyphicon'
				+' glyphicon-trash">'
				+'</span></button>'
			}
		}],
		lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
		dom: 'Bfrtip',
		buttons: [
		'pageLength',
		{
			extend: 'colvis',
			text: 'Pilih Kolom'

		},
		{
			extend: 'excelHtml5',
			text: 'Excel',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		},
		{
			extend: 'print',
			text: 'Print',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		}]
	})
}

initJenisBangunan()
initKecamatan()
initRole()
initJenisRole()

$("#dataMasterTabs").tabs()
$("#dataMasterDraggable").draggable()



newDataHandler(
	() => ({ nama : $('#jenisbangunanInput').val() }),
	'jenisbangunan',
	'jenisbangunanButton',
	initJenisBangunan,
	() => {$('#jenisbangunanInput').val('')},
	() => $('#jenisbangunanInput').val() === '')

newDataHandler(
	() => ({ nama : $('#kecamatanInput').val() }),
	'kecamatan',
	'kecamatanButton',
	initKecamatan,
	() => {$('#kecamatanInput').val('')},
	() => $('#kecamatanInput').val() === '')

newDataHandler(
	() => ({
		usernameRole : $('#usernameRoleInput').val(),
		namaRole : $('#namaRoleInput').val(),
		jenisRole : $('#jenisRoleInput').val()
	}),
	'role',
	'roleButton',
	initRole,
	() => {
		$('#usernameRoleInput').val('')
		$('#namaRoleInput').val('')
	},
	() => $('#usernameRoleInput').val() === '' || $('#namaRoleInput').val() === '')

newDataHandler(
	() => ({ nama : $('#namaJenisRoleInput').val() }),
	'jenisrole',
	'jenisroleButton',
	initJenisRole,
	() => {
		$('#namaJenisRoleInput').val('')
		writeJenisRole()
	},
	() => $('#namaJenisRoleInput').val() === '')

})


</script>
