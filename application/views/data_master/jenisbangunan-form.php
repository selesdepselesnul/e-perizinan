<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>

	<?php
	echo form_open(site_url('jenisbangunan/'.$jenis_bangunan->nomor), ["class" => "form-horizontal"]);

	if(isset($message))
		echo '<div class="form-group"><div class="col-sm-4 col-sm-offset-2"><div class="alert alert-danger" role="alert">
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			  <span class="sr-only">Error:</span>'.
			  $message.
			'</div></div></div>'
	?>
	<div class="form-group">
		<label for="nama" class="control-label col-sm-2">
        	Jenis Bangunan
        </label for="nama">
			<div class="col-sm-4">
				<input name="nama" class="form-control" type="text"
				value="<?php echo $jenis_bangunan->nama;?>"/>
			</div>
		</div>
		<input name="old_nama" class="form-control" type="hidden"
				value="<?php echo $jenis_bangunan->nama;?>"/>
	</div>
	<button type="submit" class="btn btn-primary col-sm-2"/>
	<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</form>
