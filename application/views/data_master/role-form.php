<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>

	<?php
	echo form_open(site_url('role/'.$role->nomor), ["class" => "form-horizontal"]);
	if(isset($message))
		echo '<div class="form-group"><div class="col-sm-4 col-sm-offset-2"><div class="alert alert-danger" role="alert">
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			  <span class="sr-only">Error:</span>'.
			  $message.
			'</div></div></div>'
	?>
	<div class="form-group">
		<label for="nama" class="control-label col-sm-2">
            Username
        </label for="username">
			<div class="col-sm-4">
				<input name="username" class="form-control" type="text"
				value="<?php echo $role->username;?>"/>
			</div>
		</div>
		<input name="old_username" class="form-control" type="hidden"
				value="<?php echo $role->username;?>"/>
        <div class="form-group">
            <label for="nomor_jenis_role" class="control-label col-sm-2">
                Jenis Role
            </label>
            <div class="col-md-2 col-sm-4">
                <select name="nomor_jenis_role" class="form-control">
                    <?php
                    $this->eupbElement->generateOption(
                        $all_jenis_role,
                        $role->nomorJenisRole);
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="control-label col-sm-2">
                    Password
                </label>
                <div class="col-md-2 col-sm-4">
                    <input name="password" class="form-control" type="password"
                    value="<?php echo $role->password;?>"/>
                </div>
            </div>
			<input name="old_password" class="form-control" type="hidden"
                    value="<?php echo $role->password;?>"/>
	</div>
	<button type="submit" class="btn btn-primary col-sm-2"/>
	<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</form>
