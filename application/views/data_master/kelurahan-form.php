<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>
	<?php
	if(is_null($kelurahan)) {
		$nama = '';
		$url = site_url('kelurahan/'.$nomor_kecamatan);
	} else {
		$nama = $kelurahan->nama;
		$url = site_url('kelurahan/'.$kelurahan->nomorKecamatan.'/'.$kelurahan->nomor);
	}
	echo form_open($url, ["class" => "form-horizontal"]);
	?>

	<div class="form-group">
		<label for="nama" class="control-label col-sm-2">
            Nama Kelurahan
        </label for="nama">
			<div class="col-sm-4">
				<input name="nama" class="form-control" type="text"
				value="<?php echo $nama;?>"/>
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-primary col-sm-2"/>
	<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</form>
