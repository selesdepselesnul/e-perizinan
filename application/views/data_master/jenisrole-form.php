<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>

	<?php
	// echo validation_errors();
	echo form_open(site_url('jenisrole/'.$jenisrole->nomor), ["class" => "form-horizontal"]);
	if(isset($message))
	echo '<div class="form-group"><div class="col-sm-4 col-sm-offset-2"><div class="alert alert-danger" role="alert">
		  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		  <span class="sr-only">Error:</span>'.
		  $message.
		'</div></div></div>'
	?>


	<div class="form-group">
		<label for="nama" class="control-label col-sm-1" disabled>Nama</label for="nama">
			<div class="col-sm-4">
				<input name="nama" class="form-control" type="text"
				value="<?php echo $jenisrole->nama;?>"/>
			</div>
		</div>

		<input name="old_nama" class="form-control" type="hidden"
				value="<?php echo $jenisrole->nama;?>"/>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Pemohon</legend>

			<div class="form-group">
				<button class="col-sm-1" title="menambahkan data Pemohon" disabled>Tambah</button>
				<div class="col-sm-4">
					<input name="dapat_tambah_pemohon" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatTambahPemohon)?>/>
				</div>
			</div>

			<div class="form-group">
				<div class="btn-preview-div editing-div col-sm-1" title="memperbaharui data Pemohon" disabled>
					<span class="glyphicon glyphicon-pencil"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_edit_pemohon" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatEditPemohon)?>/>
				</div>
			</div>


			<div class="form-group">
				<div class="btn-preview-div deleting-div col-sm-1" title="menghapus data Pemohon" disabled>
					<span class="glyphicon glyphicon-trash"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_hapus_pemohon" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatHapusPemohon)?>/>
				</div>
			</div>
		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Penerima Kuasa</legend>
			<div class="form-group">
				<button class="col-sm-1" title="menambahkan data Penerima Kuasa" disabled>
					Tambah
				</button>
				<div class="col-sm-4">
					<input name="dapat_tambah_penerimakuasa" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatTambahPenerimaKuasa)?>/>
				</div>
			</div>

			<div class="form-group">
			<div class="btn-preview-div editing-div col-sm-1" title="memperbaharui data Penerima Kuasa" disabled>
					<span class="glyphicon glyphicon-pencil"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_edit_penerimakuasa" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatEditPenerimaKuasa)?>/>
				</div>
			</div>


			<div class="form-group">
				<div class="btn-preview-div deleting-div col-sm-1" title="menghapus data Penerima Kuasa" disabled>
					<span class="glyphicon glyphicon-trash"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_hapus_penerimakuasa" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatHapusPenerimaKuasa)?>/>
				</div>
			</div>


		</fieldset>


		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Berkas</legend>
			<div class="form-group">
				<button class="col-sm-1" title="menambahkan data Berkas" disabled>Tambah</button>
				<div class="col-sm-4">
					<input name="dapat_tambah_berkas" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatTambahBerkas)?>/>
				</div>
			</div>

			<div class="form-group">
				<div class="btn-preview-div editing-div col-sm-1" title="memperbaharui data Berkas" disabled>
					<span class="glyphicon glyphicon-pencil"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_edit_berkas" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatEditBerkas)?>/>
				</div>
			</div>


			<div class="form-group">
				<div class="btn-preview-div deleting-div col-sm-1" title="menghapus data Berkas" disabled>
					<span class="glyphicon glyphicon-trash"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_hapus_berkas" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatHapusBerkas)?>/>
				</div>
			</div>


		</fieldset>



		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Persyaratan</legend>


			<div class="form-group">
				<div class="btn-preview-div editing-div col-sm-1" title="memperbaharui data Persyaratan"disabled>
					<span class="glyphicon glyphicon-pencil"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_edit_persyaratan" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatEditPersyaratan)?>/>
				</div>
			</div>
		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Proses</legend>

			<div class="form-group">
				<button class="col-sm-1" title="menambahkan data Proses" disabled>Tambah</button>
				<div class="col-sm-4">
					<input name="dapat_tambah_permohonan" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatTambahPermohonan)?>/>
				</div>
			</div>

			<div class="form-group">
				<div class="btn-preview-div editing-div col-sm-1" title="memperbaharui data Proses" disabled>
					<span class="glyphicon glyphicon-pencil"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_edit_permohonan" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatEditPermohonan)?>/>
				</div>
			</div>


			<div class="form-group">
				<div class="btn-preview-div deleting-div col-sm-1" title="menghapus data Proses" disabled>
					<span class="glyphicon glyphicon-trash"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_hapus_permohonan" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatHapusPermohonan)?>/>
				</div>
			</div>


			<div class="form-group">
				<div class="btn-preview-div validating-div col-sm-1" title="memvalidasi data Proses">
					<span class="glyphicon glyphicon-check"></span>
				</div>
				<div class="col-sm-4">
					<input name="dapat_validasi_permohonan" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatValidasiPermohonan)?>/>
				</div>
			</div>

		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Monitoring</legend>

			<div class="form-group">
				<div class="btn-preview-div editing-div col-sm-1" title="memperbaharui data Monitoring" disabled>
					<span class="glyphicon glyphicon-pencil"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_edit_monitoring" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatEditMonitoring)?>/>
				</div>
			</div>


			<div class="form-group">
				<div class="btn-preview-div deleting-div col-sm-1" title="menghapus data Monitoring" disabled>
					<span class="glyphicon glyphicon-trash"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_hapus_monitoring" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatHapusMonitoring)?>/>
				</div>
			</div>
		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Data Master</legend>
			<div class="form-group">
				<div class="btn-preview-div setting-div col-sm-1" title="mengelola Data Master" disabled>
					<span class="glyphicon glyphicon-cog"></span>
				</div>

				<div class="col-sm-4">
					<input name="dapat_konfigurasi" class="form-control" type="checkbox"
					<?php echo $eupbElement->intToCheckBoxVal($jenisrole->dapatKonfigurasi)?>/>
				</div>
			</div>
		</fieldset>



	</div>
	<button type="submit" class="btn btn-primary col-sm-2"/>
	<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</form>
