<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row"></div>
	</div>
	<?php
	echo validation_errors();
	echo form_open(
		site_url('permohonan/validate/'.$permohonan->nomor),
		['class' => 'form-horizontal']);
		?>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Dari Pemohon</legend>
			<div class="form-group">
				<label for="nomor_pemohon" class="control-label col-sm-2">
					Nama
				</label>
				<div class="col-sm-4">
					<input
					type="text"
					name="nomor_pemohon"
					class="form-control"
					value="<?php echo $permohonan->namaPemohon;?>"
					disabled />
				</div>
			</div>

			<div class="form-group">
				<label for="nomor_pemohon" class="control-label col-sm-2">
					Nomor
				</label>
				<div class="col-sm-4">
					<input
					type="text"
					name="nomor_pemohon"
					class="form-control"
					value="<?php echo $permohonan->nomorPemohon;?>"
					disabled />
				</div>
			</div>
	</fieldset>

		<fieldset class="scheduler-border">

		<legend class="scheduler-border">Koordinat</legend>

		<div class="form-group">
			<label for="latitude"
			class="control-label col-sm-2">Latitude</label>
			<div class="col-sm-4">
				<input
				type="number"
				name="latitude"
				id="latitude"
				class="form-control"
				value="<?php echo $permohonan->latitude; ?>"
				disabled/>
			</div>
			<label for="longitude" class="control-label col-sm-1">
				Longitude
			</label>
			<div class="col-sm-4">
				<input
				type="number"
				name="longitude"
				id="longitude"
				class="form-control"
				value="<?php echo $permohonan->longitude; ?>"
				disabled />
			</div>
			<label id="searchLabel" class="btn btn-danger"
			onclick="clicked();">
			<span class="glyphicon glyphicon-map-marker"></span>
		</label>
	</div>
	</fieldset>


	<div class="form-group">
		<label for="validasi1" class="control-label col-sm-2">Peruntukan Lahan</label>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					<input type="radio" name="validasi1" id="accept1" value="accept">
					Terima
				</label>
			</div>
		</div>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					<input type="radio" name="validasi1" value="deny">
					Tolak
				</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="validasi2" class="control-label col-sm-2">Perencanaan Kota</label>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					<input type="radio" name="validasi2" id="accept2" value="accept">
					Terima
				</label>
			</div>
		</div>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					<input type="radio" name="validasi2" value="deny">
					Tolak
				</label>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="validasi3" class="control-label col-sm-2">Kesesuaian Teknis</label>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					<input type="radio" name="validasi3" id="accept3" value="accept">
					Terima
				</label>
			</div>
		</div>
		<div class="col-sm-1">
			<div class="radio">
				<label>
					<input type="radio" name="validasi3" value="deny">
					Tolak
				</label>
			</div>
		</div>
	</div>
	<div class="form-group" hidden id="notaDinasPenolakan">
		<label for="nomor_nota_dinas_penolakan" class="control-label col-sm-2">Nomor Nota Penolakan</label>
		<div class="col-sm-4">
			<input name="nomor_nota_dinas_penolakan" type="text" class="form-control">
		</div>
	</div>
</div>

<button type="submit" class="btn btn-primary col-sm-2"/>
<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</div>
</form>
</div>
<script>
	function clicked() {
		console.log('clicked!');
		const latArr = $('#latitude').val().split('.');
		const longArr = $('#longitude').val().split('.');

		var latitude = $('#latitude').val();
		var longitude = $('#longitude').val();

		if(latArr.length > 1){
			latitude = latArr[0]+'_' +latArr[1];
		}

		if(longArr.length > 1) {
			longitude = longArr[0]+'_'+longArr[1];
		}

		const mapUrl = latitude + '/'+ longitude +'/';
		const baseUrl = '<?php echo site_url(); ?>';

		window.open(baseUrl + 'latlong/' +  mapUrl, '_blank');
	}

	$(document).ready(() => {
		$('input[value="deny"]').click(() => {
			$('#notaDinasPenolakan').fadeIn()
		})

		$('input[value="accept"]').click(() => {
			if($('#accept1').is(':checked')
				&& $('#accept2').is(':checked')
				&& $('#accept3').is(':checked'))
				$('#notaDinasPenolakan').fadeOut()
		})
	})
</script>
