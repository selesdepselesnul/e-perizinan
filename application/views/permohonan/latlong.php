<div class="row">
	<div class="form-inline col-md-offset-2">
		<input type="number" placeholder="latitude"
		class="form-control col-sm-2" id="latitude"
		value="<?php echo $latitude; ?>" step="any"/>
		<input type="number" placeholder="longitude" class="form-control"
		id="longitude" value="<?php echo $longitude; ?>" step="any"/>
		<label for="map_type">Jenis map :</label>
		<select id="mapType" name="map_type" class="form-control">
			<option value="roadmap">Roadmap</option>
			<option value="hybrid">Hybrid</option>
		</select>
	</div>
</div>
<div class="row">
	<div id="googleMap" style="width:800px;height:600px;" class="col-md-offset-2"></div>
</div>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="<?php echo site_url('assets/js/latlong.js'); ?>">
</script>
