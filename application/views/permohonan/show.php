<table id="permohonanTable" class="display">
	<thead>
		<tr id="headerPermohonanTable">
			<th>No. Proses</th>
			<th>Tgl Proses</th>
			<th>Tgl Berkas Masuk</th>
			<th>Tgl Pembahasan</th>
			<th>Tgl Tinjauan Lapangan</th>
			<th>Pembahasan Rapat DKPRD</th>
			<th>Latitude</th>
			<th>Longitude</th>
			<th>Berita Acara Lapangan</th>
			<th>Jenis Kegiatan</th>
			<th>Izin prinsip</th>
			<th>Tgl. Izin Prinsip</th>
			<th>Nomor Izin Prinsip</th>
			<th>Tgl. Izin Lokasi</th>
			<th>Nomor Izin Lokasi</th>
			<th>Penerima Perizinan</th>
			<th>Tgl. Penerimaan Perizinan</th>
			<th>Pemohon</th>
			<th>Persyaratan</th>
			<th>Penerima Kuasa</th>
			<th>Peruntukan Lahan</th>
			<th>Perencanaan Kota</th>
			<th>Kesesuaian Teknis</th>
			<th>No. Nota Penolakan</th>
		</tr>
	</thead>
</table>

<div id="pemohonDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="pemohonTable" class="display">
				<thead>
					<tr>
						<th>No. Pemohon</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>Nama badan usaha</th>
						<th>Jabatan</th>
						<th>Alamat badan usaha</th>
						<th>Npwp</th>
						<th>Jenis Badan Usaha</th>
						<th>No Akte Perusahaan</th>
						<th>Luas Bangunan</th>
						<th>Luas Tanah</th>
						<th>Jenis Bangunan</th>
						<th>Alamat Lokasi (Nama Jalan)</th>
						<th>Rt</th>
						<th>Rw</th>
						<th>Kelurahan</th>
						<th>Kecamatan</th>
						<th>Kota</th>
						<th>No. Disposisi Sekda</th>
						<th>No. Disposisi Ka Bappeda</th>
						<th>No. Disposisi Kabid</th>
					</tr>
				</thead>
			</table>

		</div>
	</div>
</div>


<div id="persyaratanDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="persyaratanTable" class="display">
				<thead>
					<tr>
						<th>Surat Permohonan</th>
						<th>Ktp</th>
						<th>Surat Kuasa</th>
						<th>Akte Pendirian</th>
						<th>Nomor (Sertifikat Tanah)</th>
						<th>Tahun (Sertifikat Tanah)</th>
						<th>Luas Tanah (Sertifikat Tanah)</th>
						<th>Atas Nama (Sertifikat Tanah)</th>
						<th>Jenis Kepemilikan Tanah (Sertifikat Tanah)</th>
						<th>Pelunasan Pbb</th>
						<th>Proposal Proyek</th>
						<th>Rincian Lahan</th>
						<th>Surat Pernyataan</th>
						<th>Surat Rencana Pembangunan</th>
						<th>Surat Domisili Perusahaan</th>
						<th>Surat Pemberitahuan Tetangga</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<div id="penerimaKuasaDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="penerimaKuasaTable" class="display">
				<thead>
					<tr>
						<th>No. Penerima Kuasa</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>No Hp</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$(document).tooltip()
		function initPenerimaKuasaTable(nomorPemohon, namaPemohon) {
			$("title").text('Daftar Penerima Kuasa : ' + namaPemohon)
			$("#penerimaKuasaDialog").dialog( "option", "title", "Daftar Penerima Kuasa : " + namaPemohon)
			$('#penerimaKuasaTable').DataTable().destroy()
			$('#penerimaKuasaTable').DataTable({
				language: default_lang('penerima kuasa'),
				ajax: BASE_URL + '/json/penerimakuasa/wherepemohonid/'+nomorPemohon,
				processing: true,
				columns: [
				{ data : "nomor" },
				{ data : "nama" },
				{ data : "alamat" },
				{ data : "noHp" }],
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
				dom: 'Bfrtip',
				buttons: [
				'pageLength',
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]
			})
		}

		const initPersyaratanTable = (pemohonID, namaPemohon) => {
			$("title").text('Persyaratan : ' + namaPemohon)
			$("#persyaratanDialog").dialog( "option", "title", "Daftar Persyaratan : " + namaPemohon)
			$('#persyaratanTable').DataTable().destroy();
			$('#persyaratanTable').DataTable({
				language: default_lang('persyaratan'),
				ajax: BASE_URL + '/json/pemohon/'+pemohonID+'/persyaratan/',
				processing: true,
				searching: false,
				info: false,
				paging: false,
				ordering: false,
				columns: [
					{ data : "nomorSuratPermohonan" },
					{ data : "nomorKtp" },
					{ data : "nomorSuratKuasa" },
					{ data : "nomorAktePendirian" },
					{ data : "nomorSertifikatTanah" },
					{ data: 'tahunSertifikatTanah' },
					{ data: 'luasTanahSertifikatTanah' },
					{ data: 'atasNamaSertifikatTanah' },
					{ data: 'jenisKepemilikanTanahSertifikatTanah' },
					{ data : "nomorPbb" },
					{ data : "nomorProposalProyek" },
					{ data : "nomorRincianLahan" },
					{ data : "nomorSuratPernyataan" },
					{ data : "nomorSuratRencanaPembangunan" },
					{ data : "nomorSuratDomisiliPerusahaan" },
					{ data : "nomorSuratPemberitahuanTetangga" }],
				dom: 'Bfrtip',
				buttons: [
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]
			})
		}


		function initPemohonTable(nomorPemohon, namaPemohon) {
			$("title").text('e-UPB | Pemohon : ' + namaPemohon)
			$("#pemohonDialog").dialog( "option", "title", "Identitas Pemohon : " + namaPemohon)
			$('#pemohonTable').DataTable().destroy()
			$('#pemohonTable').DataTable({
				language: default_lang('pemohon'),
				paging: false,
				ordering: false,
				info: false,
				searching: false,
				ajax: BASE_URL + '/json/pemohon/'+nomorPemohon,
				processing: true,
				columns: [
				{ data : "nomor" },
				{ data : "nama" },
				{ data : "alamat" },
				{ data : "namaBadanUsaha" },
				{ data : "jabatan" },
				{ data : "alamatBadanUsaha" },
				{ data : "npwp" },
				{ data : "jenisBadanUsaha"},
				{ data : "noAktePerusahaan"},
				{ data : "luasBangunan"},
				{ data : "luasTanah"},
				{ data : "jenisBangunan"},
				{ data : "alamatLokasi"},
				{ data : "rt"},
				{ data : "rw"},
				{ data : "kelurahan"},
				{ data : "kecamatan"},
				{ data : "kota"},
				{ data : "nomorDisposisiSekda"},
				{ data : "nomorDisposisiKaBappeda"},
				{ data : "nomorDisposisiKabid"}],
				dom: 'Bfrtip',
				buttons: [
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]
			})
}


onListPenerimaKuasa = function(nomor, nama) {
	initPenerimaKuasaTable(nomor, nama)
	$('#penerimaKuasaDialog').dialog('open')
}

onListPersyaratan = function(nomor, nama) {
	initPersyaratanTable(nomor, nama)
	$('#persyaratanDialog').dialog('open')
}

onListPemohon = function(nomor, nama) {
	initPemohonTable(nomor, nama)
	$('#pemohonDialog').dialog('open')
}

function intToValidation(x) {
	if(x == 0)
		return 'ditolak'
	else if(x == 1)
		return 'selesai'
	else
		return 'dalam proses'
}

const permohonanTable = createTable({
	dataName: 'Permohonan',
	tableName: 'permohonanTable',
	headerName: 'headerPermohonanTable',
	ajaxURL: '/json/permohonan/',
	defaultLang: 'proses',
	addingRoute: 'permohonan',
	defaultCol: [
	{ data : "nomor" },
	{ data : "tglPermohonan" },
	{ data : "tglBerkasMasuk" },
	{ data : "tglPembahasan" },
	{ data : "tglTinjauanLapangan" },
	{ data : "pembahasanRapatBKPRD" },
	{ data : "latitude" },
	{ data : "longitude" },
	{ data : "beritaAcaraLapangan" },
	{ data : "jenisKegiatan" },
	{ data : "izinPrinsip" },
	{ data : "tglIzinPrinsip" },
	{ data : "nomorIzinPrinsip" },
	{ data : "tglIzinLokasi" },
	{ data : "nomorIzinLokasi" },
	{ data : "penerimaPerizinan" },
	{ data : "tglPenerimaPerizinan" },
	{ data : null,
		orderable: false,
		render : function(x) {
			$('#pemohonDialog').dialog({
				autoOpen: false,
				modal: true,
				dragable: true,
				height: 280,
				width: 1200,
				show: {
					effect: 'blind',
					duration: 1000
				},
				hide: {
					effect: 'explode',
					duration: 1000
				}
			})
			return '<button class="btn btn-default" title="daftar pemohon dari '
			+ x.namaPemohon + '" onclick="onListPemohon('+x.nomorPemohon+', \''+x.namaPemohon+'\')">'+x.namaPemohon+'</button>'
		}
	},
	{ data : null,
		orderable: false,
		render : function(x) {
			$('#persyaratanDialog').dialog({
				autoOpen: false,
				modal: true,
				dragable: true,
				height: 280,
				width: 1200,
				show: {
					effect: 'blind',
					duration: 1000
				},
				hide: {
					effect: 'explode',
					duration: 1000
				}
			})
			return '<button class="btn btn-default" title="persyaratan dari pemohon '
			+ x.namaPemohon + '" onclick="onListPersyaratan('
			+x.nomorPemohon+', \''+x.namaPemohon+'\')">persyaratan</button>'
		}
	},
	{ data : null,
		orderable: false,
		render : function(x) {
			$('#penerimaKuasaDialog').dialog({
				autoOpen: false,
				modal: true,
				dragable: true,
				height: 280,
				width: 1200,
				show: {
					effect: 'blind',
					duration: 1000
				},
				hide: {
					effect: 'explode',
					duration: 1000
				}
			})
			return '<button class="btn btn-default"'
			+ ' title="daftar penerima kuasa dari pemohon '
			+ x.namaPemohon + '" onclick="onListPenerimaKuasa('
				+x.nomorPemohon+',\''+x.namaPemohon+'\')">penerima kuasa</button>'
}
},
{ data : null,
	orderable: false,
	render: function(x) {
		return intToValidation(x.validasi1)
	}
},
{ data : null,
	orderable: false,
	render: function(x) {
		return intToValidation(x.validasi2)
	}
},
{ data : null,
	orderable: false,
	render: function(x) {
		return intToValidation(x.validasi3)
	}
},
{data: 'nomorNotaDinasPenolakan'}],
handleValidating: (x) => '<button title="validasi data proses dari pemohon ' + x.namaPemohon
+'" id="editingButton_'
+x.nomor+'" class="validating-button '
+'btn btn-primary" onclick="onValidating('+x.nomor+')">'
+'<span class="glyphicon'
+' glyphicon-check"></span>'
+'</button>',
handleEditing: (x) => '<button title="perbaharui data proses dari pemohon ' + x.namaPemohon
+'" id="editingButton_'
+x.nomor+'" class="editing-button '
+'btn btn-warning" onclick="onEditing(\'permohonan\', '+ x.nomor+')">'
+'<span class="glyphicon'
+' glyphicon-pencil"></span>'
+'</button>',
handleDeleting: (x) => '<button title="hapus data proses dari pemohon ' + x.namaPemohon
+'" id="removingButton_'+ x.nomor
+'" class="removing-button '
+'btn btn-danger" onclick="onRemoving(\'permohonan\', '+ x.nomor+')">'
+'<span class="glyphicon'
+' glyphicon-trash">'
+'</span></button>'
})

})
</script>
