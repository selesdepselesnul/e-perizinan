<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">
			
		</div>	
	</div>
	<?php
	echo validation_errors(); 

	if(is_null($permohonan)) {
		$latitude = '';
		$longitude = '';
		$url = site_url('permohonan');
		$nomor_pemohon = '';
		$berita_acara_lapangan = ''; 
		$jenis_kegiatan = '';
		$tgl_izin_prinsip = '';
		$nomor_izin_prinsip = '';
		$izin_prinsip = '';
		$tgl_berkas_masuk = '';
		$tgl_pembahasan = '';
		$pembahasan_rapat_bkprd = '';
		$tgl_tinjauan_lapangan = '';
		$penerima_perizinan = '';
		$tgl_penerima_perizinan = '';
		$tgl_izin_lokasi = '';
		$nomor_izin_lokasi = '';
	} else {
		$latitude = $permohonan->latitude;
		$longitude = $permohonan->longitude;
		$url = site_url('permohonan/'.$permohonan->nomor);
		$nomor_pemohon = $permohonan->nomorPemohon; 
		$berita_acara_lapangan = $permohonan->beritaAcaraLapangan;
		$jenis_kegiatan = $permohonan->jenisKegiatan;
		$tgl_izin_prinsip = $permohonan->tglIzinPrinsip;
		$nomor_izin_prinsip = $permohonan->nomorIzinPrinsip;
		$izin_prinsip = $permohonan->izinPrinsip;
		$tgl_berkas_masuk = $permohonan->tglBerkasMasuk;
		$tgl_pembahasan = $permohonan->tglPembahasan;
		$pembahasan_rapat_bkprd = $permohonan->pembahasanRapatBKPRD;
		$tgl_tinjauan_lapangan = $permohonan->tglTinjauanLapangan;
		$penerima_perizinan = $permohonan->penerimaPerizinan;
		$tgl_penerima_perizinan = $permohonan->tglPenerimaPerizinan;
		$tgl_izin_lokasi = $permohonan->tglIzinLokasi;
		$nomor_izin_lokasi = $permohonan->nomorIzinLokasi;
	}
	echo form_open($url, ['class' => 'form-horizontal']);
	?>
	
	<fieldset class="scheduler-border">	
		<legend class="scheduler-border">Pemohon</legend>
		<div class="form-group">
			<label for="nama_pemohon" class="control-label col-sm-2">
				Nama 
			</label>
			<div class="col-sm-4">
				<input name="nama_pemohon" id="namaPemohon" 
				type="text" class="form-control" 
				value="<?php echo $nama_pemohon;?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_pemohon" class="control-label col-sm-2">
				Nomor
			</label>
			<div class="col-sm-2">
				<select id="nomorPemohon" name="nomor_pemohon"  class="form-control">
					<?php 
					if($nomor_pemohon != '')
						echo '<option class="nomor-pemohon">'
					.$nomor_pemohon.
					'</option>';

					?>
				</select>
			</div>
		</div>
	</fieldset>
	
	<fieldset class="scheduler-border">
		<legend class="scheduler-border">Izin Prinsip</legend>			
		<div class="form-group">
			<label for="izin_prinsip" class="control-label col-sm-2">
				Izin Prinsip
			</label>
			<div class="col-md-2 col-sm-4">
				<select name="izin_prinsip" class="form-control">
					<?php
					$this->eupbElement->generateOptionNonObj(
						['dalam proses', 'sudah selesai'],
						$izin_prinsip); 
						?>
					</select>
				</div>
			</div>	

			<div class="form-group">
				<label for="tgl_izin_prinsip" class="control-label col-sm-2">
					Tanggal
				</label>
				<div class="col-sm-2">
					<input type="text" id="tglIzinPrinsip" name="tgl_izin_prinsip" 
					class="form-control  date-picker-id" value="<?php echo $tgl_izin_prinsip;?>">
				</div>
			</div>

			<div class="form-group">
				<label for="nomor_izin_prinsip" class="control-label col-sm-2">
					Nomor
				</label>
				<div class="col-sm-2">
					<input name="nomor_izin_prinsip" id="nomorIzinPrinsip" 
					type="text" class="form-control" 
					value="<?php echo $nomor_izin_prinsip;?>"/>
				</div>
			</div>
		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Perizinan</legend>

			<div class="form-group">
				<label for="tgl_penerima_perizinan" class="control-label col-sm-2">
					Tanggal Terima
				</label>
				<div class="col-sm-2">
					<input type="text" id="tglPenerimaPerizinan" name="tgl_penerima_perizinan" 
					class="form-control  date-picker-id" value="<?php echo $tgl_penerima_perizinan;?>">
				</div>
			</div>

			<div class="form-group">
				<label for="penerima_perizinan" class="control-label col-sm-2">
					Penerima 
				</label>
				<div class="col-sm-4">
					<input name="penerima_perizinan" id="penerimaPerizinan" 
					type="text" class="form-control" 
					value="<?php echo $penerima_perizinan;?>"/>
				</div>
			</div>
		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Koordinat</legend>
			<div class="form-group">
				<label for="latitude" class="control-label col-sm-2">Latitude</label>
				<div class="col-sm-3">
					<input type="number" name="latitude" id="latitude" 
					class="form-control" step="any" 
					value="<?php echo $latitude ?>" />
				</div>
				<label for="longitude" class="control-label col-sm-1">Longitude</label>
				<div class="col-sm-3">
					<input type="number" name="longitude" id="longitude" 
					class="form-control" step="any" 
					value="<?php echo $longitude ?>"/>
				</div>
				<label id="searchLabel" class="btn btn-danger col-sm-1" 
				onclick="handleSearchLatlong()">
				<span class="glyphicon glyphicon-map-marker"></span>
			</label>
		</div>
	</fieldset>

	<fieldset class="scheduler-border">
		<legend class="scheduler-border">Izin Lokasi</legend>

		<div class="form-group">
			<label for="tgl_izin_lokasi" class="control-label col-sm-2">
				Tanggal 
			</label>
			<div class="col-sm-2">
				<input type="text" id="tglIzinLokasi" name="tgl_izin_lokasi" 
				class="form-control  date-picker-id" value="<?php echo $tgl_izin_lokasi;?>">
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_izin_lokasi" class="control-label col-sm-2">
				Nomor 
			</label>
			<div class="col-sm-4">
				<input name="nomor_izin_lokasi" id="nomorIzinLokasi" 
				type="text" class="form-control" 
				value="<?php echo $nomor_izin_lokasi;?>"/>
			</div>
		</div>
	</fieldset>

	<div class="form-group">
		<label for="tgl_berkas_masuk" class="control-label col-sm-2">
			Tgl. Berkas Masuk
		</label>
		<div class="col-sm-2">
			<input type="text" name="tgl_berkas_masuk" 
			class="form-control date-picker-id" value="<?php echo $tgl_berkas_masuk;?>">
		</div>
	</div>


	<div class="form-group">
		<label for="tgl_pembahasan" class="control-label col-sm-2">
			Tgl. Pembahasan
		</label>
		<div class="col-sm-2">
			<input type="text" id="tglPembahasan" name="tgl_pembahasan" 
			class="form-control  date-picker-id" value="<?php echo $tgl_pembahasan;?>">
		</div>
	</div>

	<div class="form-group">
		<label for="pembahasan_rapat_bkprd" class="control-label col-sm-2">
			Pembahasan Rapat BKPRD
		</label>
		<div class="col-sm-2">
			<textarea name="pembahasan_rapat_bkprd" rows="10" cols="50"><?php echo $pembahasan_rapat_bkprd;?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label for="tgl_tinjauan_lapangan" class="control-label col-sm-2">
			Tgl. Tinjauan Lapangan
		</label>
		<div class="col-sm-2">
			<input type="text" id="tglTinjauanLapangan" name="tgl_tinjauan_lapangan" 
			class="form-control  date-picker-id" value="<?php echo $tgl_tinjauan_lapangan;?>">
		</div>
	</div>

	<div class="form-group">
		<label for="berita_acara_lapangan" class="control-label col-sm-2">
			Berita acara lapangan
		</label>
		<div class="col-sm-2">
			<select name="berita_acara_lapangan" id="beritaAcaraLapangan" class="form-control">
				<?php 
				$this->eupbElement->generateOptionNonObj(
					['Ada', 'Tidak ada'],
					$berita_acara_lapangan);  
					?>
				</select>
			</div>
		</div>

		
		<div class="form-group">
			<label for="jenis_kegiatan" class="control-label col-sm-2">
				Jenis Kegiatan
			</label>
			<div class="col-sm-2">
				<textarea name="jenis_kegiatan" rows="10" cols="50"><?php echo $jenis_kegiatan;?></textarea>
			</div>
		</div>

		
		<button type="submit" class="btn btn-primary col-sm-2"/>
		<span class="glyphicon glyphicon-floppy-save"></span>
	</button>
</form>
</div>
<script src="<?php echo site_url('assets/js/nomorpemohonhandler.js'); ?>">
</script>
<script>
	function handleSearchLatlong() {
		console.log('clicked!');
		const latArr = $('#latitude').val().split('.');
		const longArr = $('#longitude').val().split('.');

		var latitude = $('#latitude').val();
		var longitude = $('#longitude').val();

		if(latArr.length > 1){
			latitude = latArr[0]+'_' +latArr[1];
		}

		if(longArr.length > 1) {
			longitude = longArr[0]+'_'+longArr[1];
		}

		const mapUrl = latitude + '/'+ longitude +'/';
		const baseUrl = '<?php echo site_url(); ?>';

		window.open(baseUrl + 'latlong/' +  mapUrl, '_blank');
	}
</script>
