<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">
			
		</div>	
	</div>
	<?php 
	echo validation_errors();
	$kecamatan = '';
	$kelurahan = ''; 
	if(is_null($pemohon)) {
		$nama = '';
		$alamat = '';
		$jenis_badan_usaha = '';
		$nama_badan_usaha = '';
		$jabatan = '';
		$alamat_badan_usaha = '';
		$npwp = '';
		$no_akte_perusahaan = '';
		$luas_bangunan = '';
		$luas_tanah = '';
		$alamat_lokasi = '';
		$rt = '';
		$rw = '';
		$jenis_bangunan = '';
		$no_disposisi_sekda = '';
		$no_disposisi_kepala_bappeda = '';
		$no_disposisi_kabid = '';
		$is_hidden = 'hidden';
		$url = site_url('pemohon');	
	} else {
		$nama = $pemohon->nama;
		$alamat = $pemohon->alamat;
		$jenis_badan_usaha = '';
		$nama_badan_usaha = $pemohon->namaBadanUsaha;
		$jabatan = $pemohon->jabatan;
		$alamat_badan_usaha = $pemohon->alamatBadanUsaha;
		$npwp = $pemohon->npwp;
		$luas_bangunan = $pemohon->luasBangunan;
		$luas_tanah = $pemohon->luasTanah;
		$alamat_lokasi = $pemohon->alamatLokasi;
		$rt = $pemohon->rt;
		$rw = $pemohon->rw;
		$jenis_bangunan = $pemohon->nomorJenisBangunan;
		$no_disposisi_sekda = $pemohon->nomorDisposisiSekda;
		$no_disposisi_kepala_bappeda = $pemohon->nomorDisposisiKaBappeda;
		$no_disposisi_kabid = $pemohon->nomorDisposisiKabid;
		$jenis_badan_usaha = $pemohon->jenisBadanUsaha;
		$no_akte_perusahaan = $pemohon->noAktePerusahaan;
		if(strcasecmp($jenis_badan_usaha, 'perseorangan') === 0)
			$is_hidden = 'hidden';
		else
			$is_hidden = '';
		$url = site_url('pemohon/'.$pemohon->nomor);
	}
	echo form_open($url, ["class" => "form-horizontal"]);
	?>	
	<div class="form-group">
		<label for="nama"  class="control-label col-sm-2">Nama</label>
		<div class="col-sm-4">
			<input name="nama" class="form-control" type="text" 
			value="<?php echo $nama; ?>" />
		</div>
	</div>

	<div class="form-group">
		<label for="alamat" class="control-label col-sm-2">Alamat</label>
		<div class="col-sm-4">
			<input name="alamat" type="text" class="form-control" 
			value="<?php echo $alamat; ?>"/>
		</div>
	</div>

	<div class="form-group">
		<label for="jenis_badan_usaha" class="control-label col-sm-2">
			Jenis Badan Usaha
		</label>
		<div class="col-md-2 col-sm-4">
			<select name="jenis_badan_usaha" class="form-control" id="jenisBadanUsaha">
				<?php
				$this->eupbElement->generateOptionNonObj(
					['perseorangan', 'badan hukum'],
					$jenis_badan_usaha); 
					?>
				</select>
			</div>
		</div>	

		<div class="form-group" id="aktePerusahaan" <?php echo " $is_hidden" ?>>
			<label for="no_akte_perusahaan" class="control-label col-sm-2">
				No Akte Perusahaan
			</label>
			<div class="col-sm-2">
				<input name="no_akte_perusahaan" type="text" class="form-control"
				value="<?php echo $no_akte_perusahaan; ?>" />
			</div>
		</div>


		<div class="form-group">
			<label for="nama_badan_usaha" class="control-label col-sm-2">Nama badan usaha</label>
			<div class="col-sm-4">
				<input name="nama_badan_usaha" type="text" class="form-control" 
				value="<?php echo $nama_badan_usaha; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="jabatan" class="control-label col-sm-2">Jabatan</label>
			<div class="col-sm-4">
				<input name="jabatan" type="text" class="form-control" 
				value="<?php echo $jabatan; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="alamat_badan_usaha" class="control-label col-sm-2">Alamat badan usaha</label>
			<div class="col-sm-4">
				<input name="alamat_badan_usaha" type="text" class="form-control" 
				value="<?php echo $alamat_badan_usaha; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="npwp" class="control-label col-sm-2">Npwp</label>
			<div class="col-sm-2">
				<input name="npwp" type="text" class="form-control"
				value="<?php echo $npwp; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="luas_bangunan" class="control-label col-sm-2">
				Luas bangunan
			</label>
			<div class="col-sm-2">
				<div class="input-group">
					<input name="luas_bangunan" type="number" step="any" 
					class="form-control" value="<?php echo $luas_bangunan; ?>"/>
					<div class="input-group-addon">m<sup>2</sup></div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label for="luas_tanah" class="control-label col-sm-2">
				Luas tanah
			</label>
			<div class="col-sm-2">
				<div class="input-group">

					<input name="luas_tanah" type="number" step="any" 
					class="form-control" value="<?php echo $luas_tanah; ?>" />
					<div class="input-group-addon">m<sup>2</sup></div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label for="alamat_lokasi" class="control-label col-sm-2">
				Alamat Lokasi (Nama Jalan)
			</label>
			<div class="col-sm-2">
				<div class="input-group">
					<input name="alamat_lokasi" type="text" class="form-control" 
					value="<?php echo $alamat_lokasi; ?>" />
				</div>
			</div>
		</div>

		<div class="form-group" >
			<label for="rt"  class="control-label col-sm-2">
				Rt
			</label>
			<div class="col-sm-2 col-xs-3">
				<input name="rt" type="number"  
				class="form-control" value="<?php echo $rt; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="rw" class="control-label col-sm-2">
				Rw
			</label>
			<div class="col-sm-2 col-xs-3">
				<input name="rw" type="number"  
				class="form-control" value="<?php echo $rw; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="jenis_bangunan" class="control-label col-sm-2">
				Jenis bangunan
			</label>
			<div class="col-md-2 col-sm-4">
				<select name="jenis_bangunan" class="form-control">
					<?php
					$this->eupbElement->generateOption(
						$all_jenis_bangunan,
						$jenis_bangunan); 
						?>
					</select>
				</div>
			</div>	

			<div class="form-group">
				<label for="kecamatan" class="control-label col-sm-2">Kecamatan</label>
				<div class="col-md-3 col-sm-4">
					<select name="kecamatan" id="kecamatan" class="form-control">
						<?php 
						$this->eupbElement->generateOption(
							$all_kecamatan,
							$kecamatan);  
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="nomor_kelurahan" class="control-label col-sm-2">Kelurahan</label>
					<div class="col-md-3 col-sm-4">
						<select name="nomor_kelurahan" class="form-control" id="kelurahan">
						</select>
					</div>
				</div>


				<div class="form-group">
					<label for="no_disposisi_sekda" class="control-label col-sm-2">
						No Disposisi Sekda
					</label>
					<div class="col-sm-2">
						<input name="no_disposisi_sekda" type="text" class="form-control"
						value="<?php echo $no_disposisi_sekda; ?>" />
					</div>
				</div>

				<div class="form-group">
					<label for="no_disposisi_kepala_bappeda" class="control-label col-sm-2">
						No Disposisi Kepala Bappeda
					</label>
					<div class="col-sm-2">
						<input name="no_disposisi_kepala_bappeda" type="text" class="form-control"
						value="<?php echo $no_disposisi_kepala_bappeda; ?>" />
					</div>
				</div>

				<div class="form-group">
					<label for="no_disposisi_kabid" class="control-label col-sm-2">
						No Disposisi Kabid
					</label>
					<div class="col-sm-2">
						<input name="no_disposisi_kabid" type="text" class="form-control"
						value="<?php echo $no_disposisi_kabid; ?>" />
					</div>
				</div>

			</div>

			<button type="submit" class="btn btn-primary col-sm-2"/>
			<span class="glyphicon glyphicon-floppy-save"></span>
		</button>	
	</form>


	<script src="<?php echo site_url('assets/js/pemohon.js'); ?>">
	</script>
	<script>
		$(document).ready(() => {
			$("#jenisBadanUsaha").change(function() {
				if ($('#jenisBadanUsaha').val() === 'badan hukum')
					$('#aktePerusahaan').fadeIn();
				else
					$('#aktePerusahaan').fadeOut();
			});
		})
	</script>