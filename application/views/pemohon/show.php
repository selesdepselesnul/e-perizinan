<table id="pemohonTable" class="display">
	<div class="container-fluid">
		<thead>
			<tr id="headerPemohonTable">
				<th>No. Pemohon</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Nama badan usaha</th>
				<th>Jabatan</th>
				<th>Alamat badan usaha</th>
				<th>Npwp</th>
				<th>Jenis Badan Usaha</th>
				<th>No Akte Perusahaan</th>
				<th>Luas Bangunan</th>
				<th>Luas Tanah</th>
				<th>Jenis Bangunan</th>
				<th>Alamat Lokasi (Nama Jalan)</th>
				<th>Rt</th>
				<th>Rw</th>
				<th>Kelurahan</th>
				<th>Kecamatan</th>
				<th>Kota</th>
				<th>No. Disposisi Sekda</th>
				<th>No. Disposisi Ka Bappeda</th>
				<th>No. Disposisi Kabid</th>
				<th>Persyaratan</th>
				<th>Penerimakuasa</th>
				<th>Berkas</th>
			</tr>
		</thead>
	</div>
</table>

<div id="persyaratanDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="persyaratanTable" class="display">
				<thead>
					<tr id="headerPersyaratanTable">
						<th>Surat Permohonan</th>
						<th>Ktp</th>
						<th>Surat Kuasa</th>
						<th>Akte Pendirian</th>
						<th>Nomor (Sertifikat Tanah)</th>
						<th>Tahun (Sertifikat Tanah)</th>
						<th>Luas Tanah (Sertifikat Tanah)</th>
						<th>Atas Nama (Sertifikat Tanah)</th>
						<th>Jenis Kepemilikan Tanah (Sertifikat Tanah)</th>
						<th>Pelunasan Pbb</th>
						<th>Proposal Proyek</th>
						<th>Rincian Lahan</th>
						<th>Surat Pernyataan</th>
						<th>Surat Rencana Pembangunan</th>
						<th>Surat Domisili Perusahaan</th>
						<th>Surat Pemberitahuan Tetangga</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<div id="berkasDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="berkasTable" class="display">
				<thead>
					<tr id="headerBerkasTable">
						<th>Nomor</th>
						<th>Nama</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>


<div id="penerimaKuasaDialog" hidden>
	<div class="container">
		<div class="row">
			<table id="penerimaKuasaTable" class="display">
				<thead>
					<tr id="headerPenerimaKuasaTable">
						<th>No. Penerima Kuasa</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>No Hp</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<script src="<?php echo site_url('assets/js/pemohoncontroller.js'); ?>"></script>
