<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="<?php echo site_url('assets/js/jquery-2.1.4.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/main.js'); ?>"></script>
	<script>
		const BASE_URL = "<?php echo site_url(); ?>";
	</script>
	<title><?php echo $title; ?></title>
	<!-- Bootstrap -->
	<link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/signin.css'); ?>" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <div class="container">
     <body>

      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
         <div class="navbar-header">
          <a href="<?php echo site_url();?>">
            <img src="<?php echo site_url('assets/pic/lambangcimahismall.png')?>">
          </a>
          <kbd class="well" style="background-color: #f0ad4e; color: #000; border-color: transparent">
            <b>Badan Perencanaan Pembangunan Daerah -Kota Cimahi-</b></kbd>
          </div>
        </nav>

        <?php

        if($is_wrong)
          $is_hidden = "";
        else
          $is_hidden = "hidden";

        echo validation_errors();

        echo form_open(site_url('login'), ['class' =>"form-signin"]);

        ?>



        <h2 class="form-signin-heading">Login</h2>
        <label for="username" class="sr-only">Username</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="username" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

        <div class="alert alert-danger" role="alert" <?php echo $is_hidden ?>>
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Username atau Password Salah !
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>

      </form>

    </body>
  </div>
  </html>
