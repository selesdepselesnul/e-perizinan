<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>
	<?php

	echo validation_errors();
	if(is_null($penerimakuasa)) {
		$nama = '';
		$alamat = '';
		$no_hp = '';
		$url = site_url('penerimakuasa/'.$nomor_pemohon);
	} else {
		$nama = $penerimakuasa->nama;
		$alamat = $penerimakuasa->alamat;
		$no_hp = $penerimakuasa->noHp;
		$url = site_url('penerimakuasa/'.$nomor_pemohon.'/'.$penerimakuasa->nomor);
	}


	echo form_open($url, ['class' => 'form-horizontal']);
	?>
	<fieldset class="scheduler-border">
		<legend class="scheduler-border">Dari Pemohon</legend>
		<div class="form-group">
			<label for="nama_pemohon" class="control-label col-sm-2">
				Nama
			</label>
			<div class="col-sm-4">
				<input name="nama_pemohon" id="namaPemberiKuasa"
				type="text" class="form-control" disabled
				value="<?php echo $nama_pemohon;?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_pemohon" class="control-label col-sm-2">
				Nomor
			</label>
			<div class="col-sm-4">
				<input name="nomor_pemohon" id="namaPemberiKuasa"
				type="text" class="form-control" disabled
				value="<?php echo $nomor_pemohon;?>"/>
			</div>
		</div>
	</fieldset>

	<div class="form-group">
		<label for="nama" class="control-label col-sm-2">Nama</label>
		<div class="col-sm-4">
			<input name="nama" class="form-control" type="text"
			value="<?php echo $nama; ?>" />
		</div>
	</div>

	<div class="form-group">
		<label for="alamat" class="control-label col-sm-2">Alamat</label>
		<div class="col-sm-4">
			<input name="alamat" type="text" class="form-control"
			value="<?php echo $alamat; ?>"/>
		</div>
	</div>

	<div class="form-group">
		<label for="no_hp" class="control-label col-sm-2">No Hp</label>
		<div class="col-sm-2">
			<input name="no_hp" type="text" class="form-control"
			value="<?php echo $no_hp; ?>"/>
		</div>
	</div>


	<button type="submit" class="btn btn-primary col-sm-2"/>
	<span class="glyphicon glyphicon-floppy-save"></span>
</button>
</form>
</div>
<script src="<?php echo site_url('assets/js/nomorpemberikuasahandler.js'); ?>">
</script>
