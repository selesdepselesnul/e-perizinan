<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">
			
		</div>	
	</div>
	<?php 
	echo validation_errors();
	echo form_open('monitoring/'.$monitoring->nomor, ["class" => "form-horizontal"]);
	?>	
	<div class="form-group">
		<label for="nama_pemohon"  class="control-label col-sm-2">
			Nama Pemohon
		</label>
		<div class="col-sm-4">
			<input name="nama_pemohon" class="form-control" type="text" 
			value="<?php echo $nama_pemohon; ?>" disabled/>
		</div>
	</div>

	<fieldset class="scheduler-border">
		<legend class="scheduler-border">Izin Prinsip</legend>
		<div class="form-group">
			<label for="tgl_izin_prinsip" class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-4">
				<input name="tgl_izin_prinsip" type="text" class="form-control" 
				value="<?php echo $tgl_izin_prinsip; ?>" disabled/>
			</div>
		</div>	

		<div class="form-group" id="aktePerusahaan">
			<label for="nomor_izin_prinsip" class="control-label col-sm-2">
				Nomor 
			</label>
			<div class="col-sm-4">
				<input name="nomor_izin_prinsip" type="text" class="form-control"
				value="<?php echo $nomor_izin_prinsip; ?>" disabled/>
			</div>
		</div>
	</fieldset>

	<fieldset class="scheduler-border">

		<legend class="scheduler-border">IPPT</legend>
		<div class="form-group">
			<label for="nomor_ippt" class="control-label col-sm-2">
				Nomor 
			</label>
			<div class="col-sm-4">
				<input name="nomor_ippt" type="text" class="form-control" 
				value="<?php echo $monitoring->nomorIppt; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="tgl_ippt" class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-4">
				<input name="tgl_ippt" type="text" class="form-control date-picker-id" 
				value="<?php echo $monitoring->tglIppt; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="dalam_proses_ippt" class="control-label col-sm-2">
				Dalam Proses
			</label>
			<div class="col-sm-4">
				<input name="dalam_proses_ippt" type="text" class="form-control" 
				value="<?php echo $monitoring->dalamProsesIppt; ?>"/>
			</div>
		</div>
	</fieldset>

	<fieldset class="scheduler-border">
		<legend class="scheduler-border">Dokumen Lingkungan</legend>
		<div class="form-group">
			<label for="nomor_dokumen_lingkungan" class="control-label col-sm-2">
				Nomor
			</label>
			<div class="col-sm-4">
				<input name="nomor_dokumen_lingkungan" type="text" class="form-control"
				value="<?php echo $monitoring->nomorDokumenLingkungan; ?>" />
			</div>
		</div>


		<div class="form-group">
			<label for="tgl_dokumen_lingkungan" class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-4">
				<input name="tgl_dokumen_lingkungan" type="text" class="form-control date-picker-id"
				value="<?php echo $monitoring->tglDokumenLingkungan; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="dalam_proses_dokumen_lingkungan" class="control-label col-sm-2">
				Dalam Proses
			</label>
			<div class="col-sm-4">
				<input name="dalam_proses_dokumen_lingkungan" type="text" class="form-control"
				value="<?php echo $monitoring->dalamProsesDokumenLingkungan; ?>" />
			</div>
		</div>
	</fieldset>


	<fieldset class="scheduler-border">
		<legend class="scheduler-border">AMDAL Lalu Lintas</legend>
		<div class="form-group">
			<label for="nomor_amdal_lalin" class="control-label col-sm-2">
				Nomor
			</label>
			<div class="col-sm-4">
				<input name="nomor_amdal_lalin" type="text" class="form-control"
				value="<?php echo $monitoring->nomorAmdalLalin; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="tgl_amdal_lalin" class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-4">
				<input name="tgl_amdal_lalin" type="text" class="form-control date-picker-id"
				value="<?php echo $monitoring->tglAmdalLalin; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="dalam_proses_amdal_lalin" class="control-label col-sm-2">
				Dalam Proses
			</label>
			<div class="col-sm-4">
				<input name="dalam_proses_amdal_lalin" type="text" class="form-control"
				value="<?php echo $monitoring->dalamProsesAmdalLalin; ?>" />
			</div>
		</div>
	</fieldset>


	<fieldset class="scheduler-border">
		<legend class="scheduler-border">KKOP</legend>
		<div class="form-group">
			<label for="nomor_kkop" class="control-label col-sm-2">
				Nomor
			</label>
			<div class="col-sm-4">
				<input name="nomor_kkop" type="text" class="form-control"
				value="<?php echo $monitoring->nomorKkop; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="tgl_kkop" class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-4">
				<input name="tgl_kkop" type="text" class="form-control date-picker-id"
				value="<?php echo $monitoring->tglKkop; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="dalam_proses_kkop" class="control-label col-sm-2">
				Dalam Proses
			</label>
			<div class="col-sm-4">
				<input name="dalam_proses_kkop" type="text" class="form-control"
				value="<?php echo $monitoring->dalamProsesKkop; ?>" />
			</div>
		</div>
	</fieldset>


	<fieldset class="scheduler-border">
	<legend class="scheduler-border">IMB</legend>
		<div class="form-group">
			<label for="nomor_imb" class="control-label col-sm-2">
				Nomor
			</label>
			<div class="col-sm-4">
				<input name="nomor_imb" type="text" class="form-control"
				value="<?php echo $monitoring->nomorImb; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="tgl_imb" class="control-label col-sm-2">
				Tanggal
			</label>
			<div class="col-sm-4">
				<input name="tgl_imb" type="text" class="form-control date-picker-id"
				value="<?php echo $monitoring->tglImb; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="dalam_proses_imb" class="control-label col-sm-2">
				Dalam Proses
			</label>
			<div class="col-sm-4">
				<input name="dalam_proses_imb" type="text" class="form-control"
				value="<?php echo $monitoring->dalamProsesImb; ?>" />
			</div>
		</div>
	</fieldset>

</div>

<button type="submit" class="btn btn-primary col-sm-2"/>
<span class="glyphicon glyphicon-floppy-save"></span>
</button>	
</form>