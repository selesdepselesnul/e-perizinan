<table id="monitoringTable" class="display" cellspacing="0" width="100%">
	<thead>
		<tr id="headerMonitoringTable">
			<th>Nomor Monitoring</th>
			<th>Nama Pemohon</th>
			<th>Tanggal Izin Prinsip</th>
			<th>Nomor Izin Prinsip</th>
			<th>Nomor<br/>(IPPT)</th>
			<th>Tanggal<br/>(IPPT)</th>
			<th>Dalam Proses<br/>(IPPT)</th>
			<th>Nomor<br/>(Dokumen Lingkungan)</th>
			<th>Tanggal<br/>(Dokumen Lingkungan)</th>
			<th>Dalam Proses<br/>(Dokumen Lingkungan)</th>
			<th>Nomor<br/>(AMDAL Lalin)</th>
			<th>Tanggal<br/>(AMDAL Lalin)</th>
			<th>Dalam Proses<br/>(AMDAL Lalin)</th>
			<th>Nomor<br/>(KKOP)</th>
			<th>Tanggal<br/>(KKOP)</th>
			<th>Dalam Proses<br/>(KKOP)</th>
			<th>Nomor<br/>(IMB)</th>
			<th>Tanggal<br/>(IMB)</th>
			<th>Dalam Proses<br/>(IMB)</th>
		</tr>
	</thead>
</table>
<script src="<?php echo site_url('assets/js/monitoringcontroller.js') ?>"></script>