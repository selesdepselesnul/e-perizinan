<div class="form-inline">
	<div class="form-group">
		<label for="year_picker">Pilih tahun : </label>
		<select id="yearPicker" name="year_picker" class="form-control"></select>
		<button class="form-control" type="button" id="reportPerYearButton" title="Rekap Per-Bulan">
			<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
		</button>
	</div>
</div>
<br/>
<table id="rekapDataPemohonTable" class="display">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Pemohon</th>
			<th>Alamat Pemohon</th>
			<th>Tanggal Masuk Permohonan - Nota Dinas</th>
			<th>Jenis / Rencana Kegiatan / Pembangunan Yang Diajukan</th>
			<th>Alamat Lokasi Lahan</th>
			<th>Luas Lahan</th>
			<th>Luas Bangunan</th>
			<th>Pembahasan Rapat BPKRD</th>
			<th>Nomor Izin Prinsip</th>
		</tr>
	</thead>
</table>

<div id="rekapDataPemohonPerYearDialog" hidden>
	<div class="container">
		<div class="row">
			<div class="form-inline">
				<div class="form-group">
					<label for="month_picker">Pilih bulan : </label>
					<select id="monthPicker" name="month_picker" class="form-control">
						<?php
						array_walk(range(1, 12), function ($item, $key) {
							if($item < 10)
								$month = '0'.$item;
							else
								$month = $item;

							echo '<option value="'.$month.'">'.$month.'</option>';
						});
						?>
					</select>
				</div>
			</div>
			<br/>
			<table id="rekapDataPemohonPerYearTable" class="display">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama Pemohon</th>
						<th>Alamat Pemohon</th>
						<th>Tgl. Masuk Permohonan</th>
						<th>Jenis Kegiatan</th>
						<th>Alamat / Lokasi Lahan</th>
						<th>Luas Lahan</th>
						<th>Luas Bangunan</th>
						<th>Pembahasan Rapat BKPRD</th>
						<th>Tgl. Tinjauan Lapangan</th>
						<th>Disetujui / Tidak</th>
						<th>Tgl. Izin Prinsip</th>
						<th>No. Izin Prinsip</th>
						<th>Tgl. Izin Lokasi</th>
						<th>No. Izin Lokasi</th>
						<th>Tgl. Terima Perizinan</th>
						<th>Nama Penerima</th>
					</tr>
				</thead>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<script>

	$(document).ready(() => {

		const initReportPerYear = (month, year) => {
			if(!year) year = new Date().getFullYear()

			$('#rekapDataPemohonPerYearTable').DataTable().destroy()
			$('#rekapDataPemohonPerYearTable').DataTable({
				ajax: BASE_URL + 'json/rekapdatapemohon/month/' + month
				+'/year/'+year,
				processing: true,
				language: default_lang('rekap data pemohon'),
				order: [[ 1, 'asc' ]],
				columns: [
				{ data : "nomor" },
				{ data : "namaPemohon" },
				{ data : "alamatPemohon" },
				{ data : "tglPermohonan" },
				{ data : "jenisKegiatan" },
				{ data : "alamatLokasi" },
				{ data : "luasLahan" },
				{ data : "luasBangunan"},
				{ data : "pembahasanRapatBKPRD"},
				{ data : "tglTinjauanLapangan"},
				{ data : "validasi"},
				{ data : "tglIzinPrinsip"},
				{ data : "nomorIzinPrinsip"},
				{ data : "tglIzinLokasi"},
				{ data : "nomorIzinLokasi"},
				{ data : "tglPenerimaPerizinan"},
				{ data : "penerimaPerizinan"}],
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
				dom: 'Bfrtip',
				buttons: [
				'pageLength',
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]

			})
}

$('#rekapDataPemohonPerYearDialog').dialog({
	autoOpen: false,
	title : 'Rekap Perbulan',
	modal: true,
	dragable: true,
	height: 280,
	width: 1200,
	show: {
		effect: 'blind',
		duration: 1000
	},
	hide: {
		effect: 'explode',
		duration: 1000
	}
})

const toStringMonth = (month) => {
	switch(month) {
		case "01":
		return "Januari";
		case "02":
		return "Febuari";
		case "03":
		return "Maret";
		case "04":
		return "April";
		case "05":
		return "Mei";
		case "06":
		return "Juni";
		case "07":
		return "Juli";
		case "08":
		return "Agustus";
		case "09":
		return "September";
		case "10":
		return "Oktober";
		case "11":
		return "November";
		default:
		return "Desember";
	}
}

$('#reportPerYearButton').click(() => {

	const year = $('#yearPicker').val()
	const month = $('#monthPicker').val();

	initReportPerYear(month, year)

	const rekapDataPemohonDialog = $('#rekapDataPemohonPerYearDialog')
	$("title").text('Rekap bulan ' + toStringMonth(month) + ' tahun ' + year)
	rekapDataPemohonDialog.dialog('open')
	$('#monthPicker').change(() => {
		$("title").text('Rekap bulan ' + toStringMonth($('#monthPicker').val()) + ' tahun ' + year)
		initReportPerYear($('#monthPicker').val(), year)
	})

})

var initialYear
$.getJSON(BASE_URL + '/json/rekapdatapemohon/getfirstyear')
.done(function(firstYear) {
	$.getJSON( BASE_URL + '/json/rekapdatapemohon/getlastyear')
	.done(function(lastYear) {
		_.each(_.range(parseInt(firstYear), parseInt(lastYear)+1), function(year) {
			$('#yearPicker').append('<option value="'+ year +'">'+year+'</option>')
		})


		function initRekapDataPemohonTable(year) {
			if(!year) year = new Date().getFullYear()

			$("title").text('Data Permohonan Perizinan Pada Bidang Fisik BAPPEDA Kota Cimahi Tahun '
				+ $('#yearPicker').val())
			$('#rekapDataPemohonTable').DataTable().destroy()
			$('#rekapDataPemohonTable').DataTable({
				ajax: BASE_URL + 'json/rekapdatapemohon/year/' + year,
				processing: true,
				language: default_lang('rekap data pemohon'),
				order: [[ 1, 'asc' ]],
				columns: [
				{ data : "nomor" },
				{ data : "namaPemohon" },
				{ data : "alamatPemohon" },
				{ data : "tglPermohonan" },
				{ data : "jenisKegiatan" },
				{ data : "alamatLokasi" },
				{ data : "luasLahan" },
				{ data : "luasBangunan"},
				{ data : "pembahasanRapatBKPRD"},
				{ data : "nomorIzinPrinsip"}],
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
				dom: 'Bfrtip',
				buttons: [
				'pageLength',
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]

			})

		}

		$( '#yearPicker' ).change(function() {
			console.log($('#yearPicker').val())
			initRekapDataPemohonTable($('#yearPicker').val())
		})

		initRekapDataPemohonTable($('#yearPicker').val())


		$(document).tooltip()

	})



})
})


</script>
