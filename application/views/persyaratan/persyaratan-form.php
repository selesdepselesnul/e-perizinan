<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<?php if(isset($header))
		echo "<h2>$header</h2>"; ?>
	</div>
	<div class="panel-body">
		<div class="row">

		</div>
	</div>
	<?php


	echo form_open(site_url('persyaratan/'.$persyaratan->nomorPemohon),
		["class" => "form-horizontal"]);
		?>
		<fieldset class="scheduler-border">
		<legend class="scheduler-border">Dari Pemohon</legend>
			<div class="form-group">
				<label for="nama_pemohon" class="control-label col-sm-2">
					Nama
				</label>
				<div class="col-sm-4">
					<input name="nama_pemohon" id="namaPemohon"
					type="text" class="form-control"
					value="<?php echo $persyaratan->namaPemohon;?>" disabled/>
				</div>
			</div>

			<div class="form-group">
				<label for="nomor_pemohon" class="control-label col-sm-2">
					Nomor
				</label>
				<div class="col-sm-4">
					<input name="nomor_pemohon" id="nomorPemohon"
					type="text" class="form-control"
					value="<?php echo $persyaratan->nomorPemohon;?>" disabled/>
				</div>
			</div>
		</fieldset>

		<fieldset class="scheduler-border">
			<legend class="scheduler-border">Sertifikat Tanah</legend>
			<div class="form-group">
				<label for="nomor_sertifikat_tanah" class="control-label col-sm-2">
					Nomor
				</label>
				<div class="col-sm-4">
					<input name="nomor_sertifikat_tanah" type="text" class="form-control"
					value="<?php echo $persyaratan->nomorSertifikatTanah; ?>"/>
				</div>
			</div>

			<div class="form-group">
				<label for="luas_tanah_sertifikat_tanah" class="control-label col-sm-2">
					Luas Tanah
				</label>
				<div class="col-sm-4">
					<div class="input-group">
						<input name="luas_tanah_sertifikat_tanah" type="number" step="any"
						class="form-control"
						value="<?php echo $persyaratan->luasTanahSertifikatTanah; ?>"/>
						<div class="input-group-addon">m<sup>2</sup></div>
					</div>
				</div>
			</div>


			<div class="form-group">
				<label for="tahun_sertifikat_tanah" class="control-label col-sm-2">
					Tahun
				</label>
				<div class="col-sm-4">
					<input name="tahun_sertifikat_tanah" type="text" class="date-picker-id form-control"
					value="<?php echo $persyaratan->tahunSertifikatTanah; ?>"/>
				</div>
			</div>


			<div class="form-group">
				<label for="jenis_kepemilikan_sertifikat_tanah" class="control-label col-sm-2">
					Jenis Kepemilikan
				</label>
				<div class="col-sm-4">
					<input name="jenis_kepemilikan_tanah_sertifikat_tanah" type="text" class="form-control"
					value="<?php echo $persyaratan->jenisKepemilikanTanahSertifikatTanah; ?>"/>
				</div>
			</div>

			<div class="form-group">
				<label for="atas_nama_sertifikat_tanah" class="control-label col-sm-2">
					Atas Nama
				</label>
				<div class="col-sm-4">
					<input name="atas_nama_sertifikat_tanah" type="text" class="form-control"
					value="<?php echo $persyaratan->atasNamaSertifikatTanah; ?>"/>
				</div>
			</div>
		</fieldset>

		<div class="form-group">
			<label for="nomor_surat_permohonan" class="control-label col-sm-2">Nomor Surat Permohonan</label>
			<div class="col-sm-4">
				<input name="nomor_surat_permohonan" class="form-control" type="text"
				value="<?php echo $persyaratan->nomorSuratPermohonan; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_ktp" class="control-label col-sm-2">Nomor Ktp</label>
			<div class="col-sm-4">
				<input name="nomor_ktp" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorKtp; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_surat_kuasa" class="control-label col-sm-2">Nomor Surat Kuasa</label>
			<div class="col-sm-4">
				<input name="nomor_surat_kuasa" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorSuratKuasa; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_proposal_proyek" class="control-label col-sm-2">Nomor Proposal Proyek</label>
			<div class="col-sm-4">
				<input name="nomor_proposal_proyek" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorProposalProyek; ?>"/>
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_akte_pendirian" class="control-label col-sm-2">Nomor Akte Pendirian</label>
			<div class="col-sm-4">
				<input name="nomor_akte_pendirian" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorAktePendirian; ?>"/>
			</div>
		</div>


		<div class="form-group">
			<label for="nomor_pbb" class="control-label col-sm-2">Nomor Pbb</label>
			<div class="col-sm-4">
				<input name="nomor_pbb" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorPbb; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_rincian_lahan" class="control-label col-sm-2">Nomor Rincian Lahan</label>
			<div class="col-sm-4">
				<input name="nomor_rincian_lahan" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorRincianLahan; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_surat_pernyataan" class="control-label col-sm-2">
				Nomor Surat Pernyataan
			</label>
			<div class="col-sm-4">
				<input name="nomor_surat_pernyataan" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorSuratPernyataan; ?>" />
			</div>
		</div>


		<div class="form-group">
			<label for="nomor_rencana_pembangunan" class="control-label col-sm-2">
				Nomor Rencana Pembangunan
			</label>
			<div class="col-sm-4">
				<input name="nomor_rencana_pembangunan" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorSuratRencanaPembangunan; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label for="nomor_domisili_perusahaan" class="control-label col-sm-2">
				Nomor Domisili Perusahaan
			</label>
			<div class="col-sm-4">
				<input name="nomor_domisili_perusahaan" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorSuratDomisiliPerusahaan; ?>" />
			</div>
		</div>


		<div class="form-group">
			<label for="nomor_pemberitahuan_tetangga" class="control-label col-sm-2">
				Nomor Pemberitahuan Tetangga
			</label>
			<div class="col-sm-4">
				<input name="nomor_pemberitahuan_tetangga" type="text" class="form-control"
				value="<?php echo $persyaratan->nomorSuratPemberitahuanTetangga; ?>" />
			</div>
		</div>

		<button type="submit" class="btn btn-primary col-sm-2"/>
		<span class="glyphicon glyphicon-floppy-save"></span>
	</button>
</form>
