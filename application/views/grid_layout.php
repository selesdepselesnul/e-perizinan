<?php
$this->eupbElement->generatePagination(
	'penerimakuasa', $total_rows);	
?>
	<div class="table-responsive">
		<table name="pemohon_table" class="table table-bordered">
			<tr>
				<th>Id</th>
				<?php
				array_walk(
					$all_headers,
					function ($x) {
						echo '<th>'.$x.'<th>'; 	
					}); 
				?>
			</tr>
			<?php
			array_walk($some_penerima_kuasa, function($x) {
				echo 
				'<tr id="pemohon_'.$x->id.'">'.
				'<td>'.$x->id.'</td>'.
				$show_properties($x).
				'<td><button id="editingButton_'.$x->id.'"class="editing-button form-control">
				<span class="glyphicon glyphicon-pencil"></span></button></td>'.
				'<td><button id="removingButton_'.$x->id.'" class="removing-button form-control">
				<span class="glyphicon glyphicon-trash"></span></button></td>'.
				'</tr>';
			});
			?>
		</table>
	</div>
</div>
<div class="container">
	<button id="addingButton" class="form-control">
		<span class="glyphicon glyphicon-plus">
		</span>
	</button>
</div>
<script>
	$(document).ready(function() {
		runController('pemberikuasa');
	});
</script>
