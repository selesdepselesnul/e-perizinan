<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class BerkasController extends CI_Controller {
	const BERKAS_PATH = './berkas-file/';

	public function __construct() {

		parent::__construct();

		$this->eupbAuth->checkIfLogin();


		$this->load->model(
			'BerkasModel',
			'berkasModel',
			TRUE);

		$this->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);

		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);

		if(!file_exists('./berkas-file/'))
			mkdir('./berkas-file/', 0777);


	}

	public function create($nomorPemohon) {

		$this->eupbAuth->checkIfEnable('dapatTambahBerkas');

		$this->load->view('master-layout', [
			'title' => 'Berkas',
			'page' => 'berkas/berkas-form',
			'header' => 'Penambahan Data Berkas Pemohon',
			'activating' => 'pemohon',
			'nomor_pemohon' => $nomorPemohon,
			'nama_pemohon' => $this->pemohonModel->findById($nomorPemohon)->nama,
			'berkas' => NULL,
			'error' => NULL
			]);
	}

	public function edit($nomor) {

		$this->eupbAuth->checkIfEnable('dapatEditBerkas');

		$berkas = $this->berkasModel->findById($nomor);
		$this->load->view('master-layout', [
			'title' => 'Berkas',
			'page' => 'berkas/berkas-form',
			'header' => 'Pembaharuan Data Berkas Pemohon',
			'activating' => 'pemohon',
			'nomor_pemohon' => $berkas->nomorPemohon,
			'nama_pemohon' => $this->pemohonModel->findById($berkas->nomorPemohon)->nama,
			'berkas' => $berkas,
			'error' => NULL
		]);

	}

	public function update($nomorPemohon, $nomorBerkas) {

		$this->eupbAuth->checkIfEnable('dapatEditBerkas');

		$selectedBerkas = $this->berkasModel->findById($nomorBerkas);

		$this->form_validation->set_rules(
			'nama', 'nama', 'required');

		if($this->form_validation->run()) {
			$new_name = $this->input->post('nama');
			rename(self::BERKAS_PATH.$nomorPemohon.'/'.$selectedBerkas->nama.$selectedBerkas->ext,
				self::BERKAS_PATH.$nomorPemohon.'/'.$new_name.$this->input->post('ext'));
			$selectedBerkas->nama = $new_name;
			$selectedBerkas->ext = $this->input->post('ext');
			$selectedBerkas->update();

			$this->logModel->kegiatan = 'memperbaharui';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->data = 'berkas dengan nomor : '
			.$nomorBerkas.'; dari pemohon dengan nomor : '.$nomorPemohon;
			$this->logModel->save();

			redirect(site_url('pemohon'));
		} else {
			redirect(site_url('berkas/'.$nomorBerkas.'/edit'));
		}
	}


	public function store($nomorPemohon) {

		$this->eupbAuth->checkIfEnable('dapatTambahBerkas');

		if(!file_exists('./berkas-file/'.$nomorPemohon))
			mkdir('./berkas-file/'.$nomorPemohon, 0777);


		$this->berkasModel->nomorPemohon = $nomorPemohon;
		$this->berkasModel->save();


		$config['upload_path']          = './berkas-file/'.$nomorPemohon;
		$config['allowed_types']        = 'jpg|png|pdf|doc|xlsx|docx';
		$config['remove_spaces']		= TRUE;

		if($this->input->post('nama') !== '')
			$config['file_name'] = $this->input->post('nama') . $this->input->post('ext');

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('berkas_file')) {
			$error = ['error' => $this->upload->display_errors()];

			$this->berkasModel->delete($this->berkasModel->getLastNomor());
			$this->load->view('master-layout', [
				'title' => 'Berkas',
				'page' => 'berkas/berkas-form',
				'header' => 'Penambahan Data Berkas Pemohon',
				'activating' => 'pemohon',
				'nomor_pemohon' => $nomorPemohon,
				'nama_pemohon' => $this->pemohonModel->findById($nomorPemohon)->nama,
				'berkas' => NULL,
				'error' => $error
				]);
		} else {
			$data = ['upload_data' => $this->upload->data()];

			$this->berkasModel = $this->berkasModel->findById($this->berkasModel->getLastNomor());

			if($this->input->post('nama') === '') {
				$this->berkasModel->nama = strstr($data['upload_data']['file_name'], '.', true);
				$this->berkasModel->ext = strstr($data['upload_data']['file_name'], '.');
			} else {
				$this->berkasModel->nama = $this->input->post('nama');
				$this->berkasModel->ext = $this->input->post('ext');
			}

			$this->berkasModel->update();

			$this->logModel->kegiatan = 'menambahkan';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->data = 'berkas dengan nomor : '
			.$this->berkasModel->nomor.'; dari pemohon dengan nomor : '.$nomorPemohon;
			$this->logModel->save();

			redirect(site_url('pemohon'));
		}
	}

	public function destroy($nomor) {

		$this->eupbAuth->checkIfEnable('dapatHapusBerkas');

		$to_be_deleted_berkas = $this->berkasModel->findById($nomor);

		$this->logModel->kegiatan = 'menghapus';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'berkas dengan nomor : '
		.$nomor.'; dari pemohon dengan nomor : '.$to_be_deleted_berkas->nomorPemohon;
		$this->logModel->save();

		unlink('./berkas-file/'.$to_be_deleted_berkas->nomorPemohon.'/'.$to_be_deleted_berkas->nama.$to_be_deleted_berkas->ext);
		$this->berkasModel->delete($nomor);

		echo "success";
	}

}
