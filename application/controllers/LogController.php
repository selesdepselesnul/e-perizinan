<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class LogController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
	}

	public function destroy($nomor) {
		$this->logModel->delete($nomor);
		echo "success";
	}
}
