<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PersyaratanJsonController extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PersyaratanModel', 
			'persyaratanModel',
			TRUE);
	}

	public function getAll() {
		echo json_encode(['data' => $this->persyaratanModel->getAll()]);
	}

	public function wherePemohonID($id) {
		echo json_encode(['data' => $this->persyaratanModel->wherePemohonID($id)]);
	}
}