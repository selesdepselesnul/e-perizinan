<?php
/**
 *@author : Moch Deden (https://github.com/selesdepselesnul)
 */
class MonitoringJsonController extends CI_Controller {


	public function __construct() {
		parent::__construct();

		$this->load->model(
			'PermohonanModel',
			'permohonanModel',
			TRUE);
		$this->load->model(
			'MonitoringModel',
			'monitoringModel',
			TRUE);
		$this->load->helper('date');
	}

	public function create() {
		$this->load->view('master-layout', [
			'title' => 'Proses',
			'page' => 'permohonan/permohonan-form',
			'header' => 'Penambahan Data Proses',
			'activating' => 'permohonan',
			'all_id_pemohon' => $this->pemohonModel->getAllId(),
			'permohonan' => NULL,
			'nama_pemohon' => ''
			]);
	}

	public function edit($nomor) {
		$permohonan = $this->permohonanModel->findById($nomor);
		$pemohon = $this->pemohonModel->findById(
			$permohonan->nomorPemohon);
		$this->load->view('master-layout', [
			'title' => 'Proses',
			'header' => 'Pembaharuan Data Proses',
			'page' => 'permohonan/permohonan-form',
			'activating' => 'permohonan',
			'all_id_pemohon' => $this->pemohonModel->getAllId(),
			'permohonan' => $permohonan,
			'nama_pemohon' => $pemohon->nama
			]);
	}

	public function checkValidation($id) {
		$this->load->view('master-layout', [
			'title' => 'Proses',
			'header' => 'Proses',
			'page' => 'permohonan/validation',
			'activating' => 'permohonan',
			'all_id_pemohon' => $this->pemohonModel->getAllId(),
			'permohonan' => $this->permohonanModel->findById($id)
			]);
	}

	public function saveWithMethod($method) {
		$this->form_validation->set_rules(
			'nomor_pemohon', 'Nomor Pemohon', 'required');
		$this->form_validation->set_rules(
			'nama_pemohon', 'Nama Pemohon', 'required');

		if($this->form_validation->run()) {
			$this->permohonanModel->nomorPemohon =
			$this->input->post('nomor_pemohon');
			$this->permohonanModel->namaPemohon =
			$this->input->post('nama_pemohon');
			$this->permohonanModel->latitude =
			$this->input->post('latitude');
			$this->permohonanModel->longitude =
			$this->input->post('longitude');
			$this->permohonanModel->beritaAcaraLapangan =
			$this->input->post('berita_acara_lapangan');
			$this->permohonanModel->jenisKegiatan = html_escape(
				$this->input->post('jenis_kegiatan'));
			$this->permohonanModel->nomorIzinPrinsip =
			$this->input->post('nomor_izin_prinsip');
			$this->permohonanModel->izinPrinsip =
			$this->input->post('izin_prinsip');
			$this->permohonanModel->tglBerkasMasuk =
			$this->input->post('tgl_berkas_masuk');
			$this->permohonanModel->tglPembahasan =
			$this->input->post('tgl_pembahasan');
			$this->permohonanModel->pembahasanRapatBKPRD =
			$this->input->post('pembahasan_rapat_bkprd');
			$this->permohonanModel->tglTinjauanLapangan =
			$this->input->post('tgl_tinjauan_lapangan');
			$this->permohonanModel->penerimaPerizinan =
			$this->input->post('penerima_perizinan');
			$this->permohonanModel->tglPenerimaPerizinan =
			$this->input->post('tgl_penerima_perizinan');
			$method();
			redirect(site_url('permohonan/'));
		} else {
			redirect(site_url('permohonan/create'));
		}
	}

	public function store() {
		$this->saveWithMethod(function () {
			$this->permohonanModel->tglPermohonan = date("d/m/Y");
			$this->permohonanModel->save();
		});
	}

	public function show() {
		$this->load->view('master-layout', [
			'title' => 'Monitoring',
			'header' => 'Data Monitoring',
			'page' => 'monitoring/show',
			'activating' => 'monitoring'
			]);
	}


	public function update($nomor) {
		$this->saveWithMethod(function() use($nomor) {
			$this->permohonanModel->tglPermohonan =
			$this->permohonanModel->findById($nomor)->tglPermohonan;
			$this->permohonanModel->nomor = $nomor;
			$this->permohonanModel->update();
		});
	}

	public function destroy($id) {
		$this->permohonanModel->delete($id);
		echo "success";
	}

	private function radioToBool($x) {
		if ($x == 'accept')
			return true;
		else if($x == 'deny')
			return false;
		else
			return NULL;
	}

	public function validate($nomor) {
		$old_permohonan = $this->permohonanModel->findById($nomor);

		$old_permohonan->nomor = $old_permohonan->nomor;
		$old_permohonan->nomorPemohon =  $old_permohonan->nomorPemohon;
		$old_permohonan->latitude = $old_permohonan->latitude;
		$old_permohonanModel->longitude = $old_permohonan->longitude;
		$old_permohonan->validasi1 =
		$this->radioToBool($this->input->post('validasi1'));
		$old_permohonan->validasi2 =
		$this->radioToBool($this->input->post('validasi2'));
		$old_permohonan->validasi3 =
		$this->radioToBool($this->input->post('validasi3'));
		$old_permohonan->nomorNotaDinasPenolakan =
		$this->input->post('nomor_nota_dinas_penolakan');

		$old_permohonan->update();
		redirect(site_url('permohonan/'));
	}

	public function getAll() {
		$all_monitoring = array_map(function ($m) {
			$monitoring = (array)$m;
			$permohonan = $this->permohonanModel->findById(
				$monitoring['nomorPermohonan']);
			$monitoring['namaPemohon'] = $permohonan->namaPemohon;
			$monitoring['tglIzinPrinsip'] = $permohonan->tglIzinPrinsip;
			$monitoring['nomorIzinPrinsip'] =
			$permohonan->nomorIzinPrinsip;
			return (object)$monitoring;
		}, $this->monitoringModel->getAll());

		echo json_encode(['data' => $all_monitoring]);
	}
}
