<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class KecamatanJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'KecamatanModel',
			'kecamatanModel',
			TRUE);
	}

	public function getAll() {
		echo json_encode(['data' => $this->kecamatanModel->getAll()]);
	}

	public function findById($nomor) {
		echo json_encode(
			$this->kecamatanModel->findById($nomor));
	}

	public function store() {
		$this->kecamatanModel->nama = $this->input->post('nama');
		if($this->kecamatanModel->isExists('nama', $this->kecamatanModel->nama)) {
			echo json_encode(-1);
		} else {
			$this->kecamatanModel->save();
			echo json_encode($this->kecamatanModel->getLastNomor());
		}
	}

	public function update($nomor) {
		$this->kecamatanModel->nomor = $nomor;
		$this->kecamatanModel->nama = $this->input->post('nama');
		$this->kecamatanModel->update();
		echo json_encode($nomor);
	}
}
