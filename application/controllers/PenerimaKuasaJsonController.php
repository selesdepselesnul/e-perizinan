<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PenerimaKuasaJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PenerimaKuasaModel', 
			'penerimaKuasaModel',
			TRUE);
	}

	public function wherePemohonID($id) {
		echo json_encode(['data' => $this->penerimaKuasaModel->wherePemohonID($id)]);
	}
}