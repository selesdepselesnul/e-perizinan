<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class MainController extends CI_Controller {

	private $CI;

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('EupbAuth', NULL, 'eupbAuth');
		$this->load->helper('url');
		$this->load->model(
			'PermohonanModel',
			'permohonanModel',
			TRUE);
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
		$this->CI =& get_instance();
	}

	public function latlong($latitude=0, $longitude=0) {
		$this->eupbAuth->checkIfLogin();

		$lat = $latitude;
		$long = $longitude;

		$latitudeArr = explode('_', $latitude);
		$longitudeArr = explode('_', $longitude);

		if(count($latitudeArr) > 1)
			$lat = $latitudeArr[0].'.'.$latitudeArr[1];
		if(count($longitudeArr) > 1)
			$long = $longitudeArr[0].'.'.$longitudeArr[1];

		$this->load->view('master-layout', [
			'title' => 'Map',
			'page' => 'permohonan/latlong',
			'header' => 'Map',
			'activating' => 'latlong',
			'latitude' => $lat,
			'longitude' => $long
			]);
	}

	public function index() {

		$this->eupbAuth->checkIfLogin();

		$this->load->view('master-layout',[
			'title' => 'Statistik Proses',
			'page' => 'index',
			'header' => 'Statistik Proses',
			'activating' => '']);
	}


	public function login() {
		$this->eupbAuth->ifNotLogin(function() {
			$this->load->view('login', [
				'title' => 'Login',
				'is_wrong' => false
				]);
		});
	}

	public function logout() {

		$this->eupbAuth->logout();

	}

	public function postLogin() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role = $this->roleModel
		->getRole($username, $password);

		if (!is_null($role)) {
			$this->session->role = (array)$role;
			$this->session->jenisRole = (array)$this->jenisRoleModel->findById(
				$role->nomorJenisRole);

			$this->logModel->kegiatan = 'login';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->save();

			redirect(index_page());
		} else {
			$this->load->view('login', [
				'title' => 'Login',
				'is_wrong' => true
				]);
		}
	}

	public function datamaster() {

		$this->eupbAuth->checkIfLogin();
		$this->eupbAuth->CheckIfEnable('dapatKonfigurasi');
		$this->load->view('master-layout',[
			'title' => 'Data Master',
			'page' => 'data_master/show',
			'header' => 'Data Master',
			'activating' => 'datamaster',
			'all_jenis_role' => $this->jenisRoleModel->getAll()]);
	}

	public function playground() {
	}
}
