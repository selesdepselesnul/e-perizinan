<?php
use Rah\Danpu\Dump;
use Rah\Danpu\Export;
use Rah\Danpu\Import;
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class DatabaseController extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

    private function dump($dumper) {
        $database_config = $this->config->item('database');
		try {
		    $dump = new Dump;
		    $dump
		        ->file('sql/'.$this->input->post('file_name').'.sql')
		        ->dsn('mysql:dbname='.$database_config['name']
					.';host='.$database_config['hostname'])
		        ->user($database_config['username'])
		        ->pass($database_config['password']);

		    $dumper($dump);
		} catch (\Exception $e) {
		    echo 'Export failed with message: ' . $e->getMessage();
		}


		echo json_encode($this->input->post('file_name'));
    }

	public function export() {
        $this->dump(function($x) {
            return new Export($x);
        });
	}

    public function import() {
        $this->db->query('DROP DATABASE eperizinan');
        $this->db->query('CREATE DATABASE eperizinan');
        $this->dump(function($x) {
            return new Import($x);
        });
	}

}
