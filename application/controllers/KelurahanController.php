<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class KelurahanController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'KelurahanModel',
			'kelurahanModel',
			TRUE);
		$this->load->model(
			'KecamatanModel',
			'kecamatanModel',
			TRUE);

		$this->eupbAuth->checkIfEnable('dapatKonfigurasi');

	}

	public function destroy($nomor) {
		$this->kelurahanModel->delete($nomor);
		echo "success";
	}

	public function create($nomor_kecamatan) {

		$this->load->view(
			'master-layout', [
			'title' => 'Kelurahan',
			'page' => 'data_master/kelurahan-form',
			'header' => 'Penambahan Data Kelurahan di Kecamatan '
							. $this->kecamatanModel->findById($nomor_kecamatan)->nama,
			'activating' => 'datamaster',
			'nomor_kecamatan' => $nomor_kecamatan,
			'kelurahan' => NULL ]);
	}


	public function edit($nomor_kecamatan, $nomor_kelurahan) {

		$this->load->view(
			'master-layout', [
			'title' => 'Kelurahan',
			'page' => 'data_master/kelurahan-form',
			'header' => 'Pembaharuan Data Kelurahan di Kecamatan '
				.$this->kecamatanModel->findById($nomor_kecamatan)->nama,
			'activating' => 'datamaster',
			'kelurahan' => $this->kelurahanModel->findById($nomor_kelurahan) ]);

	}

	private function saveWithMethod($method, $nomor_kecamatan) {

		$this->form_validation->set_rules(
			'nama', 'nama', 'required');

		if($this->form_validation->run()) {

			$this->kelurahanModel->nama =
			$this->input->post('nama');

			$method();
			redirect(site_url('datamaster#tabs-3'));
		} else {
			redirect(site_url('kelurahan/'.$nomor_kecamatan.'/create'));
		}
	}


	public function store($nomor_kecamatan) {
		$this->saveWithMethod(function() use($nomor_kecamatan){
			$this->kelurahanModel->nomorKecamatan = $nomor_kecamatan;
			$this->kelurahanModel->save();
		}, $nomor_kecamatan);
	}

	public function update($nomor_kecamatan, $nomor_kelurahan) {

		$this->saveWithMethod(function() use($nomor_kecamatan, $nomor_kelurahan){
			$this->kelurahanModel->nomor = $nomor_kelurahan;
			$this->kelurahanModel->nomorKecamatan = $nomor_kecamatan;
			$this->kelurahanModel->update();
		}, $nomor_kecamatan);

	}

}
