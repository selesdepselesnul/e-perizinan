<?php 
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class BerkasJsonController extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(
			'BerkasModel',
			'berkasModel',
			TRUE);
	}

	public function wherePemohonID($nomor) {
		echo json_encode([ 'data' => $this->berkasModel->wherePemohonID($nomor)]);
	}
}