<?php
/**
 *@author : Moch Deden (https://github.com/selesdepselesnul)
 */
class JenisRoleJsonController extends CI_Controller {


	public function __construct() {
		parent::__construct();
	}

	public function getAll() {
		echo json_encode(['data' => $this->jenisRoleModel->getAll()]);
	}

	public function findById($nomor) {
		echo json_encode($this->jenisRoleModel->findById($nomor));
	}

	public function store() {
		$this->jenisRoleModel->nama = $this->input->post('nama');
		if($this->jenisRoleModel->isExists('nama', $this->jenisRoleModel->nama)) {
			echo json_encode(-1);
		} else {
			$this->jenisRoleModel->save();
			echo json_encode($this->jenisRoleModel->getLastNomor());
		}
	}

}
