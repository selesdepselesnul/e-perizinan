<?php 
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class KelurahanJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'KelurahanModel',
			'kelurahanModel',
			TRUE);
	}

	public function showAll() {
		echo json_encode($this->kelurahanModel->getAll());
	}

	public function findById($nomor) {
		echo json_encode($this->kelurahanModel->findById($nomor));
	}

	public function whereKecamatanId($nomorKecamatan) {
		echo json_encode(['data' => $this->kelurahanModel->whereKecamatanId($nomorKecamatan)]);
	}

	public function store() {
		$this->kelurahanModel->nama = $this->input->post('nama');
		$this->kelurahanModel->nomorKecamatan = $this->input->post('nomorKecamatan');
		$this->kelurahanModel->save();
		echo json_encode($this->kelurahanModel->getLastNomor());
	}

	public function update($nomor) {
		$this->kelurahanModel = $this->kelurahanModel->findById($nomor);
		$this->kelurahanModel->nama = $this->input->post('nama');
		$this->kelurahanModel->update();
		echo json_encode($nomor);
	}

}