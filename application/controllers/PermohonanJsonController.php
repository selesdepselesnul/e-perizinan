<?php 
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PermohonanJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PermohonanModel', 
			'permohonanModel',
			TRUE);
	}

	public function getAll() {
		echo json_encode(['data' => $this->permohonanModel->getAll()]);
	}

	public function countValidation() {
		echo json_encode($this->permohonanModel->countValidation());
	}

}