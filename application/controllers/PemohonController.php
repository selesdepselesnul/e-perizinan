<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PemohonController extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();

		$this->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);
		$this->load->model(
			'JenisBangunanModel',
			'jenisBangunanModel',
			TRUE);
		$this->load->model(
			'KecamatanModel',
			'kecamatanModel',
			TRUE);
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);


	}

	public function create() {

		$this->eupbAuth->checkIfEnable('dapatTambahPemohon');

		$this->load->view('master-layout', [
			'title' => 'Pemohon',
			'page' => 'pemohon/pemohon-form',
			'header' => 'Penambahan Data Pemohon',
			'activating' => 'pemohon',
			'pemohon' => NULL,
			'nama_pemohon' => '',
			'all_jenis_bangunan' =>
			$this->jenisBangunanModel->getAll(),
			'all_kecamatan' => $this->kecamatanModel->getAll()
			]);
	}


	private function saveWithMethod($method) {

		$this->form_validation->set_rules(
			'nama', 'nama', 'required');
		$this->form_validation->set_rules(
			'nomor_kelurahan', 'nomor_kelurahan', 'required');
		$this->form_validation->set_rules(
			'jenis_bangunan', 'jenis_bangunan', 'required');

		if($this->form_validation->run()) {

			$this->pemohonModel->nama =
			$this->input->post('nama');

			$this->pemohonModel->alamat =
			$this->input->post('alamat');

			$this->pemohonModel->namaBadanUsaha =
			$this->input->post('nama_badan_usaha');


			$this->pemohonModel->jabatan =
			$this->input->post('jabatan');


			$this->pemohonModel->alamatBadanUsaha =
			$this->input->post('alamat_badan_usaha');

			$this->pemohonModel->npwp =
			$this->input->post('npwp');

			$this->pemohonModel->luasBangunan =
			toNumSafely($this->input->post('luas_bangunan'));

			$this->pemohonModel->luasTanah =
			toNumSafely($this->input->post('luas_tanah'));

			$this->pemohonModel->alamatLokasi =
			$this->input->post('alamat_lokasi');

			$this->pemohonModel->rt =
			toNumSafely($this->input->post('rt'));

			$this->pemohonModel->rw =
			toNumSafely($this->input->post('rw'));

			$this->pemohonModel->nomorJenisBangunan =
			$this->input->post('jenis_bangunan');

			$this->pemohonModel->nomorKelurahan =
			$this->input->post('nomor_kelurahan');

			$this->pemohonModel->kota = 'Cimahi';

			$this->pemohonModel->nomorDisposisiSekda =
			$this->input->post('no_disposisi_sekda');

			$this->pemohonModel->nomorDisposisiKaBappeda =
			$this->input->post('no_disposisi_kepala_bappeda');

			$this->pemohonModel->nomorDisposisiKabid =
			$this->input->post('no_disposisi_kabid');

			$this->pemohonModel->jenisBadanUsaha =
			$this->input->post('jenis_badan_usaha');

			if(strcmp($this->pemohonModel->jenisBadanUsaha, 'perseorangan') === 0)
				$this->pemohonModel->noAktePerusahaan = '';
			else
				$this->pemohonModel->noAktePerusahaan = $this->input->post('no_akte_perusahaan');

			$method();

			redirect(site_url('pemohon'));
		} else {
			redirect(site_url('pemohon/create'));
		}

	}

	public function update($nomor) {
		$this->eupbAuth->checkIfEnable('dapatEditPemohon');

		$this->saveWithMethod(function() use($nomor) {
			$this->pemohonModel->nomor = $nomor;
			$this->pemohonModel->update();
			$this->logModel->kegiatan = 'memperbaharui';
			$this->logModel->waktu = now_wib();
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->data = 'pemohon dengan nomor : ' . $nomor;
			$this->logModel->save();
		});
	}

	public function edit($nomor) {
		$this->eupbAuth->checkIfEnable('dapatEditPemohon');

		$this->load->view(
			'master-layout', [
			'title' => 'Pemohon',
			'page' => 'pemohon/pemohon-form',
			'header' => 'Pembaharuan Data Pemohon',
			'activating' => 'pemohon',
			'pemohon' => $this->pemohonModel->findById($nomor),
			'all_jenis_bangunan' =>
			$this->jenisBangunanModel->getAll(),
			'all_kecamatan' => $this->kecamatanModel->getAll()
			]);
	}

	public function store() {
		$this->eupbAuth->checkIfEnable('dapatTambahPemohon');

		$this->logModel->kegiatan = 'menambahkan';

		$this->saveWithMethod(function() {
			$this->pemohonModel->save();
			$this->logModel->waktu = now_wib();
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->data = 'pemohon dengan nomor : ' . $this->pemohonModel->getLastNomor();
			$this->logModel->save();
		});


	}

	public function show() {
		$this->load->view('master-layout', [
			'title' => 'Pemohon',
			'header' => 'Data Pemohon',
			'page' => 'pemohon/show',
			'activating' => 'pemohon',
			'total_rows' => $this->pemohonModel->count(),
			'table_header' => 'pemohon/table-headers',
			'table_data' => 'pemohon/table-datas',
			'controller' => 'pemohon'
			]);
	}


	public function destroy($nomor) {
		$this->eupbAuth->checkIfEnable('dapatHapusPemohon');

		$this->logModel->kegiatan = 'menghapus';
		$this->logModel->data = 'pemohon dengan nomor : '.$nomor;
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->save();


		$this->pemohonModel->delete($nomor);
		echo "success";
	}
}
