<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PemberiKuasaJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PemberiKuasaModel', 
			'pemberiKuasaModel',
			TRUE);
	}

	public function whereNameLike($pattern_name) {
		echo json_encode(
			$this->pemberiKuasaModel->whereNameLike(
				str_replace("%20", " ", $pattern_name))
		);
	}
}