<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class JenisBangunanController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'JenisBangunanModel',
			'jenisBangunanModel',
			TRUE);
	}

	public function edit($nomor) {
		$this->eupbAuth->checkIfEnable('dapatEditPemohon');

		$this->load->view(
			'master-layout', [
			'title' => 'Jenis Bangunan',
			'page' => 'data_master/jenisbangunan-form',
			'header' => 'Pembaharuan Data Jenis Bangunan',
			'activating' => 'datamaster',
			'jenis_bangunan' => $this->jenisBangunanModel->findById($nomor)
			]);
	}



	public function update($nomor) {
		$this->form_validation->set_rules(
			'nama', 'nama', 'required');

		if($this->form_validation->run()) {

			$this->jenisBangunanModel->nomor = $nomor;

			$this->jenisBangunanModel->nama =
			$this->input->post('nama');


			if($this->jenisBangunanModel->isExists('nama', $this->jenisBangunanModel->nama)
			  && $this->jenisBangunanModel->nama != $this->input->post('old_nama')) {
				$this->load->view(
					'master-layout', [
					'title' => 'Jenis Bangunan',
					'page' => 'data_master/jenisbangunan-form',
					'header' => 'Pembaharuan Data Jenis Bangunan',
					'activating' => 'datamaster',
					'jenis_bangunan' => $this->jenisBangunanModel->findById($nomor),
					'message' => 'jenis bangunan sudah ada!'
					]);
				return;
			  }


			$this->jenisBangunanModel->update();

			redirect(site_url('datamaster#tabs-4'));
		} else {
			redirect(site_url('jenisbangunan/'.$nomor.'/edit'));
		}

	}


	public function destroy($nomor) {
		$this->jenisBangunanModel->delete($nomor);
		echo "success";
	}

}
