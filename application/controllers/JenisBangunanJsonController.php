<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class JenisBangunanJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'JenisBangunanModel',
			'jenisBangunanModel',
			TRUE);
	}

	public function getAll() {
		echo json_encode(['data' => $this->jenisBangunanModel->getAll()]);
	}


	public function store() {
		$this->jenisBangunanModel->nama = $this->input->post('nama');
		if($this->jenisBangunanModel->isExists('nama', $this->jenisBangunanModel->nama)) {
			echo json_encode(-1);
		} else {
			$this->jenisBangunanModel->save();
			echo json_encode($this->jenisBangunanModel->getLastNomor());
		}
	}

	public function update($nomor) {
		$this->jenisBangunanModel->nomor = $nomor;
		$this->jenisBangunanModel->nama = $this->input->post('nama');
		$this->jenisBangunanModel->update();
		echo json_encode($nomor);
	}
}
