<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class KecamatanController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'KecamatanModel',
			'kecamatanModel',
			TRUE);
	}


	public function edit($nomor) {
		$this->eupbAuth->checkIfEnable('dapatEditPemohon');

		$this->load->view(
			'master-layout', [
			'title' => 'Kecamatan',
			'page' => 'data_master/kecamatan-form',
			'header' => 'Pembaharuan Data Kecamatan',
			'activating' => 'datamaster',
			'kecamatan' => $this->kecamatanModel->findById($nomor)
			]);
	}

	public function update($nomor) {
		$this->form_validation->set_rules(
			'nama', 'nama', 'required');

		if($this->form_validation->run()) {

			$this->kecamatanModel->nomor = $nomor;

			$this->kecamatanModel->nama =
			$this->input->post('nama');

			if($this->kecamatanModel->isExists('nama', $this->kecamatanModel->nama)
			 && $this->kecamatanModel->nama != $this->input->post('old_nama')) {

				$this->load->view(
					'master-layout', [
					'title' => 'Kecamatan',
					'page' => 'data_master/kecamatan-form',
					'header' => 'Pembaharuan Data Kecamatan',
					'activating' => 'datamaster',
					'kecamatan' => $this->kecamatanModel->findById($nomor),
					'message' => 'kecamatan sudah ada!'
				]);
				return;

			}


			$this->kecamatanModel->update();

			redirect(site_url('datamaster#tabs-3'));
		} else {
			redirect(site_url('kecamatan/'.$nomor.'/edit'));
		}

	}


	public function destroy($nomor) {
		$this->kecamatanModel->delete($nomor);
		echo "success";
	}

}
