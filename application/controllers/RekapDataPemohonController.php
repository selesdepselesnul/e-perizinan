<?php 
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class RekapDataPemohonController extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();

	}

	public function show() {
		$this->load->view('master-layout', [
			'title' => 'Laporan -Rekap Data Pemohon-',
			'header' => 'Laporan -Rekap Data Pemohon-',
			'page' => 'rekap_data_pemohon/show',
			'activating' => 'laporan',
			'controller' => 'rekapdatapemohon'
			]);
	}

}