<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */ 
class PersyaratanController extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();

		$this->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
	}

	public function edit($nomor_pemohon) {

		$this->eupbAuth->checkIfEnable('dapatEditPersyaratan');

		$this->load->view('master-layout', [
			'title' => 'Persyaratan',
			'page' => 'persyaratan/persyaratan-form',
			'header' => 'Pembaharuan Data Persyaratan',
			'activating' => 'persyaratan',
			'persyaratan' => $this->pemohonModel->findPersyaratanById($nomor_pemohon)
			]);
	}

	public function update($nomor_pemohon) {

		$this->eupbAuth->checkIfEnable('dapatEditPersyaratan');

		$selectedPemohonModel = $this->pemohonModel->findById($nomor_pemohon);


		$luas_tanah = $this->input->post('luas_tanah_sertifikat_tanah');

		$selectedPemohonModel->nomorSertifikatTanah = 
		$this->input->post('nomor_sertifikat_tanah');
		$selectedPemohonModel->tahunSertifikatTanah = 
		$this->input->post('tahun_sertifikat_tanah');
		$selectedPemohonModel->luasTanahSertifikatTanah = $luas_tanah == "" ? 0 : $luas_tanah;
		$selectedPemohonModel->atasNamaSertifikatTanah = 
		$this->input->post('atas_nama_sertifikat_tanah');
		$selectedPemohonModel->jenisKepemilikanTanahSertifikatTanah = 
		$this->input->post('jenis_kepemilikan_tanah_sertifikat_tanah');

		$selectedPemohonModel->nomorSuratPermohonan = 
		$this->input->post('nomor_surat_permohonan');
		$selectedPemohonModel->nomorKtp = 
		$this->input->post('nomor_ktp');
		$selectedPemohonModel->nomorSuratKuasa = 
		$this->input->post('nomor_surat_kuasa');	
		$selectedPemohonModel->nomorAktePendirian = 
		$this->input->post('nomor_akte_pendirian');
		$selectedPemohonModel->nomorPbb = 
		$this->input->post('nomor_pbb');
		$selectedPemohonModel->nomorProposalProyek = 
		$this->input->post('nomor_proposal_proyek');
		$selectedPemohonModel->nomorRincianLahan = 
		$this->input->post('nomor_rincian_lahan');				
		$selectedPemohonModel->nomorSuratPernyataan = 
		$this->input->post('nomor_surat_pernyataan');
		$selectedPemohonModel->nomorSuratRencanaPembangunan = 
		$this->input->post('nomor_rencana_pembangunan');
		$selectedPemohonModel->nomorSuratDomisiliPerusahaan = 
		$this->input->post('nomor_domisili_perusahaan');
		$selectedPemohonModel->nomorSuratPemberitahuanTetangga = 
		$this->input->post('nomor_pemberitahuan_tetangga');

		$selectedPemohonModel->update();

		$this->logModel->kegiatan = 'memperbaharui';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'persyaratan dari pemohon dengan nomor : '.$nomor_pemohon;
		$this->logModel->save();

		redirect(site_url('pemohon/'));	
	}

	public function destroy($id) {

		$this->eupbAuth->checkIfEnable('dapatHapusPersyaratan');

		$this->persyaratanModel->delete($id);
		echo "success";
	}
}