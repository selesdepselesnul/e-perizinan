<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class ProfileController extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();
	}

	public function create() {
		$this->load->view('master-layout', [
			'title' => 'Pemohon',
			'page' => 'pemohon/pemohon-form',
			'header' => 'Penambahan Data Pemohon',
			'activating' => 'pemohon',
			'pemohon' => NULL,
			'nama_pemohon' => '',
			'all_jenis_bangunan' =>
			$this->jenisBangunanModel->getAll(),
			'all_kecamatan' => $this->kecamatanModel->getAll()
			]);
	}

	public function update() {
		$this->form_validation->set_rules(
			'password', 'password', 'required');

		$this->form_validation->set_rules(
			'username', 'username', 'required');


		if($this->form_validation->run()) {

			$this->roleModel = $this->roleModel->findById($this->session->role['nomor']);

			if($this->roleModel->isExists('username', $this->input->post('username'))
			 && $this->input->post('username') != $this->input->post('old_username')) {
				 $this->load->view(
					 'master-layout', [
					 'title' => 'Profile Pengguna',
					 'page' => 'profile-form',
					 'header' => 'Pembaharuan Profile',
					 'activating' => 'profile',
					 'message' => 'username sudah dipakai!'
					 ]);
				return;
			 }

			if($this->input->post('password') != $this->input->post('old_password'))
				$this->roleModel->password =
					password_hash($this->input->post('password'), PASSWORD_DEFAULT);

			$this->roleModel->username = $this->input->post('username');
			$this->roleModel->update();

			$this->session->role = (array)$this->roleModel;
			redirect(site_url());
		} else {
			 $this->load->view(
					 'master-layout', [
					 'title' => 'Profile Pengguna',
					 'page' => 'profile-form',
					 'header' => 'Pembaharuan Profile',
					 'activating' => 'profile',
					 'message' => 'field tidak boleh kosong!'
			 ]);
		}
	}

	public function edit() {
		$this->load->view(
			'master-layout', [
			'title' => 'Profile Pengguna',
			'page' => 'profile-form',
			'header' => 'Pembaharuan Profile',
			'activating' => 'profile'
			]);
	}

}
