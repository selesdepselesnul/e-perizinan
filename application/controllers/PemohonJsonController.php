<?php 
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PemohonJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PemohonModel', 
			'pemohonModel',
			TRUE);
		$this->load->model(
			'KelurahanModel', 
			'kelurahanModel',
			TRUE);
		$this->load->model(
			'KecamatanModel', 
			'kecamatanModel',
			TRUE);
		$this->load->model(
			'JenisBangunanModel', 
			'jenisBangunanModel',
			TRUE);
	}

	public function destroy($id) {
		$this->pemohonModel->delete($id);
		echo "success";
	}

	public function getAll() {
		$all_pemohon = array_map(function ($x) {
			$pemohon = (array)$x;
			$pemohon['kelurahan'] = '';
			$pemohon['kecamatan'] = '';
			$pemohon['jenisBangunan'] = '';
			if(!is_null($x->nomorKelurahan)) {
				$kelurahan = $this->kelurahanModel->findById($x->nomorKelurahan);
				$pemohon['kelurahan'] = $kelurahan->nama;
				$pemohon['kecamatan'] = $this->kecamatanModel->findById($kelurahan->nomorKecamatan)->nama;
				$pemohon['jenisBangunan'] = $this->jenisBangunanModel->findById(
					$x->nomorJenisBangunan)->nama;
			} 
			return (object)$pemohon;
		}, $this->pemohonModel->getAll());
		echo json_encode(['data' => $all_pemohon]);
	}

	public function findPersyaratanById($nomor) {
		echo json_encode(['data' => [$this->pemohonModel->findPersyaratanById($nomor)]]);
	}

	public function whereNameLike($pattern_name) {
		echo json_encode(
			$this->pemohonModel->whereNameLike(
				str_replace("%20", " ", $pattern_name))
			);
	}

	public function findById($nomor) {
		$pemohon = (array)$this->pemohonModel->findById($nomor);;
		$pemohon['kelurahan'] = '';
		$pemohon['kecamatan'] = '';
		$pemohon['jenisBangunan'] = '';
		if(!is_null($pemohon['nomorKelurahan'])) {
			$kelurahan = $this->kelurahanModel->findById($pemohon['nomorKelurahan']);
			$pemohon['kelurahan'] = $kelurahan->nama;
			$pemohon['kecamatan'] = $this->kecamatanModel->findById($kelurahan->nomorKecamatan)->nama;
			$pemohon['jenisBangunan'] = $this->jenisBangunanModel->findById(
				$pemohon['nomorJenisBangunan'])->nama;
		}
		echo json_encode(
			['data' => [(object)$pemohon]]);
	}
}