<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class JenisRoleController extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();
		$this->eupbAuth->CheckIfEnable('dapatKonfigurasi');
		$this->load->model(
			'JenisRoleModel',
			'jenisRoleModel',
			TRUE);
	}



	public function edit($nomor) {
		$this->load->view(
			'master-layout', [
			'title' => 'Kewenangan',
			'page' => 'data_master/jenisrole-form',
			'header' => 'Pembaharuan Kewenangan',
			'activating' => 'datamaster',
			'jenisrole' => $this->jenisRoleModel->findById($nomor),
			'eupbElement' => $this->eupbElement
			]);
	}

	public function update($nomor) {

		$selected_jenis_role = $this->jenisRoleModel->findById($nomor);

		$selected_jenis_role->nama = $this->input->post('nama');


		if($selected_jenis_role->isExists('nama', $selected_jenis_role->nama)
		 && $selected_jenis_role->nama != $this->input->post('old_nama')) {

			$this->load->view(
					'master-layout', [
					'title' => 'Kewenangan',
					'page' => 'data_master/jenisrole-form',
					'header' => 'Pembaharuan Kewenangan',
					'activating' => 'datamaster',
					'jenisrole' => $this->jenisRoleModel->findById($nomor),
					'eupbElement' => $this->eupbElement,
					'message' => 'jenis role sudah ada!'
			]);
			return;

		 }

		$selected_jenis_role->dapatKonfigurasi =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_konfigurasi'));

		$selected_jenis_role->dapatTambahPemohon =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_tambah_pemohon'));

		$selected_jenis_role->dapatEditPemohon =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_edit_pemohon'));

		$selected_jenis_role->dapatHapusPemohon =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_hapus_pemohon'));

		$selected_jenis_role->dapatTambahPenerimaKuasa =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_tambah_penerimakuasa'));

		$selected_jenis_role->dapatEditPenerimaKuasa =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_edit_penerimakuasa'));

		$selected_jenis_role->dapatHapusPenerimaKuasa =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_hapus_penerimakuasa'));

		$selected_jenis_role->dapatTambahBerkas =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_tambah_berkas'));

		$selected_jenis_role->dapatEditBerkas =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_edit_berkas'));

		$selected_jenis_role->dapatHapusBerkas =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_hapus_berkas'));

		$selected_jenis_role->dapatEditPersyaratan =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_edit_persyaratan'));

		$selected_jenis_role->dapatTambahPermohonan =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_tambah_permohonan'));

		$selected_jenis_role->dapatEditPermohonan =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_edit_permohonan'));

		$selected_jenis_role->dapatHapusPermohonan =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_hapus_permohonan'));

		$selected_jenis_role->dapatValidasiPermohonan =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_validasi_permohonan'));

		$selected_jenis_role->dapatEditMonitoring =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_edit_monitoring'));

		$selected_jenis_role->dapatHapusMonitoring =
		$this->eupbElement->checkBoxValToIntBool($this->input->post('dapat_hapus_monitoring'));

		$selected_jenis_role->update();

		redirect(site_url('datamaster#tabs-2'));
	}

	public function destroy($nomor) {
		$this->jenisRoleModel->delete($nomor);
		echo "success";
	}

}
