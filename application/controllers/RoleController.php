<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class RoleController extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function edit($nomor) {
		$this->eupbAuth->checkIfEnable('dapatKonfigurasi');

		$this->load->view(
			'master-layout', [
			'title' => 'Role',
			'page' => 'data_master/role-form',
			'header' => 'Pembaharuan Data Role',
			'activating' => 'datamaster',
			'role' => $this->roleModel->findById($nomor),
			'all_jenis_role' => $this->jenisRoleModel->getAll()
			]);
	}


	public function update($nomor) {

		$this->form_validation->set_rules(
			'username', 'username', 'required');
		$this->form_validation->set_rules(
			'password', 'password', 'required');

		if($this->form_validation->run()) {

			$this->roleModel->nomor = $nomor;

			if($this->roleModel->isExists('username', $this->input->post('username'))
			 && $this->input->post('username') != $this->input->post('old_username')) {

				$this->load->view( 'master-layout', [
					'title' => 'Role',
					'page' => 'data_master/role-form',
					'header' => 'Pembaharuan Data Role',
					'activating' => 'datamaster',
					'role' => $this->roleModel->findById($nomor),
					'all_jenis_role' => $this->jenisRoleModel->getAll(),
					'message' => 'username sudah dipakai!'
				]);
				return;
			}
			$this->roleModel->username =
				$this->input->post('username');

			if($this->input->post('password') != $this->input->post('old_password')) {
				$this->roleModel->password =
					password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			}

			$this->roleModel->nomorJenisRole =
			$this->input->post('nomor_jenis_role');

			$this->roleModel->update();

			$this->session->role = (array)$this->roleModel;

			redirect(site_url('datamaster'));
		} else {
			redirect(site_url('role/'.$nomor.'/edit'));
		}

	}
	public function destroy($nomor) {
		$this->roleModel->delete($nomor);
		if($nomor == $this->session->role['nomor']) {
			$this->session->sess_destroy();
			redirect(site_url());
		}
	}

}
