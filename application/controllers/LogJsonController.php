<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class LogJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
	}

	public function whereRoleID($nomor) {
		echo json_encode([ 'data' => $this->logModel->whereRoleID($nomor)]);
	}
	
}
