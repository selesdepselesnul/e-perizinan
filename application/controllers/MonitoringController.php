<?php
/**
 *@author : Moch Deden (https://github.com/selesdepselesnul)
 */
class MonitoringController extends CI_Controller {


	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();

		$this->load->model(
			'PermohonanModel',
			'permohonanModel',
			TRUE);
		$this->load->model(
			'MonitoringModel',
			'monitoringModel',
			TRUE);
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
	}

	public function edit($nomor) {

		$this->eupbAuth->checkIfEnable('dapatEditMonitoring');


		$monitoring = $this->monitoringModel->findById($nomor);
		$permohonan = $this->permohonanModel->findById(
			$monitoring->nomorPermohonan);
		$this->load->view('master-layout', [
			'title' => 'Proses',
			'header' => 'Pembaharuan Data Monitoring',
			'page' => 'monitoring/monitoring-form',
			'activating' => 'monitoring',
			'monitoring' => $monitoring,
			'nama_pemohon' => $permohonan->namaPemohon,
			'nomor_izin_prinsip' => $permohonan->nomorIzinPrinsip,
			'tgl_izin_prinsip' => $permohonan->tglIzinPrinsip
			]);
	}


	public function show() {
		$this->load->view('master-layout', [
			'title' => 'Monitoring',
			'header' => 'Data Monitoring',
			'page' => 'monitoring/show',
			'activating' => 'monitoring'
			]);
	}


	public function update($nomor) {

		$this->eupbAuth->checkIfEnable('dapatEditMonitoring');

		$monitoringModel = $this->monitoringModel->findById($nomor);
		$monitoringModel->nomorIppt = 
		$this->input->post('nomor_ippt');
		$monitoringModel->tglIppt = 
		$this->input->post('tgl_ippt');
		$monitoringModel->dalamProsesIppt = 
		$this->input->post('dalam_proses_ippt');
		$monitoringModel->nomorDokumenLingkungan = 
		$this->input->post('nomor_dokumen_lingkungan');
		$monitoringModel->tglDokumenLingkungan = 
		$this->input->post('tgl_dokumen_lingkungan');
		$monitoringModel->dalamProsesDokumenLingkungan = 
		$this->input->post('dalam_proses_dokumen_lingkungan');
		$monitoringModel->nomorAmdalLalin = 
		$this->input->post('nomor_amdal_lalin');
		$monitoringModel->tglAmdalLalin = 
		$this->input->post('tgl_amdal_lalin');
		$monitoringModel->dalamProsesAmdalLalin = 
		$this->input->post('dalam_proses_amdal_lalin');
		$monitoringModel->nomorKkop = 
		$this->input->post('nomor_kkop');
		$monitoringModel->tglKkop = 
		$this->input->post('tgl_kkop');
		$monitoringModel->dalamProsesKkop = 
		$this->input->post('dalam_proses_kkop');
		$monitoringModel->nomorImb = 
		$this->input->post('nomor_imb');
		$monitoringModel->tglImb = 
		$this->input->post('tgl_imb');
		$monitoringModel->dalamProsesImb = 
		$this->input->post('dalam_proses_imb');
		$monitoringModel->update();

		$this->logModel->kegiatan = 'memperbaharui';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'monitoring dengan nomor : '.$nomor.
		' ;dari pemohon dengan nomor : '. 
		$this->permohonanModel->findById($monitoringModel->nomorPermohonan)->nomorPemohon;

		$this->logModel->save();

		redirect(site_url('monitoring'));
	}

	public function destroy($nomor) {

		$this->eupbAuth->checkIfEnable('dapatHapusMonitoring');

		$this->logModel->kegiatan = 'menghapus';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'monitoring dengan nomor : '.$nomor.
		' ;dari pemohon dengan nomor : '. 
		$this->permohonanModel->findById(
			$this->monitoringModel->findById($nomor)->nomorPermohonan)->nomorPemohon;
		$this->logModel->save();

		$this->monitoringModel->delete($nomor);
		echo "success";
	}
}

