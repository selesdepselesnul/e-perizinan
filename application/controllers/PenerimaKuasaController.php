<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PenerimaKuasaController extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->eupbAuth->checkIfLogin();

		$this->load->model(
			'PenerimaKuasaModel',
			'penerimaKuasaModel',
			TRUE);
		$this->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
	}

	public function create($nomor_pemohon) {

		$this->eupbAuth->checkIfEnable('dapatTambahPenerimaKuasa');

		$this->load->view('master-layout', [
			'title' => 'Penerima Kuasa',
			'page' => 'penerima_kuasa/penerimakuasa-form',
			'header' => 'Penambahan Data Penerima Kuasa',
			'activating' => 'penerimakuasa',
			'nomor_pemohon' => $nomor_pemohon,
			'nama_pemohon' => $this->pemohonModel->findById($nomor_pemohon)->nama,
			'penerimakuasa' => NULL
			]);
	}

	public function edit($nomor_pemohon, $nomor_penerima_kuasa) {

		$this->eupbAuth->checkIfEnable('dapatEditPenerimaKuasa');

		$this->load->view('master-layout', [
			'title' => 'Penerima Kuasa',
			'page' => 'penerima_kuasa/penerimakuasa-form',
			'header' => 'Pembaharuan Data Penerima Kuasa',
			'activating' => 'penerimakuasa',
			'nomor_pemohon' => $nomor_pemohon,
			'nama_pemohon' => $this->pemohonModel->findById($nomor_pemohon)->nama,
			'penerimakuasa' => $this->penerimaKuasaModel->findById($nomor_penerima_kuasa)
			]);
	}

	public function update($nomor_pemohon, $nomor_penerima_kuasa) {

		$this->eupbAuth->checkIfEnable('dapatEditPenerimaKuasa');

		$this->saveWithMethod(function() use($nomor_pemohon, $nomor_penerima_kuasa) {
			$this->penerimaKuasaModel->nomorPemohon = $nomor_pemohon;
			$this->penerimaKuasaModel->nomor = $nomor_penerima_kuasa;
			$this->penerimaKuasaModel->update();

			$this->logModel->kegiatan = 'memperbaharui';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->data = 'penerimakuasa dengan nomor : '
			.$nomor_penerima_kuasa.'; dari pemohon dengan nomor : '.$nomor_pemohon;
			$this->logModel->save();
		}, 'penerimakuasa/'.$nomor_pemohon.'/'.$nomor_penerima_kuasa);
	}

	public function store($nomor_pemohon) {

		$this->eupbAuth->checkIfEnable('dapatTambahPenerimaKuasa');

		$this->saveWithMethod(function() use($nomor_pemohon){
			$this->penerimaKuasaModel->nomorPemohon = $nomor_pemohon;
			$this->penerimaKuasaModel->save();

			$this->logModel->kegiatan = 'menambahkan';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->data = 'penerimakuasa dengan nomor : '
			.$this->penerimaKuasaModel->getLastNomor().'; dari pemohon dengan nomor : '.$nomor_pemohon;
			$this->logModel->save();

		}, 'penerimakuasa/'.$nomor_pemohon.'/create');
	}

	private function saveWithMethod($method, $reroute) {
		$this->form_validation->set_rules(
			'nama', 'Nama', 'required');

		if($this->form_validation->run()) {
			$this->penerimaKuasaModel->nama =
			$this->input->post('nama');
			$this->penerimaKuasaModel->alamat =
			$this->input->post('alamat');
			$this->penerimaKuasaModel->noHp =
			$this->input->post('no_hp');

			$method();
			redirect(site_url('pemohon'));
		} else {
			redirect(site_url($reroute));
		}

	}

	public function show($page) {
		$this->load->view('master-layout', [
			'title' => 'Penerima Kuasa',
			'header' => 'Data Penerima Kuasa',
			'activating' => 'penerimakuasa',
			'page' => 'master-show',
			'table_header' => 'penerima_kuasa/table-headers',
			'table_data' => 'penerima_kuasa/table-datas',
			'total_rows' => $this->penerimaKuasaModel->count(),
			'controller' => 'penerimakuasa',
			'some_model' =>
			$this->penerimaKuasaModel->getLimit(5, $page)
			]);
	}

	public function destroy($id) {

		$this->eupbAuth->checkIfEnable('dapatHapusPenerimaKuasa');

		$this->logModel->kegiatan = 'menghapus';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'penerimakuasa dengan nomor : '
		.$id.'; dari pemohon dengan nomor : '.$this->penerimaKuasaModel->findById($id)->nomorPemohon;
		$this->logModel->save();

		$this->penerimaKuasaModel->delete($id);

		echo "success";
	}
}
