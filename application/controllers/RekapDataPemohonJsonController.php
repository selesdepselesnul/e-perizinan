<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class RekapDataPemohonJsonController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PermohonanModel',
			'permohonanModel',
			TRUE);
		$this->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);
		$this->load->model(
			'KelurahanModel',
			'kelurahanModel',
			TRUE);
		$this->load->model(
			'KecamatanModel',
			'kecamatanModel',
			TRUE);
	}

	private function constructRekapDataPemohon($permohonan) {
		$all_rekap_data_pemohon = [];
		foreach ($permohonan as $i => $p) {
			$pemohon = $this->pemohonModel->findById($p->nomorPemohon);

			$kelurahan = $this->kelurahanModel->findById($pemohon->nomorKelurahan);

			$nama_kelurahan = '';
			$nama_kecamatan = '';
			if(!is_null($kelurahan)) {
				$nama_kelurahan = $kelurahan->nama;
				$nama_kecamatan = $this->kecamatanModel->findById($kelurahan->nomorKecamatan)->nama;
			}

			$all_rekap_data_pemohon[] = (object) [
			"nomor" => $i+1,
			"tglTinjauanLapangan" => $p->tglTinjauanLapangan,
			"tglPenerimaPerizinan" => $p->tglPenerimaPerizinan,
			"penerimaPerizinan" => $p->penerimaPerizinan,
			"validasi1" => $p->validasi1,
			"validasi2" => $p->validasi2,
			"validasi3" => $p->validasi3,
			"statusValidasi" => $this->permohonanModel->getValidationStatus($p->nomor),
			"nomorPermohonan" => $p->nomor,
			"namaPemohon" => $pemohon->nama,
			"alamatPemohon" => $pemohon->alamat,
			"tglPermohonan" => $p->tglPermohonan,
			"jenisKegiatan" => $p->jenisKegiatan,
			"alamatLokasi" =>  $pemohon->alamatLokasi. ' rt '. $pemohon->rt
			. ' rw '.$pemohon->rw. ' Kel. '.$nama_kelurahan.', Kec. '
			. $nama_kecamatan,
			"luasLahan" => $pemohon->luasTanah,
			"luasBangunan" => $pemohon->luasBangunan,
			"pembahasanRapatBKPRD" => $p->pembahasanRapatBKPRD,
			"tglIzinPrinsip" => $p->tglIzinPrinsip,
			"nomorIzinPrinsip" => $p->nomorIzinPrinsip,
			"tglIzinLokasi" => $p->tglIzinLokasi,
			"nomorIzinLokasi" => $p->nomorIzinLokasi];
		}

		return $all_rekap_data_pemohon;
	}

	public function getAll() {
		$this->constructRekapDataPemohon($this->permohonanModel->getAll());
	}

	public function getFirstYear() {
		echo json_encode($this->permohonanModel->getFirstYear());

	}

	public function getLastYear() {
		echo json_encode($this->permohonanModel->getLastYear());

	}

	public function onYear($year) {
		echo json_encode(['data' =>
			$this->constructRekapDataPemohon($this->permohonanModel->onYear($year))]);
	}

	public function onMonthYear($month, $year) {
		$raw_all_rekap_data_pemohon = $this->constructRekapDataPemohon(
			$this->permohonanModel->onMonthYear($month, $year)
			);

		$all_rekap_data_pemohon = array_map(function ($p) {
			$rekap_data_per_month = (array)$p;
			$rekap_data_per_month['validasi'] = $p->statusValidasi;
			return (object)$rekap_data_per_month;
		}, $raw_all_rekap_data_pemohon);
		echo json_encode(['data' => $all_rekap_data_pemohon]);
	}



}
