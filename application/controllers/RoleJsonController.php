<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class RoleJsonController extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}

	public function getAll() {

		$all_roles = array_map(function($r) {
			$role = (array)$r;
			$role['jenis'] = $this->jenisRoleModel
								  ->findById($role['nomorJenisRole'])
								  ->nama;
			return (object)$role;
		}, $this->roleModel->getAll());
		echo json_encode(['data' => $all_roles]);
	}

	public function store() {
		$this->roleModel->username = $this->input->post('usernameRole');

		if($this->roleModel->isExists('username', $this->roleModel->username)) {
			echo json_encode(-1);
		} else {
			$this->roleModel->nomorJenisRole = $this->input->post('jenisRole');
			$this->roleModel->save();
			echo json_encode($this->roleModel->getLastNomor());
		}

	}
}
