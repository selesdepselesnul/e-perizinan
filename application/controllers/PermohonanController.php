<?php
/**
 *@author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PermohonanController extends CI_Controller {


	public function __construct() {

		parent::__construct();

		$this->eupbAuth->checkIfLogin();

		$this->load->model(
			'PermohonanModel',
			'permohonanModel',
			TRUE);
		$this->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);
		$this->load->model(
			'MonitoringModel',
			'monitoringModel',
			TRUE);
		$this->load->model(
			'LogModel',
			'logModel',
			TRUE);
		$this->load->helper('date');


	}

	public function create() {

		$this->eupbAuth->checkIfEnable('dapatTambahPermohonan');

		$this->load->view('master-layout', [
			'title' => 'Proses',
			'page' => 'permohonan/permohonan-form',
			'header' => 'Penambahan Data Proses',
			'activating' => 'permohonan',
			'all_id_pemohon' => $this->pemohonModel->getAllId(),
			'permohonan' => NULL,
			'nama_pemohon' => ''
			]);
	}

	public function edit($nomor) {

		$this->eupbAuth->checkIfEnable('dapatEditPermohonan');

		$permohonan = $this->permohonanModel->findById($nomor);
		$pemohon = $this->pemohonModel->findById(
			$permohonan->nomorPemohon);
		$this->load->view('master-layout', [
			'title' => 'Proses',
			'header' => 'Pembaharuan Data Proses',
			'page' => 'permohonan/permohonan-form',
			'activating' => 'permohonan',
			'all_id_pemohon' => $this->pemohonModel->getAllId(),
			'permohonan' => $permohonan,
			'nama_pemohon' => $pemohon->nama
			]);
	}

	public function checkValidation($id) {

		$this->eupbAuth->checkIfEnable('dapatValidasiPermohonan');

		$this->load->view('master-layout', [
			'title' => 'Validasi Proses',
			'header' => 'Validasi Proses',
			'page' => 'permohonan/validation',
			'activating' => 'permohonan',
			'all_id_pemohon' => $this->pemohonModel->getAllId(),
			'permohonan' => $this->permohonanModel->findById($id)
			]);
	}

	public function saveWithMethod($method) {
		$this->form_validation->set_rules(
			'nomor_pemohon', 'Nomor Pemohon', 'required');
		$this->form_validation->set_rules(
			'nama_pemohon', 'Nama Pemohon', 'required');

		if($this->form_validation->run()) {

			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');

			$this->permohonanModel->nomorPemohon =
			$this->input->post('nomor_pemohon');
			$this->permohonanModel->namaPemohon =
			$this->input->post('nama_pemohon');
			$this->permohonanModel->latitude =
			$latitude == '' ? 0 : $latitude;
			$this->permohonanModel->longitude =
			$longitude == '' ? 0 : $longitude;
			$this->permohonanModel->beritaAcaraLapangan =
			$this->input->post('berita_acara_lapangan');
			$this->permohonanModel->jenisKegiatan = html_escape(
				$this->input->post('jenis_kegiatan'));
			$this->permohonanModel->tglIzinPrinsip =
			$this->input->post('tgl_izin_prinsip');
			$this->permohonanModel->nomorIzinPrinsip =
			$this->input->post('nomor_izin_prinsip');
			$this->permohonanModel->izinPrinsip =
			$this->input->post('izin_prinsip');
			$this->permohonanModel->tglBerkasMasuk =
			$this->input->post('tgl_berkas_masuk');
			$this->permohonanModel->tglPembahasan =
			$this->input->post('tgl_pembahasan');
			$this->permohonanModel->pembahasanRapatBKPRD =
			$this->input->post('pembahasan_rapat_bkprd');
			$this->permohonanModel->tglTinjauanLapangan =
			$this->input->post('tgl_tinjauan_lapangan');
			$this->permohonanModel->penerimaPerizinan =
			$this->input->post('penerima_perizinan');
			$this->permohonanModel->tglPenerimaPerizinan =
			$this->input->post('tgl_penerima_perizinan');
			$this->permohonanModel->tglIzinLokasi =
			$this->input->post('tgl_izin_lokasi');
			$this->permohonanModel->nomorIzinLokasi =
			$this->input->post('nomor_izin_lokasi');
			$method();
			redirect(site_url('permohonan/'));
		} else {
			redirect(site_url('permohonan/create'));
		}
	}

	public function store() {

		$this->eupbAuth->checkIfEnable('dapatTambahPermohonan');

		$this->saveWithMethod(function () {
			$this->permohonanModel->tglPermohonan = date("d/m/Y");
			$this->permohonanModel->save();
			$this->monitoringModel->nomorPermohonan =
			$this->permohonanModel->getLastNomor();
			$this->monitoringModel->save();

			$this->logModel->kegiatan = 'menambahkan';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->data = 'proses dengan nomor : '.$this->monitoringModel->nomorPermohonan.
			' ;dari pemohon dengan nomor : '.
			$this->permohonanModel->findById($this->monitoringModel->nomorPermohonan)->nomorPemohon;
			$this->logModel->save();
		});
	}

	public function show($page) {
		$this->load->view('master-layout', [
			'title' => 'Proses',
			'header' => 'Data Proses',
			'page' => 'permohonan/show',
			'activating' => 'permohonan',
			'some_permohonan' =>
			$this->permohonanModel->getLimit(5, $page),
			'total_rows' => $this->permohonanModel->count()
			]);
	}


	public function update($nomor) {

		$this->eupbAuth->checkIfEnable('dapatEditPermohonan');

		$this->saveWithMethod(function() use($nomor) {
			$this->permohonanModel->tglPermohonan =
			$this->permohonanModel->findById($nomor)->tglPermohonan;
			$this->permohonanModel->nomor = $nomor;
			$this->permohonanModel->update();

			$this->logModel->kegiatan = 'memperbaharui';
			$this->logModel->nomorRole = $this->session->role['nomor'];
			$this->logModel->waktu = now_wib();
			$this->logModel->data = 'proses dengan nomor : '.$nomor.
			' ;dari pemohon dengan nomor : '.
			$this->permohonanModel->findById($nomor)->nomorPemohon;
			$this->logModel->save();

		});
	}

	public function destroy($id) {

		$this->logModel->kegiatan = 'menghapus';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'proses dengan nomor : '.$id.
		' ;dari pemohon dengan nomor : '.
		$this->permohonanModel->findById($id)->nomorPemohon;
		$this->logModel->save();

		$this->permohonanModel->delete($id);
		echo "success";
	}

	private function radioToBool($x) {
		if ($x == 'accept')
			return true;
		else if($x == 'deny')
			return false;
		else
			return NULL;
	}

	public function validate($nomor) {

		$this->eupbAuth->checkIfEnable('dapatValidasiPermohonan');

		$old_permohonan = $this->permohonanModel->findById($nomor);

		$old_permohonan->nomor = $old_permohonan->nomor;
		$old_permohonan->nomorPemohon =  $old_permohonan->nomorPemohon;
		$old_permohonan->latitude = $old_permohonan->latitude;
		$old_permohonanModel->longitude = $old_permohonan->longitude;
		$old_permohonan->validasi1 =
		$this->radioToBool($this->input->post('validasi1'));
		$old_permohonan->validasi2 =
		$this->radioToBool($this->input->post('validasi2'));
		$old_permohonan->validasi3 =
		$this->radioToBool($this->input->post('validasi3'));
		$old_permohonan->nomorNotaDinasPenolakan =
		$this->input->post('nomor_nota_dinas_penolakan');

		$old_permohonan->update();

		$this->logModel->kegiatan = 'memvalidasi';
		$this->logModel->nomorRole = $this->session->role['nomor'];
		$this->logModel->waktu = now_wib();
		$this->logModel->data = 'proses dengan nomor : '.$nomor.
		' ;dari pemohon dengan nomor : '.
		$this->permohonanModel->findById($nomor)->nomorPemohon;
		$this->logModel->save();
		redirect(site_url('permohonan/'));
	}
}
