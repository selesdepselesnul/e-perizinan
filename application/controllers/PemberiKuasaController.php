<?php 
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class PemberiKuasaController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(
			'PemberiKuasaModel', 
			'pemberiKuasaModel',
			TRUE);
		$this->load->model(
			'JenisBangunanModel',
			'jenisBangunanModel',
			TRUE);
		$this->load->model(
			'KecamatanModel',
			'kecamatanModel',
			TRUE);
		$this->load->helper('url');
		$this->eupbAuth->checkLoginAs(EupbAuth::OPERATOR);
	}

	public function edit($id) {
		$this->load->view('master-layout', [
			'title' => 'Pemberi Kuasa',
			'page' => 'pemberi_kuasa/pemberikuasa-form',
			'header' => 'Pembaharuan Data Pemberi Kuasa',
			'activating' => 'pemberikuasa',
			'pemberikuasa' => $this->pemberiKuasaModel->findById($id), 
			'all_jenis_bangunan' => 
			$this->jenisBangunanModel->getAll(),
			'all_kecamatan' => $this->kecamatanModel->getAll() 
			]);
	}

	public function create() {
		$this->load->view('master-layout', [
			'title' => 'Pemberi Kuasa',
			'page' => 'pemberi_kuasa/pemberikuasa-form',
			'header' => 'Penambahan Data Pemberi Kuasa',
			'pemberikuasa' => NULL,
			'activating' => 'pemberikuasa', 
			'all_jenis_bangunan' => 
			$this->jenisBangunanModel->getAll(),
			'all_kecamatan' => $this->kecamatanModel->getAll() 
			]);
	}

	public function store() {
		$this->saveWithMethod(function() {
			$this->pemberiKuasaModel->save();
		});
	}

	public function show($page) {
		$this->load->view('master-layout', [
			'title' => 'Pemberi Kuasa',
			'header' => 'Data Pemberi Kuasa',
			'activating' => 'pemberikuasa',
			'page' => 'master-show',
			'controller' => 'pemberikuasa',
			'table_header' => 'pemberi_kuasa/table-headers',
			'table_data' => 'pemberi_kuasa/table-datas',
			'total_rows' => $this->pemberiKuasaModel->count(),
			'some_model' => 
			$this->pemberiKuasaModel->getLimit(
				5, $page)
			]);
	}

	private function saveWithMethod($method) {
		$this->pemberiKuasaModel->nama = 
		$this->input->post('nama');
		
		$this->pemberiKuasaModel->alamat =	
		$this->input->post('alamat');
		
		$this->pemberiKuasaModel->luasBangunan = 	
		$this->input->post('luas_bangunan');
		
		$this->pemberiKuasaModel->luasTanah =	
		$this->input->post('luas_tanah');
		
		$this->pemberiKuasaModel->rt = 
		$this->input->post('rt');
		
		$this->pemberiKuasaModel->rw =	
		$this->input->post('rw');
		
		$this->pemberiKuasaModel->jenisBangunan = 	
		$this->input->post('jenis_bangunan');
		
		$this->pemberiKuasaModel->kecamatan = 
		$this->input->post('kecamatan');
		
		$this->pemberiKuasaModel->kelurahan = 
		$this->input->post('kelurahan');
		$method();
		redirect('pemberikuasa');
	}

	public function update($id) {
		$this->saveWithMethod(function() use($id) {
			$this->pemberiKuasaModel->id = $id;
			$this->pemberiKuasaModel->update();
		});
	}

	public function destroy($id) {
		$this->pemberiKuasaModel->delete($id);
		echo "success";
	}

}