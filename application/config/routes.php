<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'MainController';

$route['json/suratkuasapemberi/(:num)']['GET'] = 'SuratKuasaPemberiJson/show/$1';

$route['login']['GET'] = 'MainController/login';
$route['login']['POST'] = 'MainController/postLogin';
$route['logout']['GET'] = 'MainController/logout';
$route['latlong']['GET'] = 'MainController/latlong/0/0';
$route['latlong/(:any)/(:any)']['GET'] = 'MainController/latlong/$1/$2';
$route['about']['GET'] = 'MainController/about';
$route['laporan']['GET'] = 'MainController/laporan';
$route['datamaster']['GET'] = 'MainController/datamaster';
$route['profile']['GET'] = 'ProfileController/edit';
$route['profile']['POST'] = 'ProfileController/update';

$route['pemohon']['GET']= 'PemohonController/show/';
$route['pemohon/create']['GET']= 'PemohonController/create';
$route['pemohon/(:num)/edit']['GET']= 'PemohonController/edit/$1';
$route['pemohon/(:num)']['POST']= 'PemohonController/update/$1';
$route['pemohon']['POST']= 'PemohonController/store';
$route['pemohon/(:num)']['DELETE']=
'PemohonController/destroy/$1';


$route['json/jenisrole']['POST'] = 'JenisRoleJsonController/store';
$route['json/pemohon']['GET']=
'PemohonJsonController/getAll';
$route['json/persyaratan']['GET']=
'PersyaratanJsonController/getAll';
$route['json/pemberikuasa/wherenamelike/(:any)']['GET']=
'PemberiKuasaJsonController/whereNameLike/$1';
$route['json/pemohon/wherenamelike/(:any)']['GET']=
'PemohonJsonController/whereNameLike/$1';
$route['json/penerimakuasa/wherepemohonid/(:any)']['GET']=
'PenerimaKuasaJsonController/wherePemohonID/$1';
$route['json/persyaratan/wherepemohonid/(:any)']['GET']=
'PersyaratanJsonController/wherePemohonID/$1';
$route['json/pemohon/(:num)/persyaratan']['GET']=
'PemohonJsonController/findPersyaratanById/$1';
$route['json/pemohon/(:num)']['GET']=
'PemohonJsonController/findById/$1';
$route['json/permohonan']['GET']=
'PermohonanJsonController/getAll';
$route['json/permohonan/countvalidation']['GET']=
'PermohonanJsonController/countValidation';
$route['json/rekapdatapemohon']['GET']=
'RekapDataPemohonJsonController/getAll';
$route['json/rekapdatapemohon/getfirstyear']['GET']=
'RekapDataPemohonJsonController/getFirstYear';
$route['json/rekapdatapemohon/getlastyear']['GET']=
'RekapDataPemohonJsonController/getLastYear';
$route['json/rekapdatapemohon/year/(:num)']['GET']=
'RekapDataPemohonJsonController/onYear/$1';
$route['json/rekapdatapemohon/month/(:num)/year/(:num)']['GET']=
'RekapDataPemohonJsonController/onMonthYear/$1/$2';
$route['json/jenisbangunan']['GET']=
'JenisBangunanJsonController/getAll';
$route['json/jenisbangunan']['POST']=
'JenisBangunanJsonController/store';
$route['json/kecamatan']['POST']=
'KecamatanJsonController/store';
$route['json/role']['POST']=
'RoleJsonController/store';
$route['json/kelurahan']['POST']=
'KelurahanJsonController/store';
$route['json/jenisbangunan/(:num)']['POST']=
'JenisBangunanJsonController/update/$1';
$route['json/kecamatan/(:num)']['POST']=
'KecamatanJsonController/update/$1';
$route['json/berkas/wherepemohonid/(:num)']['GET']=
'BerkasJsonController/wherePemohonID/$1';
$route['json/log/whereroleid/(:num)']['GET']=
'LogJsonController/whereRoleID/$1';
$route['json/kelurahan/(:num)']['POST']=
'KelurahanJsonController/update/$1';


$route['penerimakuasa/(:num)/create']['GET']= 'PenerimaKuasaController/create/$1';
$route['penerimakuasa/(:num)']['POST']= 'PenerimaKuasaController/store/$1';
$route['penerimakuasa']['GET']= 'PenerimaKuasaController/show/0';
$route['penerimakuasa/(:num)']['GET']= 'PenerimaKuasaController/show/$1';
$route['penerimakuasa/(:num)/(:num)/edit']['GET']= 'PenerimaKuasaController/edit/$1/$2';
$route['penerimakuasa/(:num)/(:num)']['POST']= 'PenerimaKuasaController/update/$1/$2';
$route['penerimakuasa/(:num)']['DELETE']= 'PenerimaKuasaController/destroy/$1';



$route['berkas/(:num)/create']['GET'] = 'BerkasController/create/$1';
$route['berkas/(:num)/edit']['GET'] = 'BerkasController/edit/$1';
$route['berkas/(:num)/(:num)']['POST'] = 'BerkasController/update/$1/$2';
$route['berkas/(:num)']['POST'] = 'BerkasController/store/$1';

$route['persyaratan/create']['GET'] = 'PersyaratanController/create';
$route['persyaratan']['POST'] = 'PersyaratanController/store';
$route['persyaratan']['GET'] = 'PersyaratanController/show/0';
$route['persyaratan/(:num)']['GET'] = 'PersyaratanController/show/$1';
$route['persyaratan/(:num)/edit']['GET']= 'PersyaratanController/edit/$1';
$route['persyaratan/(:num)']['POST']= 'PersyaratanController/update/$1';
$route['persyaratan/(:num)']['DELETE']=
'PersyaratanController/destroy/$1';

$route['permohonan/create']['GET'] = 'PermohonanController/create';
$route['permohonan']['POST'] = 'PermohonanController/store';
$route['permohonan']['GET'] = 'PermohonanController/show/0';
$route['permohonan/(:num)']['GET'] = 'PermohonanController/show/$1';
$route['permohonan/(:num)/edit']['GET']= 'PermohonanController/edit/$1';
$route['permohonan/(:num)']['POST']= 'PermohonanController/update/$1';
$route['permohonan/(:num)']['DELETE']=
'PermohonanController/destroy/$1';
$route['permohonan/checkvalidation/(:num)']['GET']=
'PermohonanController/checkValidation/$1';
$route['permohonan/validate/(:num)']['POST']=
'PermohonanController/validate/$1';

$route['monitoring']['GET'] = 'MonitoringController/show';
$route['monitoring/(:num)/edit']['GET'] = 'MonitoringController/edit/$1';
$route['monitoring/(:num)']['POST'] = 'MonitoringController/update/$1';
$route['monitoring/(:num)']['DELETE']= 'MonitoringController/destroy/$1';


$route['role/(:num)/edit']['GET'] = 'RoleController/edit/$1';
$route['role/(:num)']['POST'] = 'RoleController/update/$1';

$route['jenisrole/(:num)/edit']['GET'] = 'JenisRoleController/edit/$1';
$route['jenisrole/(:num)']['POST'] = 'JenisRoleController/update/$1';

$route['kecamatan/(:num)/edit']['GET'] = 'KecamatanController/edit/$1';
$route['kecamatan/(:num)']['POST'] = 'KecamatanController/update/$1';

$route['jenisbangunan/(:num)/edit']['GET'] = 'JenisBangunanController/edit/$1';
$route['jenisbangunan/(:num)']['POST'] = 'JenisBangunanController/update/$1';

$route['json/database/export']['POST'] = 'DatabaseController/export';
$route['json/database/import']['POST'] = 'DatabaseController/import';

$route['kelurahan/(:num)/create']['GET']= 'KelurahanController/create/$1';
$route['kelurahan/(:num)']['POST']= 'KelurahanController/store/$1';
$route['kelurahan/(:num)/(:num)/edit']['GET'] = 'KelurahanController/edit/$1/$2';
$route['kelurahan/(:num)/(:num)']['POST'] = 'KelurahanController/update/$1/$2';


$route['jenisbangunan/(:num)']['DELETE']=
'JenisBangunanController/destroy/$1';
$route['kecamatan/(:num)']['DELETE']=
'KecamatanController/destroy/$1';
$route['role/(:num)']['DELETE']=
'RoleController/destroy/$1';
$route['berkas/(:num)']['DELETE']=
'BerkasController/destroy/$1';
$route['jenisrole/(:num)']['DELETE']=
'JenisRoleController/destroy/$1';
$route['kelurahan/(:num)']['DELETE']=
'KelurahanController/destroy/$1';
$route['log/(:num)']['DELETE']=
'LogController/destroy/$1';


$route['rekapdatapemohon']['GET'] = 'RekapDataPemohonController/show';

$route['json/kecamatan']['GET'] = 'KecamatanJsonController/getAll';
$route['json/kelurahan']['GET'] = 'KelurahanJsonController/showAll';
$route['json/kelurahan/whereid/(:num)']['GET'] =
'KelurahanJsonController/findById/$1';
$route['json/kecamatan/whereid/(:num)']['GET'] =
'KecamatanJsonController/findById/$1';
$route['json/role']['GET'] =
'RoleJsonController/getAll';
$route['json/kelurahan/wherekecamatanid/(:num)']['GET'] =
'KelurahanJsonController/whereKecamatanId/$1';
$route['json/monitoring']['GET'] = 'MonitoringJsonController/getAll';
$route['json/jenisrole']['GET'] = 'JenisRoleJsonController/getAll';
$route['json/jenisrole/whereid/(:num)']['GET'] = 'JenisRoleJsonController/findById/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['playground']['GET'] = 'MainController/playground';
