<?php

function now_wib() {
	$date = new DateTime("now", new DateTimeZone('Asia/Jakarta'));
	return $date->format(DateTime::COOKIE);
}