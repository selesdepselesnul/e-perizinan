<?php
/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
class EupbAuth {

	private $CI;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->load->helper('url');
		$this->CI->load->model(
			'LogModel',
			'logModel',
			TRUE);
	}

	private function ifLoginOrElse($if_login_action, $else_action){
		if ($this->hasLoginSession())
			$if_login_action();
		else
			$else_action();
	}


	private function hasLoginSession() {
		return $this->CI->session->has_userdata('jenisRole');
	}

	public function checkIfLogin() {
		if(!$this->CI->session->has_userdata('jenisRole'))
			redirect(site_url('login'));
	}

	public function ifLogin($action) {
		$this->ifLoginOrElse($action, function(){
			redirect(site_url('login'));
		});
	}

	public function ifNotLogin($action) {
		if (!$this->hasLoginSession())
			$action();
		else
			redirect(index_page());
	}

	public function CheckIfEnable($actionString) {
		$role = $this->CI->roleModel->findById($this->CI->session->role['nomor']);
		$jenisRoleModel = (array)$this->CI->jenisRoleModel->findById($role->nomorJenisRole);
		if ($jenisRoleModel[$actionString] != 1)
			redirect(site_url('login'));
	}

	public function logout() {
		$this->CI->logModel->kegiatan = 'logout';
		$this->CI->logModel->nomorRole = $this->CI->session->role['nomor'];
		$this->CI->logModel->waktu = now_wib();
		$this->CI->logModel->save();

		$this->CI->session->sess_destroy();
		redirect(site_url());
	}
}
