<?php

class EupbElement {

	private $CI;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->library('pagination');
		$this->CI->load->model(
			'PemohonModel',
			'pemohonModel',
			TRUE);
	}

	public function generateOption($xs, $selected_item='', $className='') { 
		array_walk($xs, function ($x) use ($selected_item, $className) {
			if($x->nomor == $selected_item)
				echo '<option value="'
			.$x->nomor.'" selected class="'.$className.'">'.$x->nama.
			'</option>';
			else
				echo '<option value="'.$x->nomor.
			'" class="'.$className.'">'.$x->nama.'</option>';
		});	
	}

	public function generateOptionNonObj($xs, $selected_item='', $className='') { 
		array_walk($xs, function ($x) use ($selected_item, $className) {
			if($x == $selected_item)
				echo '<option value="'
			.$x.'" selected class="'.$className.'">'.$x.
			'</option>';
			else
				echo '<option value="'.$x.
			'" class="'.$className.'">'.$x.'</option>';
		});	
	}

	public function intToCheckBoxVal($x) {
		return $x == 1 ? "checked" : "";
	}

	public function checkBoxValToIntBool($x) {
		return $x === 'on' ? 1 : 0;
	}

	public function openFormWithValidation($post_url) {
		echo validation_errors(); 
		echo form_open(site_url($post_url)); 
	}

	public function openFormGroup($name, $label, $type) {
		echo '<div class="form-group">'.
		"<label for=\"{$name}\">{$label} : </label>".
		"<input name=\"$name\" type=\"$type\" class=\"form-control\" /></div>";
	}

	public function generateJenisPermohonanOpt() {
		echo '<div class="form-group">'.
		'<label for="jenis_permohonan">Jenis permohonan : </label>'.
		'<select name="jenis_permohonan" class="form-control">';

		$all_jenis_permohonan = $this->CI->pemohonModel->getAllJenisPermohonan();
		foreach ( $all_jenis_permohonan as $jenis_permohonan) {
			echo '<option value="'.$jenis_permohonan->jenisPermohonan.'">'. 
			$jenis_permohonan->jenisPermohonan.'</option>';
		} 
		echo '</select></div>';
	}

	public function generatePagination($url, $total_rows) {
		$config['base_url'] = 'http://127.0.0.1:8080/e-upb/'.$url.'/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 5;
		$config['full_tag_open'] = '<li>';
		$config['full_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<kbd>';
		$config['cur_tag_close'] = '</kbd>';
		$this->CI->pagination->initialize($config);
		echo '<ul class="pagination">';
		echo $this->CI->pagination->create_links();
		echo '</ul>';
	}
}