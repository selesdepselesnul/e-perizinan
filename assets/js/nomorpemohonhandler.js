/**
* @author : Moch Deden (https://github.com/selesdepselesnul)
*
*/
$(document).ready(function() {

	$('#namaPemohon').on('input', function() {
		const pattern = $('#namaPemohon').val()
		var url = 'json/pemohon/wherenamelike/""'
		if(pattern !== '') {
			url = 'json/pemohon/wherenamelike/' + pattern
			$('.nomor-pemohon').remove()
		}



		$.getJSON(BASE_URL+url)
		.done(function (xs) {

			const uniquePemohonNames = _.uniq(
				_.map(xs, function(x) {
					return x.nama
				})
				)

			console.log("the object = ")
			console.log(xs)

			$( "#namaPemohon" ).autocomplete({
				source: uniquePemohonNames
			})

			const filteredPemberiKuasa = _.filter(xs, function(x) {
				return x.nama.toLowerCase() == pattern.toLowerCase()
			})

			console.log('filteredPemberiKuasa = ' + filteredPemberiKuasa)

			_.each(filteredPemberiKuasa, function(x) {
				$( "#nomorPemohon" )
				.append('<option class="nomor-pemohon">'
					+x.nomor+'</option>')
			})

		})
	})

	$('#namaPemohon').on('autocompleteselect', function (e, ui) {
        $.getJSON(BASE_URL+'json/pemohon/wherenamelike/'+ui.item.value)
		.done(function (xs) {
			_.each(xs, function(x) {
				$( "#nomorPemohon" )
				.append('<option class="nomor-pemohon">'
					+x.nomor+'</option>')
			})
		})
    })

})