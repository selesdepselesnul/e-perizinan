/**
* @author : Moch Deden (https://github.com/selesdepselesnul)
*
*/
$(document).ready(function(e) {

	$('#namaPemberiKuasa').on('input', function() {
		const pattern = $('#namaPemberiKuasa').val()
		var url = 'json/pemberikuasa/wherenamelike/""'
		if(pattern !== '') {
			url = 'json/pemberikuasa/wherenamelike/' + pattern
			$('.id-pemberi-kuasa').remove()
		}

		console.log('something changing! bruh!')
		console.log('the pattern = ' + pattern)

		$.getJSON(BASE_URL+url)
		 .done(function (xs) {

		 	console.log(xs)
			const uniquePemberiKuasaNames = _.uniq(
				_.map(xs, function(x) {
					return x.nama
				})
			)

			$( "#namaPemberiKuasa" ).autocomplete({
				source: uniquePemberiKuasaNames
			})

			const filteredPemberiKuasa = _.filter(xs, function(x) {
				return x.nama.toLowerCase() == pattern.toLowerCase()
			})

			console.log('filtered pemberikuasa = '+filteredPemberiKuasa)

			_.each(filteredPemberiKuasa, function(x) {
				$( "#idPemberiKuasa" )
				.append('<option class="id-pemberi-kuasa">'
				 	     +x.id+'</option>')
			})

		 })
	})


	$('#namaPemberiKuasa').on('autocompleteselect', function (e, ui) {
        $.getJSON(BASE_URL+'json/pemberikuasa/wherenamelike/'+ui.item.value)
		.done(function (xs) {
			console.log('autocompleteselect is shown below : ')
			console.log(xs)
			_.each(xs, function(x) {
				$( "#idPemberiKuasa" )
				.append('<option class="id-pemberi-kuasa">'
					+x.id+'</option>')
			})
		})
    })

})
