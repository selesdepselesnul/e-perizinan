$(document).ready(function () {

    function makeHeader(data) {
    	switch(data) {
    		case 'suratkuasapemberi':
    		    return '<tr><th>kode pemberi</th></tr>'
    	}
    }

	$('#dataKind').click(function() {
		$('#placeHolder').remove();
		$('#dataId').removeAttr('disabled')

		const dataKind = $('#dataKind').val();
		$.getJSON(BASE_URL+'json/'+dataKind+'/'+$('#dataId').val())
		 .done(function(xs) {
		 	$('#dataTable').append(makeHeader(dataKind))
		 })
	})	
})