/**
 *
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
$(document).ready(() => {
	createTable({
		dataName: 'Monitoring',
		tableName: 'monitoringTable',
		headerName: 'headerMonitoringTable',
		ajaxURL: 'json/monitoring',
		defaultLang: 'monitoring',
		defaultCol: [
		{ data : "nomor" },
		{ data : "namaPemohon" },
		{ data : "tglIzinPrinsip" },
		{ data : "nomorIzinPrinsip" },
		{ data : "nomorIppt" },
		{ data : "tglIppt" },
		{ data : "dalamProsesIppt" },
		{ data : "nomorDokumenLingkungan"},
		{ data : "tglDokumenLingkungan"},
		{ data : "dalamProsesDokumenLingkungan"},
		{ data : "nomorAmdalLalin"},
		{ data : "tglAmdalLalin"},
		{ data : "dalamProsesAmdalLalin"},
		{ data : "nomorKkop"},
		{ data : "tglKkop"},
		{ data : "dalamProsesKkop"},
		{ data : "nomorImb"},
		{ data : "tglImb"},
		{ data : "dalamProsesImb"}],
		handleEditing: (x) => '<button '
		+'id="editingButton_'
		+x.nomor+'" class="editing-button '
		+'btn btn-warning" title="perbaharui data monitoring dari pemohon ' + x.namaPemohon +'" onclick="onEditing(\'monitoring\', '+ x.nomor+', \'monitoring\')">'
		+'<span class="glyphicon'
		+' glyphicon-pencil"></span>'
		+'</button>',
		handleDeleting: (x) => '<button '
		+'id="removingButton_'+ x.nomor
		+'" class="removing-button '
		+'btn btn-danger" title="hapus data monitoring dari pemohon ' + x.namaPemohon + '" onclick="onRemoving(\'monitoring\', '+ x.nomor+', \'monitoring\')">'
		+'<span class="glyphicon'
		+' glyphicon-trash">'
		+'</span></button>'
	})
})
