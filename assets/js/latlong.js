/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 */
$(document).ready(function() {

	function showMap() {
		const latLong = new google.maps.LatLng(
			$('#latitude').val(),$('#longitude').val())
		const map = new google.maps.Map(document.getElementById('googleMap'),{
			center: latLong,
			zoom: 12,
			mapTypeId: toTypeId($('#mapType').val())
		})
		const marker= new google.maps.Marker({
			position:latLong,
			animation:google.maps.Animation.BOUNCE
		})
		marker.setMap(map)
	}

	if($('#latitude').val() === '0') {
		console.log('latitude is 0')
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				$('#latitude').val(position.coords.latitude)
				$('#longitude').val(position.coords.longitude)
				showMap()
			})
		} else {
			alert("Geolocation tidak support untuk browser ini")
		}

	} else {
		console.log('not zero')
	}

	function toTypeId(choice) {
		if (choice == 'roadmap')
			return google.maps.MapTypeId.ROADMAP
		else
			return google.maps.MapTypeId.HYBRID
	}

	$('#latitude').change(function() {
		showMap()
	})

	$('#longitude').change(function() {
		showMap()
	})

	$('#mapType').click(function() {
		showMap()
	})

	$('#mapType').trigger('click')
})
