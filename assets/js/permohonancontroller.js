/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 *
 */
$(document).ready(function() {
		$(document).tooltip()
		function initPenerimaKuasaTable(nomorPemohon, namaPemohon) {
			$("title").text('e-UPB | Penerima Kuasa : ' + namaPemohon)
			$("#penerimaKuasaDialog").dialog( "option", "title", "Daftar Penerima Kuasa : " + namaPemohon)
			$('#penerimaKuasaTable').DataTable().destroy()
			$('#penerimaKuasaTable').DataTable({
				language: default_lang('penerima kuasa'),
				ajax: BASE_URL + '/json/penerimakuasa/wherepemohonid/'+nomorPemohon,
				processing: true,
				columns: [
				{ data : "nomor" },
				{ data : "nama" },
				{ data : "alamat" },
				{ data : "noHp" }],
				lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
				dom: 'Bfrtip',
				buttons: [
				{
					text: 'Tambah',
					action: function () {
						onAdding('permohonan/create')
					}
				},
				'pageLength',
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]
			})
		}

		function initPersyaratanTable(nomorPemohon, namaPemohon) {
			$("title").text('e-UPB | Persyaratan : ' + namaPemohon)
			$("#persyaratanDialog").dialog( "option", "title", "Daftar Persyaratan : " + namaPemohon)
			$('#persyaratanTable').DataTable().destroy()
			$('#persyaratanTable').DataTable({
				language: default_lang('persyaratan'),
				paging: false,
				ordering: false,
				info: false,
				searching: false,
				ajax: BASE_URL + '/json/pemohon/'+nomorPemohon+'/persyaratan/',
				processing: true,
				columns: [
				{ data : "nomorSuratPermohonan" },
				{ data : "nomorKtp" },
				{ data : "nomorSuratKuasa" },
				{ data : "nomorAktePendirian" },
				{ data : "nomorSertifikatTanah" },
				{ data : "nomorPbb" },
				{ data : "nomorProposalProyek" },
				{ data : "nomorRincianLahan" },
				{ data : "nomorSuratPernyataan" },
				{ data : "nomorSuratRencanaPembangunan" },
				{ data : "nomorSuratDomisiliPerusahaan" },
				{ data : "nomorSuratPemberitahuanTetangga" }],
				dom: 'Bfrtip',
				buttons: [
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]
			})
}



function initPemohonTable(nomorPemohon, namaPemohon) {
	$("title").text('e-UPB | Pemohon : ' + namaPemohon)
	$("#pemohonDialog").dialog( "option", "title", "Identitas Pemohon : " + namaPemohon)
	$('#pemohonTable').DataTable().destroy()
	$('#pemohonTable').DataTable({
		language: default_lang('pemohon'),
		paging: false,
		ordering: false,
		info: false,
		searching: false,
		ajax: BASE_URL + '/json/pemohon/'+nomorPemohon,
		processing: true,
		columns: [
		{ data : "nomor" },
		{ data : "nama" },
		{ data : "alamat" },
		{ data : "namaBadanUsaha" },
		{ data : "jabatan" },
		{ data : "alamatBadanUsaha" },
		{ data : "npwp" },
		{ data : "noAktePerusahaan"},
		{ data : "luasBangunan"},
		{ data : "luasTanah"},
		{ data : "jenisBangunan"},
		{ data : "alamatLokasi"},
		{ data : "rt"},
		{ data : "rw"},
		{ data : "kelurahan"},
		{ data : "kecamatan"},
		{ data : "kota"}],
		dom: 'Bfrtip',
		buttons: [
		{
			extend: 'colvis',
			text: 'Pilih Kolom'

		},
		{
			extend: 'excelHtml5',
			text: 'Excel',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		},
		{
			extend: 'print',
			text: 'Print',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		}]
	})
}


onListPenerimaKuasa = function(nomor, nama) {
	console.log(nomor)
	initPenerimaKuasaTable(nomor, nama)
	$('#penerimaKuasaDialog').dialog('open')
}

onListPersyaratan = function(nomor, nama) {
	console.log(nomor)
	initPersyaratanTable(nomor, nama)
	$('#persyaratanDialog').dialog('open')
}

onListPemohon = function(nomor, nama) {
	console.log(nomor)
	initPemohonTable(nomor, nama)
	$('#pemohonDialog').dialog('open')
}

function intToValidation(x) {
	if(x == 0)
		return 'ditolak'
	else if(x == 1)
		return 'selesai'
	else
		return 'dalam proses'
}

$('#permohonanTable').DataTable({
	ajax: BASE_URL + '/json/permohonan/',
	processing: true,
	language: default_lang('permohonan'),
	columns: [
	{ data : "nomor" },
	{ data : "tglPermohonan" },
	{ data : "tglBerkasMasuk" },
	{ data : "tglPembahasan" },
	{ data : "tglTinjauanLapangan" },
	{ data : "pembahasanRapatBKPRD" },
	{ data : "latitude" },
	{ data : "longitude" },
	{ data : "beritaAcaraLapangan" },
	{ data : "jenisKegiatan" },
	{ data : "izinPrinsip" },
	{ data : "tglIzinPrinsip" },
	{ data : "nomorIzinPrinsip" },
	{ data : "penerimaPerizinan" },
	{ data : "tglPenerimaPerizinan" },
	{ data : null,
		orderable: false,
		render : function(x) {
			$('#pemohonDialog').dialog({
				autoOpen: false,
				modal: true,
				dragable: true,
				height: 280,
				width: 1200,
				show: {
					effect: 'blind',
					duration: 1000
				},
				hide: {
					effect: 'explode',
					duration: 1000
				}
			})
			return '<button class="btn btn-default"' +
			' onclick="onListPemohon('+x.nomorPemohon+', \''+x.namaPemohon+'\')">'+x.namaPemohon+'</button>'
		}
	},
	{ data : null,
		orderable: false,
		render : function(x) {
			$('#persyaratanDialog').dialog({
				autoOpen: false,
				modal: true,
				dragable: true,
				height: 280,
				width: 1200,
				show: {
					effect: 'blind',
					duration: 1000
				},
				hide: {
					effect: 'explode',
					duration: 1000
				}
			})
			return '<button class="btn btn-default"' +
			' onclick="onListPersyaratan('+x.nomorPemohon+', \''+x.namaPemohon+'\')">persyaratan</button>'
		}
	},
	{ data : null,
		orderable: false,
		render : function(x) {
			$('#penerimaKuasaDialog').dialog({
				autoOpen: false,
				modal: true,
				dragable: true,
				height: 280,
				width: 1200,
				show: {
					effect: 'blind',
					duration: 1000
				},
				hide: {
					effect: 'explode',
					duration: 1000
				}
			})
			return '<button class="btn btn-default"onclick="onListPenerimaKuasa('
				+x.nomorPemohon+',\''+x.namaPemohon+'\')">penerima kuasa</button>'
}
},
{ data : null,
	orderable: false,
	render: function(x) {
		return intToValidation(x.validasi1)
	}},
	{ data : null,
		orderable: false,
		render: function(x) {
			return intToValidation(x.validasi2)
		}},
		{ data : null,
			orderable: false,
			render: function(x) {
				return intToValidation(x.validasi3)
			}},
			{data: 'nomorNotaDinasPenolakan'},
			{ data : null,
				orderable: false,
				render : function(x) {
					return '<button title="validasi"'
					+'id="editingButton_'
					+x.nomor+'" class="validating-button '
					+'btn btn-primary" onclick="onValidating('+x.nomor+')">'
					+'<span class="glyphicon'
					+' glyphicon-check"></span>'
					+'</button>'
				}
			},
			{ data : null,
				orderable: false,
				render : function(x) {
					return '<button title="perbaharui"'
					+'id="editingButton_'
					+x.nomor+'" class="editing-button '
					+'btn btn-warning" onclick="onEditing(\'permohonan\', '+ x.nomor+')">'
					+'<span class="glyphicon'
					+' glyphicon-pencil"></span>'
					+'</button>'
				}
			},
			{ data : null,
				orderable: false,
				render : function(x) {
					return '<button title="hapus"'
					+'id="removingButton_'+ x.nomor
					+'" class="removing-button '
					+'btn btn-danger" onclick="onRemoving(\'permohonan\', '+ x.nomor+')">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'
				}
			}],
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
			dom: 'Bfrtip',
			buttons: [
			{
				text: 'Tambah',
				action: function () {
					onAdding('permohonan')
				}
			},
			'pageLength',
			{
				extend: 'colvis',
				text: 'Pilih Kolom'

			},
			{
				extend: 'excelHtml5',
				text: 'Excel',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			},
			{
				extend: 'print',
				text: 'Print',
				exportOptions: {
					modifier: {
						page: 'current'
					},
					columns: ':visible'
				}
			}]

		})
})
