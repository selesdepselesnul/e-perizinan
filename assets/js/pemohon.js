/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 *
 */
$(document).ready(function () {
	function fillKelurahan() {
		$.getJSON(BASE_URL + 'json/kelurahan/')
		.done(function(xs) {
			console.log($('#kecamatan').val())
			const filteredKelurahan = xs.filter(function(x) {
				return x.nomorKecamatan == $('#kecamatan').val()
			})
			filteredKelurahan.forEach(function(x) {
				$('#kelurahan').append(
					'<option value="'+ x.nomor +
					'" class="kelurahan">'+x.nama+'</option>')
			})
		})
	}

	fillKelurahan()

	$('#kecamatan').click(function() {
		$('.kelurahan').remove()
		fillKelurahan()
	})
})
