/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 *
 */
$(document).ready(function() {
		const initPenerimaKuasaTable = 	(nomorPemohon, namaPemohon) => {
			$("title").text('Daftar Penerima Kuasa : ' + namaPemohon)
			$("#penerimaKuasaDialog").dialog( "option",
				"title", "Daftar Penerima Kuasa : " + namaPemohon)

			const penerimaKuasaTable = createTable({
				dataName: 'PenerimaKuasa',
				tableName: 'penerimaKuasaTable',
				headerName: 'headerPenerimaKuasaTable',
				defaultLang: 'penerima kuasa',
				ajaxURL: '/json/penerimakuasa/wherepemohonid/'+nomorPemohon,
				addingRoute: 'penerimakuasa/'+nomorPemohon,
				defaultCol: [
				{ data : "nomor" },
				{ data : "nama" },
				{ data : "alamat" },
				{ data : "noHp" }],
				handleEditing: (x) => '<button title="perbaharui data ' + x.nama + '" '
					+'id="editingButton_'
					+x.nomor+'" class="editing-button '
					+'btn btn-warning" onclick="onEditing(\'penerimakuasa/'
					+x.nomorPemohon+"', "+ x.nomor+', \'pemohon\')">'
					+'<span class="glyphicon'
					+' glyphicon-pencil"></span>'
					+'</button>',
				handleDeleting: (x) => '<button title="hapus data ' + x.nama + '" '
					+'id="removingButton_'+ x.nomor
					+'" class="removing-button '
					+'btn btn-danger" onclick="onRemoving(\'penerimakuasa\', '+ x.nomor+', \'pemohon\')">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'
			})
		}

		const initBerkasTable = (nomorPemohon, namaPemohon) => {
			$("title").text('Daftar Berkas : ' + namaPemohon)
			$("#berkasDialog").dialog( "option", "title", "Daftar Berkas : " + namaPemohon)
			const berkasTable = createTable({
				dataName: 'Berkas',
				tableName: 'berkasTable',
				headerName: 'headerBerkasTable',
				defaultLang: 'berkas',
				ajaxURL: 'json/berkas/wherepemohonid/'+nomorPemohon,
				addingRoute: 'berkas/'+nomorPemohon,
				defaultCol: [{ data : "nomor" },
			 	 { data : null,
			   	   orderable: false,
			   	   render : function(x) {
				  		return '<button class="btn btn-default"><a href="'
				  			+BASE_URL+'berkas-file/'+nomorPemohon+'/'
							+x.nama+x.ext+'">'+x.nama+' </a></button>'
			   		}
				 }],
				handleEditing: (x) => '<button '
					+'id="editingButton_'
					+x.nomorPemohon+'" class="editing-button '
					+'btn btn-warning" title="ganti nama berkas '+ x.nama +'" onclick="onEditing(\'berkas\', '+ x.nomor+', \'pemohon\')">'
					+'<span class="glyphicon'
					+' glyphicon-pencil"></span>'
					+'</button>',
				handleDeleting: (x) => '<button '
					+'id="removingButton_'+ x.nomorPemohon
					+'" class="removing-button '
					+'btn btn-danger" title="hapus berkas '+x.nama+'" onclick="onRemoving(\'berkas\', '+ x.nomor+', \'pemohon\')">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'})
		}


		const initPersyaratanTable = (pemohonID, namaPemohon) => {
			$("title").text('Persyaratan : ' + namaPemohon)
			$("#persyaratanDialog").dialog( "option", "title", "Daftar Persyaratan : " + namaPemohon)
			$('#persyaratanTable').DataTable().destroy();
			$('#persyaratanTable').DataTable({
				language: default_lang('persyaratan'),
				ajax: BASE_URL + '/json/pemohon/'+pemohonID+'/persyaratan/',
				processing: true,
				searching: false,
				info: false,
				paging: false,
				ordering: false,
				columns: [
					{ data : "nomorSuratPermohonan" },
					{ data : "nomorKtp" },
					{ data : "nomorSuratKuasa" },
					{ data : "nomorAktePendirian" },
					{ data : "nomorSertifikatTanah" },
					{ data: 'tahunSertifikatTanah' },
					{ data: 'luasTanahSertifikatTanah' },
					{ data: 'atasNamaSertifikatTanah' },
					{ data: 'jenisKepemilikanTanahSertifikatTanah' },
					{ data : "nomorPbb" },
					{ data : "nomorProposalProyek" },
					{ data : "nomorRincianLahan" },
					{ data : "nomorSuratPernyataan" },
					{ data : "nomorSuratRencanaPembangunan" },
					{ data : "nomorSuratDomisiliPerusahaan" },
					{ data : "nomorSuratPemberitahuanTetangga" },
				    { data : null,
					  orderable: false,
				  	  render: (x) =>
						  '<button '
							+'id="editingButton_'
							+x.nomor+'" class="editing-button '
							+'btn btn-warning" title="perbaharui persyaratan '+ namaPemohon+'" onclick="onEditing(\'persyaratan\', '+ pemohonID+', \'pemohon\')">'
							+'<span class="glyphicon'
							+' glyphicon-pencil"></span>'
							+'</button>'

					}],
				dom: 'Bfrtip',
				buttons: [
				{
					extend: 'colvis',
					text: 'Pilih Kolom'

				},
				{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				},
				{
					extend: 'print',
					text: 'Print',
					exportOptions: {
						modifier: {
							page: 'current'
						},
						columns: ':visible'
					}
				}]
			})
		}

		onListPenerimaKuasa = (nomor, nama) => {
			initPenerimaKuasaTable(nomor, nama)
			$('#penerimaKuasaDialog').dialog('open')
		}

		onListPersyaratan = (nomor, nama) => {
			initPersyaratanTable(nomor, nama)
			$('#persyaratanDialog').dialog('open')
		}

		onListBerkas = (nomor, nama) => {
			initBerkasTable(nomor, nama)
			$('#berkasDialog').dialog('open')
		}

		const pemohonTable = createTable({
			dataName: 'Pemohon',
			tableName: 'pemohonTable',
			headerName: 'headerPemohonTable',
			defaultLang: 'pemohon',
			ajaxURL: '/json/pemohon/',
			addingRoute: 'pemohon',
			defaultCol: [
				{ data : "nomor" },
				{ data : "nama" },
				{ data : "alamat" },
				{ data : "namaBadanUsaha" },
				{ data : "jabatan" },
				{ data : "alamatBadanUsaha" },
				{ data : "npwp" },
				{ data : "jenisBadanUsaha"},
				{ data : "noAktePerusahaan"},
				{ data : "luasBangunan"},
				{ data : "luasTanah"},
				{ data : "jenisBangunan"},
				{ data : "alamatLokasi"},
				{ data : "rt"},
				{ data : "rw"},
				{ data : "kelurahan"},
				{ data : "kecamatan"},
				{ data : "kota"},
				{ data : "nomorDisposisiSekda"},
				{ data : "nomorDisposisiKaBappeda"},
				{ data : "nomorDisposisiKabid"},
				{ data : null,
					orderable: false,
					render : function(x) {
						$('#persyaratanDialog').dialog({
							autoOpen: false,
							modal: true,
							dragable: true,
							height: 280,
							width: 1200,
							show: {
								effect: 'blind',
								duration: 1000
							},
							hide: {
								effect: 'explode',
								duration: 1000
							}
						})
						return '<button class="btn btn-default" title="persyaratan '+ x.nama + '" '+
						' onclick="onListPersyaratan('+x.nomor+', \''+x.nama+'\')">persyaratan</button>'
					}
				},
				{ data : null,
					orderable: false,
					render : function(x) {
						$('#penerimaKuasaDialog').dialog({
							autoOpen: false,
							modal: true,
							dragable: true,
							height: 280,
							width: 1200,
							show: {
								effect: 'blind',
								duration: 1000
							},
							hide: {
								effect: 'explode',
								duration: 1000
							}
						})
						return '<button class="btn btn-default" title="daftar penerima kuasa '+ x.nama
							+ '"'+' onclick="onListPenerimaKuasa('
							+x.nomor+',\''+x.nama+'\')">penerima kuasa</button>'
					}
				},
				{ data : null,
					orderable: false,
					render : function(x) {
						$('#berkasDialog').dialog({
							autoOpen: false,
							modal: true,
							dragable: true,
							height: 280,
							width: 1200,
							show: {
								effect: 'blind',
								duration: 1000
							},
							hide: {
								effect: 'explode',
								duration: 1000
							}
						})
						return '<button title="berkas '+x.nama+'" '
						+'id="berkasButton_'
						+x.nomor+'" class="berkas-button '
						+'btn btn-primary" onclick="onListBerkas('+x.nomor+', \''+x.nama+'\')">'
						+'<span class="glyphicon glyphicon-folder-open"></span>'
						+'</button>'
					}
				}],
				handleEditing: (x) => '<button title="perbaharui data ' + x.nama + '" '
					+'id="editingButton_'
					+x.nomor+'" class="editing-button '
					+'btn btn-warning" onclick="onEditing(\'pemohon\', '+ x.nomor+')">'
					+'<span class="glyphicon'
					+' glyphicon-pencil"></span>'
					+'</button>',
				handleDeleting: (x) => '<button title="hapus data ' + x.nama + '" '
					+'id="removingButton_'+ x.nomor
					+'" class="removing-button '
					+'btn btn-danger" onclick="onRemoving(\'pemohon\', '+ x.nomor+')">'
					+'<span class="glyphicon'
					+' glyphicon-trash">'
					+'</span></button>'
		})

})
