/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 *
 */
$(document).ready(function() {

	function handleOnChecked(name) {
		$('input[name="'+name+'"]').click(function() {
			if($('input[name="'+name+'"]').is(':checked')) {
				$('#'+name).append(
					'<div class="col-sm-2" id="nomor_'+name+'">'+
					'<input type="text" placeholder="nomor" class="form-control"/>'
					+'</div>')
				console.log('checked')
			} else {
				$('#nomor_'+name).remove()
				console.log('not checked')
			}
		})
	}
	_.each(['surat_permohonan', 'ktp', 'surat_kuasa', 'akte_pendirian',
		 'sertifikat_tanah', 'pbb', 'proposal_proyek', 'rincian_lahan',
		 'surat_pernyataan', 'rencana_pembangunan', 'domisili_perusahaan',
		 'pemberitahuan_tetangga'], handleOnChecked)


})
