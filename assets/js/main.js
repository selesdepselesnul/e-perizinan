/**
 * @author : Moch Deden (https://github.com/selesdepselesnul)
 *
 */
function createTable(tableProp) {

	$.when( $.ajax( BASE_URL+'json/jenisrole/whereid/'+role.nomorJenisRole ) ).done(( x ) => {
		const jenisRole = $.parseJSON(x)
		$('#forValidating').remove()
		$('#forEditing').remove()
		$('#forDeleting').remove()

		if(jenisRole['dapatValidasiPermohonan'] == '1') {

			if(typeof tableProp.handleValidating !== "undefined") {
				if(tableProp.dataName == 'Permohonan') {
					tableProp.defaultCol.push({
						data : null,
						orderable: false,
						render: tableProp.handleValidating
					})
					$('#'+tableProp.headerName).append('<th id="forValidating">Validasi</th>')
				}
			}

		}

		if(jenisRole['dapatEdit'+tableProp.dataName] == '1') {
			if(typeof tableProp.handleEditing !== "undefined") {
				tableProp.defaultCol.push({
					data : null,
					orderable: false,
					render : tableProp.handleEditing
				})
				$('#'+tableProp.headerName).append('<th id="forEditing">Edit</th>')
			}
		}

		if(jenisRole['dapatHapus'+tableProp.dataName] == '1') {
			if(typeof tableProp.handleDeleting !== "undefined") {
				tableProp.defaultCol.push({
					data : null,
					orderable: false,
					render : tableProp.handleDeleting
				})
				$('#'+tableProp.headerName).append('<th id="forDeleting">Hapus</th>')
			}
		}


		$('#'+tableProp.tableName).DataTable().destroy()
		const dataTable = $('#'+tableProp.tableName).DataTable({
			language: default_lang(tableProp.defaultLang),
			ajax: BASE_URL + tableProp.ajaxURL,
			processing: true,
			columns: tableProp.defaultCol,
		// lengthMenu: [[10, 25, 50, -'1'], [10, 25, 50, "Semua"]],
		dom: 'Bfrtip',
		buttons: [
		// 'pageLength',
		{
			extend: 'colvis',
			text: 'Pilih Kolom'

		},
		{
			extend: 'excelHtml5',
			text: 'Excel',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		},
		{
			extend: 'print',
			text: 'Print Sebagian',
			exportOptions: {
				modifier: {
					page: 'current'
				},
				columns: ':visible'
			}
		},
		{
			extend: 'print',
			text: 'Print Semua',
			exportOptions: {
				columns: ':visible'
			}
		}]
	})
		if(tableProp.addingRoute !== "undefined")
			if(jenisRole['dapatTambah'+tableProp.dataName] == '1')
				dataTable.button().add(0, {
					text: 'Tambah',
					action: function () {
						onAdding(tableProp.addingRoute)
					}
				})

		})

}



function onRemoving(route, id, reroute) {
	$.ajax({
		url : BASE_URL + route + '/' + id,
		method : 'DELETE'
	}).done(function(x) {
		var lastRoute = route

		if(reroute)
			lastRoute = reroute

		window.open(BASE_URL+lastRoute, '_self');
	})
}

function onRemovingWithFinalizer(route, id, finalizer) {
	$.ajax({
		url : BASE_URL + route + '/' + id,
		method : 'DELETE'
	}).done(function(x) {
		finalizer()
	})
}

function redirectToDiv(mainRoute, divId, callback) {
	return () => {
	 	location.reload()
		window.open(mainRoute+'#'+divId, '_self')
	}
}


function onEditing(route, id) {
	window.open(
		BASE_URL+route+'/'+id+'/edit',
		'_self')
}


function onUpdatingButton(dataName, id) {
	const editingVal = $('#in_editing_value_'+dataName+'_'+id).val()
	$.post(
		BASE_URL + 'json/' + dataName + '/' + id,
		{'nama' : editingVal})
	.done((x) => {
		$('#in_editing_'+dataName+'_'+id).replaceWith('<td class="sorting_1">'
			+editingVal+'</td>')
		window.open(BASE_URL+'datamaster', '_self')
	})

}

function onEditingInPlace(dataName, id, name) {
	$('td[class="sorting_1"]:contains("'+name+'")').replaceWith(
		'<td id="in_editing_'+dataName+'_'+id+'"><div class="form-inline"><input id="in_editing_value_'
		+ dataName + '_' +id
		+'" type="text" class="form-control" value="'+name+'"/>'
		+'<button class="form-control" onclick="onUpdatingButton(\''
			+dataName+'\', \''+id+'\')"><span class="glyphicon glyphicon-ok"></span></button></div>'
	+'</td>')
}

function onAdding(route) {
	window.open(BASE_URL+route+'/create'
		, '_self')
}

function onValidating(nomor) {
	window.open(BASE_URL+'permohonan/checkvalidation/'+nomor
		, '_self')
}
